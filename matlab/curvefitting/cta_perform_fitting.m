function cta_perform_fitting(properties, output)
    distributions = cta_get_distributions();
    details       = {'KS AD', 'Parameters', 'D-Statistic'};

    % Fit Distributions %parfor!
    for i = 1:length(properties)
        fit_property(             ...
            properties{i,1},      ...
            properties{i,2},      ...
            properties{i,3},      ...
            properties{i,4},      ...
            properties{i,5},      ...
            distributions,        ...
            details,              ...
            output                ...
        );
    end
end
    
function fit_property(property_name, value_names, values, property_unit, logscale, distributions, details, output)
    disp(['Analyzing property: ', property_name]);
    
    selection = ones(length(value_names), size(distributions,1));
    
    % Fit properties
    results = cell(1,length(value_names));
    for index = 1:length(value_names)
        try
            results{index} = fit_prob_distributions(distributions, values{index});
        catch ME
            warning(getReport(ME));
            %rethrow(ME);
            % Uncomment above ^^ if you want to see/retgrow the errors
        end
    end

    for d = 1:length(details)
        % Format results
        name = details{d};

        disp(name);

        table = DataTable();
        for i = 1:size(distributions,1)
            table{1, i+1} = distributions{i,1};
        end

        for i = 1:length(value_names)
            table{i+1,1} = value_names(i);
            for j = 1:size(distributions,1)
                try
                    if d == 1
                        % KS / AD
                        table{i+1,j+1} = num2str(results{i}{d}{j}, '%1.2f ');

                        if min(results{i}{d}{j}) >= 0.05
                            table.setCellFormat(i+1, j+1, 'b');
                        else
                            selection(i,j) = 0;
                        end
                    elseif d == 2
                        % Parameters
                        table{i+1,j+1} = num2str(results{i}{d}{j}{1}, '%1.2f ');
                    else
                        % D-Statistic
                        table{i+1,j+1} = num2str(results{i}{d}{j}, '%1.2f ');
                    end
                catch ME
                    table{i+1,j+1} = '?';
                end
            end
        end

        table.toText;

        fid = fopen(strcat(output, '/', property_name, '_', name, '.tex'), 'w');
        table.toLatex('tight', 'hline', fid);
        fclose(fid);

        fid = fopen(strcat(output, '/', property_name, '_', name, '.html'), 'w');
        table.toHTML(fid);
        fclose(fid);
        
    end
    
    for i = 1:length(value_names)
        try
            selection = cell2mat(results{i}{3}(:)') == min(cell2mat(results{i}{3}(selection(i,:)==1)));
        end
    end

    
    plot_graphs(property_name, value_names, values, property_unit, logscale, distributions, details, output, results, selection);
end

function plot_graphs(property_name, value_names, values, property_unit, logscale, distributions, details, output, results, selection)
    
    for s = 1:length(values)        
        try
            close all;
            h = figure('Visible', 'off');

            v = values{s};
            [f,x] = ecdf(v);
            
            if logscale
                axes('XScale', 'log');
            end
            
            xlabel(property_unit);
            hold all;
            stairs(x, f, 'LineWidth', 4);

            for i = 1:size(distributions,1)
                try % calculate distribution cdf
                    r = distributions{i,4};
                    %filter = distributions{i,8};
                    %xf = filter(x);
                    y = call_cdf(r, x, results{s}{2}{i});
                    
                    try
                        if selection(s,i) == 1
                            plot(x, y, 'LineWidth', 4);
                        else
                            plot(x, y);
                        end
                    catch
                        plot(x, y);
                    end
                catch ME
                    disp(ME);
                    rethrow(ME);
                end
            end
            legend(['Empirical'; distributions(:,1)]','Location', 'SouthEast');

            saveas(h, strcat(output, '/', value_names{s}, '-', property_name), 'epsc2');
            saveas(h, strcat(output, '/', value_names{s}, '-', property_name), 'png');
        catch ME
            display(ME);
            %rethrow(ME);
        end
    end
    
    close all;
end