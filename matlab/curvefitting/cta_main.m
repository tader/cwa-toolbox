function cta_main()
    close all;
    clear;
    clc;
    
    if ismac
        datasets = {
            'SN1', '/Users/thomas/Documents/Studie/CTA/JobDump_May5_Oct31_2009';
            'Yahoo',    '/Users/thomas/Documents/Studie/CTA/yahoo-m';
        };
        output = '/Users/thomas/Documents/Studie/Output';
    else
        datasets = {
            'SN1', '/home/af10028/thesis-project/traces/data/facebook/cwa/traces/trace';
            'Yahoo',    '/home/af10028/thesis-project/traces/data/yahoo_mcluster/cwa/traces/yahoo-m';
        };
        output = '/home/af10028/thesis-project/modeling';
    end
    
    [cwj_data cwt_data] = cta_load(datasets);
    save('state');
    
    task_properties = cta_generate_task_properties(datasets, cwj_data, cwt_data);    
    save('state');
    
    properties      = cta_generate_properties(datasets, cwj_data, cwt_data, task_properties);
    save('state');
    
    cta_perform_basic_statistics(properties, output);
    cta_perform_correlation_check(properties, output);
    cta_perform_fitting(properties, output);
end