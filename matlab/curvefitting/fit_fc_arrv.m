function [] = fit_fc_arrv(input_path, output_path)
% Boxun Zhang, March 17th, 2011
% PDS group, TU Delft, the Netherlands
    
    swarm_props = load(input_path);

    % Basic statistics
    disp('Basic Statistics:')
    stats_table = DataTable();
    stats_table{1, 1:7} = {'property', 'Min', '1st Q', 'Median', ...
                                                'Mean', '3rd Q', 'Max'};
    stats_table{2, 1} = 'arrival rates';
    
    v = swarm_props(:, 2);
    stats_table{2, 2} = num2str(min(v), '%1.2f ');    
    stats_table{2, 3} = num2str(quantile(v, 0.25), '%1.2f ');
    stats_table{2, 4} = num2str(quantile(v, 0.5), '%1.2f ');
    stats_table{2, 5} = num2str(mean(v), '%1.2f ');
    stats_table{2, 6} = num2str(quantile(v, 0.75), '%1.2f ');
    stats_table{2, 7} = num2str(max(v), '%1.2f ');

    stats_table.toText;
    
    
    % GOF tests (Komorogov-Smirnov, Anderson-Darling)
    disp('GOF Tests:')
    gof_table = DataTable();
    gof_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                        'Pareto', 'Log-Normal', 'Gamma'};
    gof_table{2, 1} = 'arrival rates';
    
    
    % parameters of fitting distributions
    param_table = DataTable();
    param_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                        'Pareto', 'Log-Normal', 'Gamma'};
    param_table{2, 1} = 'arrival rates';
    
   
    v = swarm_props(:, 2);
    fit_res = fit_prob_distributions(v);
        
        % p-values
    gof_strs = [];
    for i=1:length(fit_res{1})
        gof_strs{i} = num2str([fit_res{1}(i), fit_res{2}(i)], '%1.3f ');
    end
    gof_table{2, 2:6} = gof_strs;
        
        % parameters
    params_cell = fit_res{3};
    params_strs = [];
    for i=1:length(params_cell)
        params_strs{i} = num2str(params_cell{i}, '%10.2f ');
    end
    param_table{2, 2:6} = params_strs;
   
    
    gof_table.toText;
    param_table.toText;
    
%     
%     fid = fopen(strcat(output_path, '/', 'basic_stats.tex'), 'w');
%     stats_table.toLatex('tight', 'hline', fid);
%     fclose (fid);
%     
%     fid = fopen(strcat(output_path, '/', 'p_values.tex'), 'w');
%     gof_table.toLatex('tight', 'hline', fid);
%     fclose (fid);
%     
%     fid = fopen(strcat(output_path, '/', 'gof_params.tex'), 'w');
%     param_table.toLatex('tight', 'hline', fid);
%     fclose (fid);
end