function [] = D_Stats(pcol)

file_obt09 = '~/Data/flashcrowd/btworld/openbt_2009/result/swarm_properties_hm_init.dat';
file_obt10 = '~/Data/flashcrowd/btworld/openbt_2010/result/swarm_properties_hm_init.dat';
file_pbt09 = '~/Data/flashcrowd/btworld/extra_2009/result/swarm_properties_hm_init.dat';

disp('OpenBitTorrent 2009...');
fit_data_from_file(file_obt09, pcol, 'foobar', '~//Desktop/');

disp('OpenBitTorrent 2010...');
fit_data_from_file(file_obt10, pcol, 'foobar', '~/Desktop/');

disp('PublicBitTorrent 2009...');
fit_data_from_file(file_pbt09, pcol, 'foobar', '~/Desktop/');

