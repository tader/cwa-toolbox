function [] = fit_cwj(input_path, output_path)
% Boxun Zhang, March 17th, 2011
% PDS group, TU Delft, the Netherlands

    if ~isdir(output_path)
        mkdir(output_path);
    end

    disp(strcat('Reading input file: ', input_path));
    tic_input = tic;
    input_data = load(input_path);
    toc(tic_input);
    
    % overwrite job id with
    % calculated inter arrival time
    for i = 2:length(input_data(:,2))
        input_data(i,1) = input_data(i,2) - input_data(i-1,2);
    end
    input_data(1,1) = 0;
    
    % JobID 
    % SubmitTime 
    % WaitTime 
    % RunTime
    % CPUs 
    % TotalWallClockTime 
    % Memory 
    % Network 
    % Disk 
    % Status 
    % UserID 
    % GroupID 
    % ExecutableID 
    % QueueID 
    % PartitionID 
    % JobProperties 
    % StructuralChanges 
    % StructuralChangeParams 
    % DiskIORatio
    
   
    properties = {
        {'InterArrivalTime'        1}
        {'WaitTime'                3}
        {'RunTime'                 4}
        {'CPUs'                    5}
        {'TotalWallClockTime'      6}
        {'Memory'                  7}
        {'Network'                 8}
        {'Disk'                    9}
        {'Status'                 10}
        {'UserID'                 11}
        {'GroupID'                12}
        {'ExecutableID'           13}
        {'QueueID'                14}
        {'PartitionID'            15}
        {'JobProperties'          16}
        {'StructuralChanges'      17}
        {'StructuralChangeParams' 18}
        {'DiskIORatio'            19}
    };

    % Basic statistics
    disp('Basic Statistics...')
    tic_basicstat = tic;
        stats_table = DataTable();
        stats_table{1, 1:7} = {'property', 'Min', '1st Q', 'Median', ...
                                                    'Mean', '3rd Q', 'Max'};

        % GOF tests (Komorogov-Smirnov, Anderson-Darling)
        gof_table = DataTable();
        gof_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                            'Pareto', 'Log-Normal', 'Gamma'};

        % D-Statistic
        d_table = DataTable();
        d_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                            'Pareto', 'Log-Normal', 'Gamma'};

        % parameters of fitting distributions
        param_table = DataTable();
        param_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                            'Pareto', 'Log-Normal', 'Gamma'};

        for prop_index = 1:length(properties)
            table_row = prop_index + 1;
            property_label  = properties{prop_index}{1};
            property_column = properties{prop_index}{2};

            stats_table{table_row, 1} = property_label;
              gof_table{table_row, 1} = property_label;
                d_table{table_row, 1} = property_label;
            param_table{table_row, 1} = property_label;

            v = input_data(:,property_column);

            stats_table{table_row, 2} = num2str(min(v), '%1.2f ');
            stats_table{table_row, 3} = num2str(quantile(v, 0.25), '%1.2f ');
            stats_table{table_row, 4} = num2str(quantile(v, 0.5), '%1.2f ');
            stats_table{table_row, 5} = num2str(mean(v), '%1.2f ');
            stats_table{table_row, 6} = num2str(quantile(v, 0.75), '%1.2f ');
            stats_table{table_row, 7} = num2str(max(v), '%1.2f ');
        end
    toc(tic_basicstat);

    stats_table.toText;
    
    
    disp('Fitting...')
    tic_gof = tic;
        for prop_index = 1:length(properties)
            table_row = prop_index + 1;
            property_label  = properties{prop_index}{1};
            property_column = properties{prop_index}{2};

            disp(strcat(['Processing property: ', property_label]));

            v = input_data(:,property_column);

            % real fitting happens here
            try
                fit_res = fit_prob_distributions(v);

                % p-values
                gof_strs = [];
                for i=1:length(fit_res{1})
                    gof_strs{i} = num2str([fit_res{1}(i), fit_res{2}(i)], '%1.3f ');
                end
                gof_table{table_row, 2:6} = gof_strs;
                
                % d-stats
                for i=1:length(fit_res{4})
                    d_table{table_row, i+1} = num2str(fit_res{4}{i}, '%1.3f');
                end

                % parameters
                params_cell = fit_res{3};
                params_strs = [];

                for i=1:length(params_cell)
                    params_strs{i} = num2str(params_cell{i}, '%10.2f ');
                end
                param_table{table_row, 2:6} = params_strs;
                
                h = figure;

                rnd_handles = {@exprnd, @wblrnd, @gprnd, @lognrnd, @gamrnd};
                for i = 1:length(rnd_handles)
                    try
                        r = rnd_handles{i};
                        clf(h,'reset');
                        hold off;
                        
                        mle = params_cell{:,i};
                        if (strcmp(func2str(r),'gprnd'))
                            Y = r(mle(1),mle(2),mle(2),length(v),1);
                        elseif (strcmp(func2str(r),'lognrnd') ...
                            || strcmp(func2str(r),'wblrnd') ...
                            || strcmp(func2str(r),'gamrnd'))
                            Y = r(mle(1),mle(2),length(v),1);
                        elseif (strcmp(func2str(r),'exprnd'))
                            Y = r(mle,length(v),1);
                        end
            
                        qqplot(v, Y);
                        saveas(h, strcat(output_path, '/qq-', property_label, '-', func2str(r)), 'epsc2');
                        saveas(h, strcat(output_path, '/qq-', property_label, '-', func2str(r)), 'png');
                    end
                end
                                
                try
                    clf(h,'reset');
                    hold off;
                    axes('XScale', 'log');
                    hold all;
                    [~, stats] = cdfplot(v);
                    x = stats.min:.1:stats.max;
                    %[f,x] = ecdf(v);
                    mu = params_cell{1};
                    plot(x, expcdf(x,mu));
                end
                
                try
                    tmp = params_cell{:,2};
                    A = tmp(1);
                    B = tmp(2);
                    plot(x, wblcdf(x,A,B));
                end
                
                try
                    tmp = params_cell{:,3};
                    K = tmp(1);
                    sigma = tmp(2);
                    theta = sigma;%tmp(3); see gof_test
                    plot(x, gpcdf(x,K,sigma,theta));
                end
                
                try
                    tmp = params_cell{:,4};
                    mu = tmp(1);
                    sigma = tmp(2);
                    plot(x, logncdf(x,mu,sigma));
                end
                
                try
                    tmp = params_cell{:,5};
                    A = tmp(1);
                    B = tmp(2);
                    plot(x, gamcdf(x,A,B));
                end
                
                legend('Emperical', 'Exponentional', 'Weibull', ...
                    'Pareto', 'Log-Normal', 'Gamma', 'Location', 'SE');

                saveas(h, strcat(output_path, '/', property_label), 'epsc2');
                saveas(h, strcat(output_path, '/', property_label), 'png');

            catch ME
                display(ME);
                %rethrow(ME);
                % Uncomment above ^^ if you want to see/retgrow the errors
            end
            
            try
                close(h);
            end

        end
    toc(tic_gof);
    
    gof_table.toText;
    d_table.toText;
    param_table.toText;
    
    % Find Correlations
    disp('Testing for correlations...')
    tic_corr = tic;
        corr_table = DataTable();
        corr_table{1, 1} = 'property';

        for i = 2:length(properties)
            corr_table{i+1, 1} = properties{i}{1};            
            i_values = input_data(:,properties{i}{2});
            

            for j = 1:length(properties)-1
                if 2 == i
                    corr_table{1, j+1} = properties{j}{1};
                end
                
                if i <= j
                    corr_table{i+1, j+1} = 'X';
                else
                    j_values = input_data(:,properties{j}{2});
                    corr_table{i+1, j+1} = num2str(corr(i_values, j_values), '%1.2f ');
                end
            end
        end
    toc(tic_corr);
    
    corr_table.toText;
    
    display('Writing output files...');
    tic_output = tic;
        % Output TeX
        fid = fopen(strcat(output_path, '/', 'basic_stats.tex'), 'w');
        stats_table.toLatex('tight', 'hline', fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'p_values.tex'), 'w');
        gof_table.toLatex('tight', 'hline', fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'd_stats.tex'), 'w');
        d_table.toLatex('tight', 'hline', fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'gof_params.tex'), 'w');
        param_table.toLatex('tight', 'hline', fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'correlation.tex'), 'w');
        corr_table.toLatex('tight', 'hline', fid);
        fclose (fid);

        % Output HTML
        fid = fopen(strcat(output_path, '/', 'basic_stats.html'), 'w');
        stats_table.toHTML(fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'p_values.html'), 'w');
        gof_table.toHTML(fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'd_stats.html'), 'w');
        d_table.toHTML(fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'gof_params.html'), 'w');
        param_table.toHTML(fid);
        fclose (fid);

        fid = fopen(strcat(output_path, '/', 'correlation.html'), 'w');
        corr_table.toHTML(fid);
        fclose (fid);
    toc(tic_output);
end
