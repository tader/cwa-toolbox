function [] = fit_btworld(input_path, output_path)
% Boxun Zhang, March 17th, 2011
% PDS group, TU Delft, the Netherlands
    
    swarm_props = load(input_path);

    % Basic statistics
    disp('Basic Statistics:')
    stats_table = DataTable();
    stats_table{1, 1:7} = {'property', 'Min', '1st Q', 'Median', ...
                                                'Mean', '3rd Q', 'Max'};
    stats_table{2, 1} = 'arrival time';
    stats_table{3, 1} = 'magnitude';
    stats_table{4, 1} = 'duration';
    stats_table{5, 1} = 'plateau';
    stats_table{6, 1} = 'growth rate';
    stats_table{7, 1} = 'fc init-size';
    stats_table{8, 1} = 'fc size';
   
    stats_table{9, 1} = 'sw time-to-peak';
    stats_table{10, 1} = 'sw half-life';
    stats_table{11, 1} = 'sw peak size';
    
    stats_table{12, 1} = 'pre-fc imp';
    stats_table{13, 1} = 'fc contribution';
    stats_table{14, 1} = 'fc left ttp';
    
    for col = 3:15
        v = swarm_props(:, col);
        stats_table{col-1, 2} = num2str(min(v), '%1.2f ');
        stats_table{col-1, 3} = num2str(quantile(v, 0.25), '%1.2f ');
        stats_table{col-1, 4} = num2str(quantile(v, 0.5), '%1.2f ');
        stats_table{col-1, 5} = num2str(mean(v), '%1.2f ');
        stats_table{col-1, 6} = num2str(quantile(v, 0.75), '%1.2f ');
        stats_table{col-1, 7} = num2str(max(v), '%1.2f ');
    end

    stats_table.toText;
    
    
    % GOF tests (Komorogov-Smirnov, Anderson-Darling)
    disp('GOF Tests:')
    gof_table = DataTable();
    gof_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                        'Pareto', 'Log-Normal', 'Gamma'};
    gof_table{2, 1} = 'arrival time';
    gof_table{3, 1} = 'magnitude';
    gof_table{4, 1} = 'duration';
    gof_table{5, 1} = 'plateau';
    gof_table{6, 1} = 'growth rate';
    gof_table{7, 1} = 'fc init-size';
    gof_table{8, 1} = 'fc size';
   
    gof_table{9, 1} = 'sw time-to-peak';
    gof_table{10, 1} = 'sw half-life';
    gof_table{11, 1} = 'sw peak size';
    
    gof_table{12, 1} = 'pre-fc imp';
    gof_table{13, 1} = 'fc contribution';
    gof_table{14, 1} = 'fc left ttp';
    
    
    % parameters of fitting distributions
    param_table = DataTable();
    param_table{1, 1:6} = {'property', 'Exponential', 'Weibull', ...
                        'Pareto', 'Log-Normal', 'Gamma'};
    param_table{2, 1} = 'arrival time';
    param_table{3, 1} = 'magnitude';
    param_table{4, 1} = 'duration';
    param_table{5, 1} = 'plateau';
    param_table{6, 1} = 'growth rate';
    param_table{7, 1} = 'fc init-size';
    param_table{8, 1} = 'fc size';
   
    param_table{9, 1} = 'sw time-to-peak';
    param_table{10, 1} = 'sw half-life';
    param_table{11, 1} = 'sw peak size';
    
    param_table{12, 1} = 'pre-fc imp';
    param_table{13, 1} = 'fc contribution';
    param_table{14, 1} = 'fc left ttp';
    
    for col = 3:15
        v = swarm_props(:, col);
        
        fit_res = fit_prob_distributions(v);
        
        % p-values
        gof_strs = [];
        for i=1:length(fit_res{1})
            gof_strs{i} = num2str([fit_res{1}(i), fit_res{2}(i)], '%1.3f ');
        end
        gof_table{col-1, 2:6} = gof_strs;
        
        % parameters
        params_cell = fit_res{3};
        params_strs = [];
        for i=1:length(params_cell)
            params_strs{i} = num2str(params_cell{i}, '%10.2f ');
        end
        param_table{col-1, 2:6} = params_strs;
    end
    
    gof_table.toText;
    param_table.toText;
    
    
    fid = fopen(strcat(output_path, '/', 'basic_stats.tex'), 'w');
    stats_table.toLatex('tight', 'hline', fid);
    fclose (fid);
    
    fid = fopen(strcat(output_path, '/', 'p_values.tex'), 'w');
    gof_table.toLatex('tight', 'hline', fid);
    fclose (fid);
    
    fid = fopen(strcat(output_path, '/', 'gof_params.tex'), 'w');
    param_table.toLatex('tight', 'hline', fid);
    fclose (fid);
end