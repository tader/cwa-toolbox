function counts = count_fits(task_fits)
    distributions = cta_get_distributions();

    counts = zeros(size(task_fits,2),size(distributions,1));
    
    for t = 1:size(task_fits,1)
    for p = 1:size(task_fits,2)
        try
            dist_gofs = task_fits{t,p}{1};

            for d = 1:size(distributions,1)
                try
                    if min(dist_gofs{d}) >= 0.05
                        counts(p,d) = counts(p,d) + 1;
                    end
                end
            end
        end
    end
    end    
end