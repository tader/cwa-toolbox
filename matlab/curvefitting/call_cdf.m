function y = call_cdf(r, range, results)
    %disp(func2str(r));
    mle = results{1};
    
    if (strcmp(func2str(r),'gpcdf'))
        y = r(range, mle(1), mle(2), mle(2));
    elseif strcmp(func2str(r),'normcdf')
        mle2 = results{2};
        y = r(range, mle, mle2);
    elseif strcmp(func2str(r),'logncdf') ...
        || strcmp(func2str(r),'wblcdf') ...
        || strcmp(func2str(r),'gamcdf')
        y = r(range, mle(1), mle(2));
    elseif (strcmp(func2str(r),'expcdf'))
        y = r(range, mle);
    end
end