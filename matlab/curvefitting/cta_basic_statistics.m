function cta_basic_statistics(title, data, titles, filename)
    try
        length(titles);
    catch
        titles = [];
    end
    
    % nanmin(X)
    %       returns the minimum of a financial time series object X with
    %       NaNs treated as missing. m is the smallest non-NaN element in X
    
    % quantile
    %       treats NaNs as missing values and removes them
    
    stats = {
        'Minimum',                  @nanmin;
        '0.01-th Quantile',         @(X) quantile(X, 0.01);
        '0.1-th Quantile',          @(X) quantile(X, 0.1);
        '0.25-th Quantile',         @(X) quantile(X, 0.25);
        'Mean',                     @nanmean;
        'Median',                   @nanmedian;
        '0.75-th Quantile',         @(X) quantile(X, 0.75);
        '0.9-th Quantile',          @(X) quantile(X, 0.9);
        '0.99-th Quantile',         @(X) quantile(X, 0.99);
        'Maximum',                  @nanmax;
        'Standard Deviation',       @nanstd;
        'Variation',                @nanvar;
        'Coefficient of Variation'  @(X) nanstd(X) ./ (abs(nanmean(X)));
    };
    
    tbl = DataTable();
    tbl{1,1} = title;
    tbl{1,2:size(stats,1)+1} = stats(:,1)';
    
    for i = 2:size(stats,1)+1
        tbl.setCellFormat(1, i, 'v');
    end
    
    for i = 1:size(stats,1)
        f = stats{i,2};
        tbl{2:size(data,2)+1,i+1} = f(data)';
    end
    
    for i = 1:length(titles)
        tbl{i+1,1} = titles(i);
    end
    
    tbl.toText;
    try
        fid = fopen(strcat(filename, '.tex'), 'w');
        tbl.toLatex('tight', 'hline', fid);
        fclose(fid);
        
        fid = fopen(strcat(filename, '.html'), 'w');
        tbl.toHTML(fid);
        fclose(fid);
    end
end