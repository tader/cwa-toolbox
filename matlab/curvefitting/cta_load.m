function [cwj_data cwt_data] = cta_load(datasets)
    cwj_data = {};
    cwt_data = {};
    
    [num_datasets ~] = size(datasets);
    for i = 1:num_datasets
        dataset_name = char(datasets(i,1));
        dataset_file = char(datasets(i,2));
        
        path_cwj = char(strcat(dataset_file, '.cwj'));
        path_cwt = char(strcat(dataset_file, '.cwtd'));
        
        disp(strcat('Loading "', dataset_name, '" data:'));
        disp(strcat('   * Reading input file: "', path_cwj, '"'));
        
        tic_input = tic;
        cwj_data{i} = load(path_cwj);
        time = toc(tic_input);
        disp(strcat('     (', num2str(time), ' s)'));

        disp(strcat('   * Reading input file: "', path_cwt, '"'));
        
        tic_input = tic;
        cwt_data{i} = load(path_cwt);
        time = toc(tic_input);
        disp(strcat('     (', num2str(time), ' s)'));
        
        disp(char(' '));
    end
end