function properties = cta_generate_task_properties(datasets, cwj_data, cwt_data)
    distributions = cta_get_distributions();

    % CWJ_FIELDS = [
    %     "JobID",
    %     "SubmitTime",
    %     "WaitTime",
    %     "RunTime",
    %     "CPUs",
    %     "TotalWallClockTime",
    %     "Memory",
    %     "Network",
    %     "Disk",
    %     "Status",
    %     "UserID",
    %     "GroupID",
    %     "ExecutableID",
    %     "QueueID",
    %     "PartitionID",
    %     "JobProperties",
    %     "StructuralChanges",
    %     "StructuralChangeParams",
    %     "DiskIORatio"
    % ]
    % 
    % CWTD_FIELDS = [
    % 1    "JobID",
    % 2    "TaskID",
    % 3    "SubmitTime",
    % 4    "WaitTime",
    % 5    "RunTime",
    % 6    "CPUs",
    % 7    "TotalWallClockTime",
    % 8    "Memory",
    % 9    "Network",
    % 10    "Disk",
    % 11    "Status",
    % 12    "ExecutableID",
    % 13    "QueueID",
    % 14    "PartitionID",
    % 15    "StructuralChanges",
    % 16    "StructuralChangeParams",
    % 17    "DiskIORatio",
    % 18    "MR_total_hdfs_read",
    % 19    "MR_total_hdfs_written",
    % 20    "MR_total_local_read",
    % 21    "MR_total_local_written",
    % 22    "MR_total_spilled_records",
    % 23    "MR_map_input",
    % 24    "MR_map_output",
    % 25    "MR_map_input_records",
    % 26    "MR_map_output_records",
    % 27    "MR_data_local",
    % 28    "MR_data_rack",
    % 29    "MR_combine_input_records",
    % 30    "MR_combine_output_records",
    % 31    "MR_reduce_input_records",
    % 32    "MR_reduce_input_groups",
    % 33    "MR_reduce_output_records",
    % 34    "MR_task_attempt_host",
    % 35    "MR_task_attempt_shuffle_finished",
    % 36    "MR_task_attempt_sort_finished",
    % 37    "MR_task_attempt_counters",
    % 38    "MR_task_attempt_id",
    % 39    "MR_task_type", 1 = reduce, 0 = map
    % 40    "MR_spilled_records",
    % ]

    tasks = cwt_data{1,2};
    task_ids = unique(tasks(:,1));
    
    try
        load('task_fits');
        disp(task_fits{1});
    catch
        task_fits = {};
    end
    
    for t = length(task_fits)+1:length(task_ids)
        tid = task_ids(t);        
        disp(strcat(['Fitting task of job: ', num2str(t)]));
        
        % all records for one job
        task_values = tasks(tasks(:,1)==tid,:);
        
        % number of tasks
        task_count = size(task_values, 1);
        fail_count = sum(task_values(:,11)==0) + sum(task_values(:,11)==4);
        cancel_count  = sum(task_values(:,11)==5);
        reduce_count = sum(task_values(:,39));
        
        % fraction of reduces
        if task_count > 0
            reduce_fraction = reduce_count / task_count;
        else
            reduce_fraction = NaN;
        end
        
        % fraction of failed tasks
        if task_count > 0
            fail_fraction = fail_count / task_count;
        else
            fail_fraction = NaN;
        end
        
        % fraction of aborted tasks
        if task_count > 0
            cancel_fraction = cancel_count / task_count;
        else
            cancel_fraction = NaN;
        end
        
        
        
        % inter-arrivaltime
        v = sort(task_values(:,3));
        task_values(1,3) = 0;
        last = v(1);
        
        for j = 2:length(v)
            task_values(j,3) = v(j)-last;
            last = v(j);
        end
        
        
        task_values_nonneg = task_values;
        task_values_nonneg(task_values<0) = NaN;
        
        fits = {};
        parfor d = 1:size(distributions,1)
            fit_handle = distributions{d,2};
            disp(func2str(fit_handle));
            if d == 1
%                 [fits{d}{1:nargout(fit_handle)}] = fit_handle(task_values);
            else
%                 [fits{d}{1:nargout(fit_handle)}] = fit_handle(task_values_nonneg);
            end
        end
        %task_fits{t,3} = fits;
        %task_fits{t,4} = task_values;
        
%       task_values = 
%           [
%               JobID_1   TaskID_1  InterArrivalTime_1  .... ;
%               JobID_2   TaskID_2  InterArrivalTime_2  .... ;
%           ]
%              
%        [mu, sigma] = normfit(task_values)
%
%       mu = [ mu_JobID mu_TaksID mu_InterArrivalTime ... ]
%       sigma = idem;
%

        %task_fits{t,3} = fit_prob_distributions(distributions, iat);
        
%         % runtimes
%         for p = 4:17
%             try
%                 v = tasks(tasks(:,1)==tid,p);       
%                 task_fits{t,p} = fit_prob_distributions(distributions, v);
%                 save('task_fits', 'task_fits');
%             end
%         end
        
        % correlation between disk and runtime?
        h = tasks(tasks(:,1)==tid,10);
        r = tasks(tasks(:,1)==tid,5);
        runtime_parameters = polyfit(h,r,1);
        
        record = {                              ...
            tid,                                ...
            task_count,                         ...
            reduce_fraction,                    ...
            fail_fraction,                      ...
            cancel_fraction,                     ...
            runtime_parameters,                 ...
            fits,                               ...
            task_values,                        ...
        };
    
        task_fits(t, 1:size(record, 2)) = record(:);
    end
    
    properties = task_fits;
end