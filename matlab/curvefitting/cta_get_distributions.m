function distributions = cta_get_distributions()
    distributions = {
        'Normal',      @normfit, @normpdf, @normcdf, @normrnd, 2, [ 4, .5; 5,  1; 6,  2],           @(x) true,     NaN;
        'Exponential', @expfit,  @exppdf,  @expcdf,  @exprnd,  1, [    .5;     1;   1.5],           @(x) x >= 0,     0; % is this > or >=?
        'Weibull',     @wblfit,  @wblpdf,  @wblcdf,  @wblrnd,  2, [ 1, .5; 2, 1; 3, 1.5],           @(x) x > 0,  1e-10;
        'Pareto',      @gpfit,   @gppdf,   @gpcdf,   @gprnd,   3, [ 1, .5, 3; 2, 1, 2; 3, 1.5, 1],  @(x) x > 0,  1e-10;   % moeilijk x >= mu als ksi >0, mu <= x <= mu - sigma / ksi anders... 
        'Log-Normal',  @lognfit, @lognpdf, @logncdf, @lognrdn, 2, [ 1, .5; 2, 1; 3, 1.5],           @(x) x > 0,  1e-10;
        'Gamma',       @gamfit,  @gampdf,  @gamcdf,  @gamrnd,  2, [ 1, .5; 2, 1; 3, 1.5],           @(x) x > 0,  1e-10;
    };
end