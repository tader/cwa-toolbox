function [dist_AD dist_KS] = gof_test(xdat, cdf_handle, varargin)
    % KS and AD tests for availability Goodness-of-fit for
    % any distribution 
    %
    % Bahman Javadi, INRIA, 2008-2009
    % Derrick Kondo, INRIA, 2007-

    % Input: 
    %	xdat: sample data
    %	cdf_handle: handle to cdf function
    %	(e.g. normcdf)
    %	varargin: parameters of fitted distribution

    % usage: gof_test (randn(1000,1), @normcdf, 0)
    
    SUB_SIZE = 30;
    NO = 1000; % default: 1000

    xdat(xdat<=0) = [];
    [n,~] = size(xdat);

    pv = [];
    p_ad=[];

    for i=1:NO
        % choose SUB_SIZE unique numbers 
        % - checked numbers are random each time
        %    r = rand_int(1, length(xdat), SUB_SIZE);

        %     if (mod(i,100)==0)
        %         disp (['Iteration ' int2str(i)])
        %     end

        %     n
        %     SUB_SIZE
        %     1        
        r = randi(round(n),round(SUB_SIZE),1);
        sub_dat = xdat(r);
        sub_dat_ks = sub_dat;
        [f,x]= ecdf(sub_dat);

        dis = call_cdf(cdf_handle, x, varargin{:});
        
        dis_ks = dis;
        [w,~] = size(f);
        sub_dat = [f,ones(w,1)];
        [k,~] = size(dis);
        dis = [dis, 2*ones(k,1)];
        mat = [sub_dat;dis];
        p_ad(i) = AnDartest(mat);
        [~,pv(i)] = kstest(sub_dat_ks, [x,dis_ks]);
    end

    dist_AD = mean(p_ad);
    dist_KS = mean(pv);
end
