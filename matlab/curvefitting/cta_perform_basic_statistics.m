function cta_perform_basic_statistics(properties, output)
    parfor d = 1:length(properties{1,2})  % loop for the different datasets
        values = [];
        
        for i = 1:size(properties,1)      % loop for the different properties in a dataset
            values(1:length(properties{i,3}{d}),i) = properties{i,3}{d};
        end
        
        cta_basic_statistics(properties{1,2}{d}, [ values ], properties(:,1), strcat(output, '/', properties{1,2}{d}, '-basic-statistics'));
    end
end