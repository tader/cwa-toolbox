function cta_perform_correlation_check(properties, output)
    % Find Correlations
    disp('Testing for correlations...')
    tic_corr = tic;
    
    parfor d = 1:length(properties{1,2})  % loop for the different datasets
        corr_table = DataTable();
        corr_table{1, 1} = 'property';

        for i = 2:size(properties,1)       % loop for the different properties in a dataset
            corr_table{i, 1} = properties{i,1};            
            i_values = properties{i,3}{d};

            for j = 1:size(properties,1)-1 % loop for the different properties in a dataset
                if 2 == i
                    corr_table{1, j+1} = properties{j,1};
                    corr_table.setCellFormat(1, j+1, 'v');
                end

                if i > j
                    j_values = properties{j,3}{d};
                    val = correlate(i_values, j_values);
                    
                    if ~isnan(val)
                        corr_table{i, j+1} = num2str(val, '%1.2f ');
                        if val > 0.1
                            corr_table.setCellFormat(i, j+1, 'b');
                        end
                    else
                        corr_table{i, j+1} = '?';
                    end
                end
            end
        end
    corr_table.toText;
    fid = fopen(strcat(output, '/', properties{1,2}{d}, '-correlations.tex'), 'w');
    corr_table.toLatex('tight', 'hline', fid);
    fclose(fid);
    end
    toc(tic_corr);    
end

function p = correlate(values_X, values_Y)
    N = min(length(values_X), length(values_Y));
    try
        p = corr(values_X(1:N),values_Y(1:N));
    catch
        p = NaN;
    end
end
