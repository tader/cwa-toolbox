function [] = fit_all()

    disp('Cleaning...');
    delete('~/Data/flashcrowd/btworld/openbt_2009/curvefitting_hm/*.*');
    delete('~/Data/flashcrowd/btworld/openbt_2010/curvefitting_hm/*.*');
    delete('~/Data/flashcrowd/btworld/extra_2009/curvefitting_hm/*.*');
    
    % openbt 2009
    disp('Processing OpenBT 2009...');
    fit_btworld('~/Data/flashcrowd/btworld/openbt_2009/result/swarm_properties_hm.dat', ...
        '~/Data/flashcrowd/btworld/openbt_2009/curvefitting_hm/')
    
    % openbt 2010
    disp('Processing OpenBT 2010...');
    fit_btworld('~/Data/flashcrowd/btworld/openbt_2010/result/swarm_properties_hm.dat', ...
        '~/Data/flashcrowd/btworld/openbt_2010/curvefitting_hm/')
    
    % extra tracker 2009
    disp('Processing ExtraTracker 2009...');
    fit_btworld('~/Data/flashcrowd/btworld/extra_2009/result/swarm_properties_hm.dat', ...
        '~/Data/flashcrowd/btworld/extra_2009/curvefitting_hm/')

end