function varargout = fit_distr(xdat, fit_handle)
% KS and AD tests for availability Goodness-of-fit for
% any distribution 
%
% Bahman Javadi, INRIA, 2008-2009
% Derrick Kondo, INRIA, 2007-

% Input: 
%	xdat: sample data
%	cdf_handle: handle to cdf function
%	(e.g. normcdf)
%	varargin: parameters of fitted distribution

% usage: gof_test (randn(1000,1), @normcdf, 0)

[varargout{1:nargout(fit_handle)}] = fit_handle(xdat);
