function [fit_results] = fit_prob_distributions(distributions, dat)
% Fit the data from the given file to well known distributions, perform
% the GOF tests, and writes the results to output tex and html files
% Arguments: 
%	file_path: full path of the file containing data to fit.
% parameterName: used in table headers in the output  files
% outputDirectory: directory to write the output files

    % Remove all NaN's from the data...
    dat(isnan(dat)) = [];    
    %%xdat(xdat<=0) = []; % <- necessary? I DON'T LIKE THIS...

    gof = {};

    for ind = 1:size(distributions,1);
        fit_handle = distributions{ind,2};
        cdf_handle = distributions{ind,4};
        sel_handle = distributions{ind,8};
        replacement = distributions{ind,9};
        
        % replace all nonfit values
        xdat = dat;
        xdat(~sel_handle(dat)) = replacement;
        xdat(isnan(xdat)) = [];
        
        if length(xdat) > 0
            % test for empty xdat!
            [f,x] = ecdf(xdat); %must be dat, not xdat! massaging alert! (but maybe D stat will fail otherwise?)

            %disp(strcat(['   - Fitting: ', func2str(fit_handle)]));

            tic_fit = tic;
                try
                [parameters{ind}{1:nargout(fit_handle)}] = fit_handle(xdat); %fit_distr(xdat, fit_handle);
                catch ME
                    cta_basic_statistics('failed fit', xdat, {func2str(fit_handle)});
                    rethrow(ME);
                end
                %disp(parameters{ind});
            time = toc(tic_fit);
            %disp(strcat(['       - Fitting time:     ', num2str(time), ' s']));

            tic_gof = tic;
                ad = 0;
                ks = 0;

                try
                [ ad ks ] = gof_test(dat, cdf_handle, parameters{ind});
                end
                try
                gof{ind} = [ks ad];
                end
            time = toc(tic_gof);
            %disp(strcat(['       - GOF time:         ', num2str(time), ' s']));

            dstat = tic;
                try
                mle = parameters{ind};            
                dis = call_cdf(cdf_handle, x, mle);
                end
                D{ind} = max(abs(dis - f));
            time = toc(dstat);
            %disp(strcat(['       - D-statistic time: ', num2str(time), ' s']));
            %disp(' ');
        end
    end
    
    fit_results = {gof, parameters, D};
end
