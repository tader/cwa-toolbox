function example_plots    
    if ismac
        output = '/Users/thomas/Documents/Studie/Output';
    else
        output = '/home/af10028/tu/cta-data/data/facebook/cta/curvefitting.test2';
    end

    range = 0:.1:10;
    distributions = cta_get_distributions();

    parfor d = 1:size(distributions,1)
        name       = distributions{d, 1};
        pdf_handle = distributions{d, 3};
        cdf_handle = distributions{d, 4};
        nump       = distributions{d, 6};
        parameters = distributions{d, 7};

        for x = 1:2
            if x == 1
                handle = pdf_handle;
                type   = 'PDF';
            else
                handle = cdf_handle;
                type   = 'CDF';
            end

            close all;
            h = figure('Visible', 'off');
            for i = 1:size(parameters,1)             
                if nump == 1
                    plot(range, handle(range, parameters(i,1)));
                elseif nump == 2
                    plot(range, handle(range, parameters(i,1), parameters(i,2)));
                else
                    plot(range, handle(range, parameters(i,1), parameters(i,2), parameters(i,3)));
                end
                hold all;
            end
            legend(num2str(parameters));
            saveas(h, strcat([output, '/Example ', name, ' ', type]), 'epsc2');
            saveas(h, strcat([output, '/Example ', name, ' ', type]), 'png');
        end
    end    
end