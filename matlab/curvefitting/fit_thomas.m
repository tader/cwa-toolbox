function [] = fit_thomas()
%     datasets = {
%         'Facebook', '/home/af10028/tu/cta-data/data/facebook/cta/traces/JobDump_May5_Oct31_2009';
%         'Yahoo',    '/home/af10028/tu/cta-data/data/yahoo_mcluster/cta/traces/yahoo-m';
%     };
    datasets = {
        'Facebook', '/Users/thomas/Documents/Studie/CTA/JobDump_May5_Oct31_2009';
        'Yahoo',    '/Users/thomas/Documents/Studie/CTA/yahoo-m';
    };

%     output = '/home/af10028/tu/cta-data/data/facebook/cta/curvefitting.test2';
    output = '/Users/thomas/Documents/Studie/Output';

    distributions = {
        {'Normal',      @normfit, @normpdf, @normcdf, @normrnd}
        {'Exponential', @expfit,  @exppdf,  @expcdf,  @exprnd}
        {'Weibull',     @wblfit,  @wblpdf,  @wblcdf,  @wblrnd}
        {'Pareto',      @gpfit,   @gppdf,   @gpcdf,   @gprnd}
        {'Log-Normal',  @lognfit, @lognpdf, @logncdf, @lognrdn}
        {'Gamma',       @gamfit,  @gampdf,  @gamcdf,  @gamrnd}
    };

    details       = {'KS AD', 'Parameters', 'D-Statistic'};

     
    % Load datasets
    [num_datasets ~] = size(datasets);
    data = {};
    cwt_data = {};
    for i = 1:num_datasets
        dataset_name = char(datasets(i,1));
        dataset_file = char(datasets(i,2));
        
        path_cwj = char(strcat(dataset_file, '.cwj'));
        path_cwt = char(strcat(dataset_file, '.cwt'));
        
        disp(strcat('Loading "', dataset_name, '" data:'));
        disp(strcat('   * Reading input file: "', path_cwj, '"'));
        
        tic_input = tic;
        data{i} = load(path_cwj);
        time = toc(tic_input);
        disp(strcat('     (', num2str(time), ' s)'));

        disp(strcat('   * Reading input file: "', path_cwt, '"'));
        
        tic_input = tic;
        cwt_data{i} = load(path_cwt);
        time = toc(tic_input);
        disp(strcat('     (', num2str(time), ' s)'));
        
        disp(char(' '));
    end

% CWJ_FIELDS = [
%     "JobID",
%     "SubmitTime",
%     "WaitTime",
%     "RunTime",
%     "CPUs",
%     "TotalWallClockTime",
%     "Memory",
%     "Network",
%     "Disk",
%     "Status",
%     "UserID",
%     "GroupID",
%     "ExecutableID",
%     "QueueID",
%     "PartitionID",
%     "JobProperties",
%     "StructuralChanges",
%     "StructuralChangeParams",
%     "DiskIORatio"
% ]
% 
% CWT_FIELDS = [
%     "JobID",
%     "TaskID",
%     "SubmitTime",
%     "WaitTime",
%     "RunTime",
%     "CPUs",
%     "TotalWallClockTime",
%     "Memory",
%     "Network",
%     "Disk",
%     "Status",
%     "ExecutableID",
%     "QueueID",
%     "PartitionID",
%     "StructuralChanges",
%     "StructuralChangeParams",
%     "DiskIORatio"
% ]    
    
    % calculate interarrival times
    interarrival = {};
    for i = 1:num_datasets
        sorted = data{i}(:,2); %sort(data{i}(:,2));
        interarrival{i} = zeros(length(sorted)-1,1);
        last = sorted(1);
        
        for j = 2:length(sorted)
            interarrival{i}(j-1) = sorted(j)-last;
            last = sorted(j);
        end
    end
    
    
    properties = {
        {
            'InterArrivalTime',   ...
            datasets(:,1),        ...
            interarrival          ...
        }

        {
            'WaitTime',           ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,3),     ...
                data{2}(:,3)      ...
            }                     ...
        }

        {
            'RunTime',            ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,4),     ...
                data{2}(:,4)      ...
            }                     ...
        }

        {
            'CPUs',               ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,5),     ...
                data{2}(:,5)      ...
            }                     ...
        }

        {
            'TotalWallClockTime', ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,6),     ...
                data{2}(:,6)      ...
            }                     ...
        }

        {
            'Memory',             ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,7),     ...
                data{2}(:,7)      ...
            }                     ...
        }

        {
            'Network',            ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,8),     ...
                data{2}(:,8)      ...
            }                     ...
        }

        {
            'Disk',               ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,9),     ...
                data{2}(:,9)      ...
            }                     ...
        }
        
        {
            'Status',             ...
            datasets(:,1),        ...
            {                     ...
                data{1}(:,10),    ...
                data{2}(:,10)     ...
            }                     ...
        }
    };


    %1 JobID 
    %2 SubmitTime 
    %3 WaitTime 
    %4 RunTime
    %5 CPUs 
    %6 TotalWallClockTime 
    %7 Memory 
    %8 Network 
    %9 Disk 
    %10 Status 
    %11 UserID 
    %12 GroupID 
    %13 ExecutableID 
    %14 QueueID 
    %15 PartitionID 
    %16 JobProperties 
    %17 StructuralChanges 
    %18 StructuralChangeParams 
    %19 DiskIORatio

    % Find Correlations
    disp('Testing for correlations...')
    tic_corr = tic;
    
    for d = 1:length(properties{1}{2})
        corr_table = DataTable();
        corr_table{1, 1} = 'property';

        for i = 2:length(properties)
            corr_table{i, 1} = properties{i}{1};            
            i_values = properties{i}{3}{d};

            for j = 1:length(properties)-1
                if 2 == i
                    corr_table{1, j+1} = properties{j}{1};
                    corr_table.setCellFormat(1, j+1, 'v');
                end

                if i > j
                    j_values = properties{j}{3}{d};
                    val = correlate(i_values, j_values);
                    
                    if ~isnan(val)
                        corr_table{i, j+1} = num2str(val, '%1.2f ');
                        if val > 0.1
                            corr_table.setCellFormat(i, j+1, 'b');
                        end
                    else
                        corr_table{i, j+1} = '?';
                    end
                end
            end
        end
    corr_table.toText;
    fid = fopen(strcat(output, '/', properties{1}{2}{d}, '-correlations.tex'), 'w');
    corr_table.toLatex('tight', 'hline', fid);
    fclose(fid);
    end
    toc(tic_corr);
    

    % Fit Distributions
    for i = 1:length(properties)
        fit_property(             ...
            properties{i}{1},     ...
            properties{i}{2},     ...
            properties{i}{3},     ...
            distributions,        ...
            details,              ...
            output                ...
        );
    end
end

    
    
function p = correlate(values_X, values_Y)
    N = min(length(values_X), length(values_Y));
    p = corr(values_X(1:N),values_Y(1:N));
end

function fit_property(property_name, value_names, values, distributions, details, output)
    disp(['Analyzing property: ', property_name]);
    % Fit properties
    results = cell(1,length(value_names));
    for index = 1:length(value_names)
        try
            results{index} = fit_prob_distributions(distributions, values{index});
        catch ME
            display(ME);
            %rethrow(ME);
            % Uncomment above ^^ if you want to see/retgrow the errors
        end
    end

    for d = 1:length(details)
        % Format results
        name = details{d};

        disp(name);

        table = DataTable();
        for i = 1:length(distributions)
            table{1, i+1} = distributions{i}{1};
        end

        for i = 1:length(value_names)
            table{i+1,1} = value_names(i);
            for j = 1:length(distributions)
                try
                    if d == 1
                        % KS / AD
                        table{i+1,j+1} = num2str(results{i}{d}{j}, '%1.2f ');

                        if min(results{i}{d}{j}) >= 0.05
                            table.setCellFormat(i+1, j+1, 'b');
                        end
                    elseif d == 2
                        % Parameters
                        table{i+1,j+1} = num2str(results{i}{d}{j}{1}, '%1.2f ');
                    else
                        % D-Statistic
                        table{i+1,j+1} = num2str(results{i}{d}{j}, '%1.2f ');
                    end
                catch ME
                    display(ME);
                    table{i+1,j+1} = '?';
                end
            end
        end

        table.toText;

        fid = fopen(strcat(output, '/', property_name, '_', name, '.tex'), 'w');
        table.toLatex('tight', 'hline', fid);
        fclose(fid);

        fid = fopen(strcat(output, '/', property_name, '_', name, '.html'), 'w');
        table.toHTML(fid);
        fclose(fid);
        
    end
    
    plot_graphs(property_name, value_names, values, distributions, details, output, results);
end

function plot_graphs(property_name, value_names, values, distributions, details, output, results)
    h = figure;

    for s = 1:length(values)
        try
            v = values{s};

            for i = 1:length(distributions)
                try
                    r = distributions{i}{5};
                    clf(h,'reset');
                    hold off;
                    
                    disp(func2str(r));
                    mle = results{s}{2}{i}{1};
                    if (strcmp(func2str(r),'gprnd'))
                        y = r(mle(1),mle(2),mle(2),length(v),1);
                    elseif strcmp(func2str(r),'normrnd')
                        mle2 = results{s}{2}{i}{2};
                        y = r(mle,mle2,length(v),1);
                    elseif strcmp(func2str(r),'lognrnd') ...
                        || strcmp(func2str(r),'wblrnd') ...
                        || strcmp(func2str(r),'gamrnd')
                        disp(func2str(r));
                        disp(mle);
                        y = r(mle(1),mle(2),length(v),1);
                    elseif (strcmp(func2str(r),'exprnd'))
                        y = r(mle,length(v),1);
                    end
%                     plot(sort(v),sort(y), 'o');
%                     xlabel('Emperical');
%                     ylabel('Fitted Distribution');
%                     axis equal;
                    qqplot(v, y);
                    saveas(h, strcat(output, '/qq-', value_names{s}, '-', property_name, '-', func2str(r)), 'epsc2');
                    saveas(h, strcat(output, '/qq-', value_names{s}, '-', property_name, '-', func2str(r)), 'png');
                catch ME
                    disp(ME);
                    %rethrow(ME);
                end
            end

            try
                clf(h,'reset');
                hold off;
                axes('XScale', 'log');
                hold all;
                [~, stats] = cdfplot(v);
                x = stats.min:.1:stats.max;
                %[f,x] = ecdf(v);
                mu = results{s}{2}{2}{1};
                plot(x, expcdf(x,mu));
            end

            try
                A = results{s}{2}{3}{1}(1);
                B = results{s}{2}{3}{1}(2);
                plot(x, wblcdf(x,A,B));
            end

            try
                K =     results{s}{2}{4}{1}(1);
                sigma = results{s}{2}{4}{1}(2);
                theta = sigma; %tmp(3); see gof_test
                plot(x, gpcdf(x,K,sigma,theta));
            end

            try
                mu =    results{s}{2}{1}{1}(1);
                sigma = results{s}{2}{1}{1}(2);
                plot(x, normcdf(x,mu,sigma));
            end

            try
                mu =    results{s}{2}{5}{1}(1);
                sigma = results{s}{2}{5}{1}(2);
                plot(x, logncdf(x,mu,sigma));
            end
            
            try
                A = results{s}{2}{6}{1}(1);
                B = results{s}{2}{6}{1}(2);
                plot(x, gamcdf(x,A,B));
            end

            legend('Emperical', 'Exponentional', 'Weibull', ...
                'Pareto', 'Normal', 'Log-Normal', 'Gamma', 'Location', 'SE');

            saveas(h, strcat(output, '/', value_names{s}, '-', property_name), 'epsc2');
            saveas(h, strcat(output, '/', value_names{s}, '-', property_name), 'png');

        catch ME
            display(ME);
            rethrow(ME);
            % Uncomment above ^^ if you want to see/retgrow the errors
        end

    
    end
    
    try
        close(h);
    end
end