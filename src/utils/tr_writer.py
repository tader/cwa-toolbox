#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Writer for the Cloud Trace Archive format.

This reader provides an iterator for a file in the Cloud Trace Archive format.
"""

__author__ = 'Thomas A. de Ruiter <thomas@de-ruiter.cx>'
__version__ = '11.2.25'
__date__ = '2011-02-25'

from progress_bar import ProgressBar
import bz2
import datetime
import gzip
import os
import sys

def toString(value):
    if value is None:
        return ""
    else:
        return str(value)

class TRWriter:
    """Cloud Trace file writer."""

    def __init__(self, filename):
        self.filename = filename
        self.progress = False
        self.column_titles = None
        self.bar = None
        self.i = 0

        if filename == "-":
            self.ofp = sys.stdout
        elif filename[-3:] == ".gz":
            self.ofp = gzip.open(self.filename, "w")
        elif filename[-4:] == ".bz2":
            self.ofp = bz2.BZ2File(self.filename, "w")
        else:
            self.ofp = open(self.filename, "w")

    def header(self, name, version, author):
        self.ofp.write("#\n")
        self.ofp.write("# Generated On: %s\n" % datetime.datetime.now().isoformat())
        self.ofp.write("#           By: %s\n" % name)
        self.ofp.write("#      Version: %s\n" % version)
        self.ofp.write("#       Author: %s\n" % author)
        self.ofp.write("#\n")
        self.ofp.write("#  Commandline: %s\n" % " ".join(sys.argv))
        self.ofp.write("#     Work Dir: %s\n" % os.getcwd())
        self.ofp.write("#\n")
        self.ofp.write("\n")

        if self.column_titles:
            self.ofp.write("\t".join(self.column_titles) + "\n")

    def writeRecord(self, record):
        items = []
        
        if isinstance(record, dict):
            if self.column_titles is not None:
                for col in self.column_titles:
                    if col in record:
                        items.append(record[col])
                    else:
                        items.append(None)
            else:
                items = record.values()
        else:
            items = record

        items = map(toString, items)        
        self.ofp.write("%s\n" % ("\t".join(items)))
        
        if self.progress:
            self.i += 1
            if self.i % 50000 == 0:
                self.bar.update(self.i)
        
    def close(self):
        self.ofp.close()
        if self.progress and self.bar is not None:
            self.bar.done()

    def write(self, iterator):
        if self.progress:
            try:
                length = len(iterator)
            except TypeError:
                length = 0
            self.bar = ProgressBar(length)

        for record in iterator:
            self.writeRecord(record)

        self.close()
