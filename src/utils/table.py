#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import math


def nf(number):
    if math.isnan(number):
        return '--'

    Number = math.fabs(number)

    if Number >= 10000000:
        fmt = '%.0e'
    elif Number >= 100:
        fmt = '%.0f'
    elif Number >= 10:
        fmt = '%.1f'
    elif Number >= 1:
        fmt = '%.2f'
    else:
        fmt = '%.3f'

    return fmt % float(number)


def pf(number):
    return "%.2f \\%%" % float(number)


def t(text, bold=False, convert=str):
    head = []
    body = [convert(text)]
    tail = []

    if bold:
        head.append('\\textbf{')
        tail.append('}')

    return ''.join(head + body + tail)


class Table:
    def __init__(self, filename=None):
        self.filename = filename
        self.contents = {}
        self.settings = {}
        self.height = 0
        self.width = 0
        self.last_cell = None

    def colspan(self, colspan):
        if self.last_cell is not None:
            if self.last_cell not in self.settings:
                self.settings[self.last_cell] = {}

            self.settings[self.last_cell]['colspan'] = colspan
            row, col = self.last_cell
            self.width = max(self.width, col + colspan - 1)

    def __setitem__(self, key, value):
        row, col = key
        self.height = max(self.height, row)
        self.width = max(self.width,  col)
        self.contents[row, col] = value
        self.last_cell = row, col

    def __getitem__(self, key):
        if key in self:
            try:
                return self.contents[key]
            except:
                return ''
        else:
            raise KeyError('Out of table size: %s' % str(key))

    def __delitem__(self, key):
        try:
            del self.contents[key]
        except:
            pass

    def __contains__(self, key):
        row, col = key
        return row <= self.height or col <= self.width

    def __len__(self):
        return self.height * self.width

    def write(self, filename=None, as_document=False):
        self.write_latex(filename, as_document)
        self.write_html(filename, as_document)

    def write_latex(self, filename=None, as_document=False):
        if filename is None:
            filename = self.filename

        fp = open('%s.tex' % filename, 'w')

        if as_document:
            fp.write('\\documentclass{report}\n\\begin{document}\n')
        fp.write('\\begin{tabular}{|%s|}\n\\hline\n' % self._col_alignment())

        row = 1

        while (row <= self.height):
            col = 1
            while (col <= self.width):
                w, t = self._format_cell(row, col)
                fp.write('\t%s' % t)
                col += w
                fp.write(' &\n' if col <= self.width else ' \\\\\n\\hline\n')
            row += 1

        fp.write('\\end{tabular}\n')
        if as_document:
            fp.write('\\end{document}')

        fp.close()

    def write_html(self, filename=None, as_document=False):
        if filename is None:
            filename = self.filename

        fp = open('%s.html' % filename, 'w')

        if as_document:
            fp.write('<html><head></head><body>\n')
        fp.write('<table border="1">\n')

        row = 1

        while (row <= self.height):
            fp.write('<tr>\n')
            col = 1
            while (col <= self.width):
                w, t = self._format_cell(row, col)
                fp.write('\t<td>%s</td>\n' % t)
                col += w
            row += 1
            fp.write('</tr>\n')

        fp.write('</table>\n')
        if as_document:
            fp.write('</body></html>')

        fp.close()

    def _format_cell(self, r, c):
        try:
            colspan = self.settings[r, c]['colspan']
        except:
            colspan = 1

        if colspan > 1:
            return colspan, '\\multicolumn{%d}{|c|}{%s}' % (
                colspan,
                self[r, c]
            )
        else:
            return 1, self[r, c]

    def _col_alignment(self):
        return '|'.join(['c'] * self.width)
