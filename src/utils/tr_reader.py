#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Reader for the Cloud Trace Archive format.

This reader provides an iterator for a file in the Cloud Trace Archive format.
"""

__author__ = 'Thomas A. de Ruiter <thomas@de-ruiter.cx>'
__version__ = '0.1'
__date__ = '2011-02-25'

import gzip
import bz2
from progress_bar import ProgressBar
import os
import sys
import stat

class TRReader:
    """Cloud Trace file reader."""
    # pylint: disable-msg=R0903

    def __init__(self, filename):
        self.filename = filename
        self.return_as_dict = False
        self.max_columns = -1
        self.header = True
        self.progress = False
        self.multi_line = False
        self.PROGRESS_UPDATE_LINES = 5000

    def __iter__(self):
        return TRReaderIterator(self, self.filename)


class TRReaderIterator:
    """Iterator for the Cloud Trace file."""
    # pylint: disable-msg=R0903

    def __init__(self, reader, filename):
        self.reader = reader
        self.filename = filename
        self.compress_ratio = 1
        self.line_number = 0
        self.PROGRESS_UPDATE_LINES = self.reader.PROGRESS_UPDATE_LINES
        
        if self.filename != "-":
            if not os.path.exists(filename) and os.path.exists("%s.gz" % self.filename):
                self.filename = "%s.gz" % self.filename

        if self.filename == "-":
            self.ifp = sys.stdin
        elif self.filename[-3:] == ".gz":
            self.ifp = gzip.open(self.filename, 'r')
            self.compress_ratio = 13
        elif self.filename[-4:] == ".bz2":
            self.ifp = bz2.BZ2File(self.filename, 'r')
            self.compress_ratio = 20
        else:
            self.ifp = open(self.filename, 'r')

        if self.reader.progress:
            self.progress_bar = ProgressBar(os.stat(self.filename)[stat.ST_SIZE] *
                                            self.compress_ratio)

        if self.reader.header:
            self.column_headers = self.__next_in_parts()
        else:
            self.reader.return_as_dict = False

    def __iter__(self):
        return self

    def __next_in_parts(self):
        """Return the next line as a list of parts."""
        if self.reader.multi_line:
            buffer = ""

        while True:
            line = self.ifp.readline()
            if not line:
                self.ifp.close()
                if self.reader.progress:
                    self.progress_bar.done()
                raise StopIteration

            self.line_number += 1
            line_stripped = line.strip()
            line = line.rstrip()

            if self.reader.progress and self.line_number % self.PROGRESS_UPDATE_LINES == 0:
                    self.progress_bar.update(self.ifp.tell())

            if len(line_stripped) == 0 or line_stripped[0] == '#':
                continue

            if self.reader.multi_line:
                if line[-2:] == " \\":
                    buffer += line[:-2]
                    continue
                else:
                    line = buffer + line

            return line.split('\t', self.reader.max_columns)

    def next(self):
        """Get the next record of the file.

        Returns:
            The next record in the cloud trace file either as a list or a dictionary.
            If the TRReader.returnAsDict is True, a dictionary is returned the names in the header row are used as keys for the fields.
            If this value is set False a list of values is returned.

        Raises:
            StopIteration: When the end of the input file is reached.
        """

        if self.reader.return_as_dict:
            record = dict(zip(self.column_headers, self.__next_in_parts()))
            record['__line__'] = self.line_number
            return record
        else:
            return self.__next_in_parts()
