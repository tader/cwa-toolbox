#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Serializer to solve the 2 GiB / 4 GiB limits of the cStringIO and the 
pickle protocol.

Test:

>>> save([b'a' * (2^31 - 10) for _ in xrange(3)], 'test')
>>> load('test') == [b'a' * (2^31 - 10) for _ in xrange(3)]
True

Example usage:

>>> save([1,2,3], 'test')
>>> load('test')
[1, 2, 3]

Clean up:

>>> import os
>>> os.remove('test')
"""

import gzip
import logging

try:
    import cPickle as pickle
except:
    import pickle

def save(data, file):
    f = gzip.open(file, 'wb', 1)
    _save_raw(('THOMSOFT PICKLER', 1.0), f)
    _save(data, f)
    f.close()

def _save(data, f):
    try:
        _save_any(data, f)
    except Exception as e:
        logging.exception(e)

        try:
            handle = {
                'list': _save_list,
                'tupe': _save_tuple,
                'dict': _save_dict
            }.get(type(data), _save_any)

            handle(data, f)
        except Exception as e:
            logging.exception(e)

def _save_raw(data, f):
    pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def _save_any(data, f):
    _save_raw(('A', 1), f)
    _save_raw(data, f)

def _save_list(data, f):
    _save_raw(('L',len(data)), f)
    map(lambda i: _save(i, f), data)

def _save_tuple(data, f):
    _save_raw(('T',len(data)), f)
    map(lambda i: _save(i, f), data)

def _save_dict(data, f):
    _save_raw(('D',len(data)), f)
    map(lambda i: _save(i, f), data.items())

def load(file):
    f = gzip.open(file, 'rb')
    if _load_raw(f) != ('THOMSOFT PICKLER', 1.0):
        raise Exception('Input not in tsplicker format')

    return _load(f)

def _load(f):
    t, l = _load_raw(f)

    handle = {
        'A': _load_any,
        'L': _load_list,
        'T': _load_tuple,
        'D': _load_dict
    }.get(t)

    return handle(f, l)

def _load_raw(f):
    return pickle.load(f)

def _load_any(f, l):
    return _load_raw(f)

def _load_list(f, l):
    return [_load(f) for _ in xrange(l)]

def _load_tuple(f, l):
    return tuple([_load(f) for _ in xrange(l)])

def _load_dict(f, l):
    return dict([_load(f) for _ in xrange(l)])

if __name__ == '__main__':
    import doctest
    doctest.testmod()

