#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Writer for the Mumak trace format.
"""

__author__ = 'Thomas A. de Ruiter <thomas@de-ruiter.cx>'
__version__ = '0.1'
__date__ = '2011-10-27'

import sys
import json
import re
import logging
from datetime import datetime

class Job:
    SCALE_TIME = 1000

    # jobtype enum  (from LoggedJob.java)
    TYPE_JAVA = "JAVA"
    TYPE_PIG = "PIG"
    TYPE_STREAMING = "STREAMING"
    TYPE_PIPES = "PIPES"
    TYPE_OVERALL = "OVERALL"

    # priority enum  (from LoggedJob.java)
    PRIORITY_VERY_LOW = "VERY_LOW"
    PRIORITY_LOW = "LOW"
    PRIORITY_NORMAL = "NORMAL"
    PRIORITY_HIGH = "HIGH"
    PRIORITY_VERY_HIGH = "VERY_HIGH"

    # values enum (from Pre21JobHistoryConstants.java)
    OUTCOME_SUCCESS = "SUCCESS"
    OUTCOME_FAILED = "FAILED"
    OUTCOME_KILLED = "KILLED"
    OUTCOME_MAP = "MAP"
    OUTCOME_REDUCE = "REDUCE"
    OUTCOME_CLEANUP = "CLEANUP"
    OUTCOME_RUNNING = "RUNNING"
    OUTCOME_PREP = "PREP"
    OUTCOME_SETUP = "SETUP"

    JSON_ENCODE = [
        "priority",
        "jobID",
        "jobName",
        "user",
        "computonsPerMapInputByte",
        "computonsPerMapOutputByte",
        "computonsPerReduceInputByte",
        "computonsPerReduceOutputByte",
        "submitTime",
        "launchTime",
        "finishTime",
        "heapMegabytes",
        "totalMaps",
        "totalReduces",
        "outcome",
        "jobtype",
        "directDependantJobs",
        "mapTasks",
        "reduceTasks",
        "otherTasks",
        "successfulMapAttemptCDFs",
        "failedMapAttemptCDFs",
        "successfulReduceAttemptCDF",
        "failedReduceAttemptCDF",
        "mapperTriesToSucceed",
        "failedMapperFraction",
        "relativeTime",
        "memForMapTasks",
        "memForReduceTasks",
        "jobMapMB",
        "jobReduceMB",
        "clusterMapMB",
        "clusterReduceMB",
    ]

    counter_jobID = 0

    def __init__(self):
        Job.counter_jobID += 1

        self._jobID_dateTime = 0
        self._jobID_serial = Job.counter_jobID
        self._total_tasks = 0
        self._total_attempts = 0
        self._total_runtime = 0
        self._task_index = {}

        self.jobID = "job_0_0"
        self.jobName = "synthetic job %d" % Job.counter_jobID
        self.priority = "NORMAL"
        self.user = "user"
        self.computonsPerMapInputByte = 0
        self.computonsPerMapOutputByte = 0
        self.computonsPerReduceInputByte = 0
        self.computonsPerReduceOutputByte = 0
        self.submitTime = 0
        self.launchTime = 0
        self.finishTime = 0
        self.heapMegabytes = 0
        self.totalMaps = 0
        self.totalReduces = 0
        self.outcome = Job.OUTCOME_SUCCESS
        self.jobtype = Job.TYPE_JAVA
        self.directDependantJobs = []
        self.mapTasks = []
        self.reduceTasks = []
        self.otherTasks = []
        self.successfulMapAttemptCDFs = [CDF(), CDF(), CDF(), CDF()]
        self.failedMapAttemptCDFs = [CDF(), CDF(), CDF(), CDF()]
        self.successfulReduceAttemptCDF = CDF()
        self.failedReduceAttemptCDF = CDF()
        self.mapperTriesToSucceed = [1]
        self.failedMapperFraction = 0
        self.relativeTime = 0
        self.memForMapTasks = 0
        self.memForReduceTasks = 0
        self.jobMapMB = 0
        self.jobReduceMB = 0
        self.clusterMapMB = 0
        self.clusterReduceMB = 0

    def updateJobID(self):
        try:
            if self.submitTime >= 0:
                t = datetime.fromtimestamp(self.submitTime / Job.SCALE_TIME)
                self._jobID_dateTime = int(t.strftime("%Y%m%d%H%M"))
        except:
            pass

        self.jobID = "job_%d_%04d" % (self._jobID_dateTime, self._jobID_serial)

    def addTasks(self, tasks):
        for task in tasks:
            self.addTask(task)

    def addTask(self, task):
        task._job = self

        if task.taskType == Task.TYPE_MAP:
            self.mapTasks.append(task)
            self.totalMaps += 1
        elif task.taskType == Task.TYPE_REDUCE:
            self.reduceTasks.append(task)
            self.totalReduces += 1
        else:
            self.otherTasks.append(task)

        self._total_tasks += 1
        self._total_attempts += task._total_attempts
        self._total_runtime += task._total_runtime

        self._task_index[task.taskID] = task

    def getWaitTime(self):
        return (
            (self.launchTime - self.submitTime) / Job.SCALE_TIME * 1.0
            if min(self.launchTime, self.submitTime) > 0
            else None
        )

    def getRunTime(self):
        return (
            (self.finishTime
            - (self.launchTime
               if self.launchTime > self.submitTime
               else self.submitTime))
            / Job.SCALE_TIME * 1.0
        )

    def getTasks(self):
        tasks = []
        tasks.extend(self.mapTasks)
        tasks.extend(self.reduceTasks)
        tasks.extend(self.otherTasks)
        return tasks

    def getTask(self, taskID):
        try:
            return self._task_index[taskID]
        except:
            return None

    def getAttempt(self, taskID, attemptID):
        try:
            return self.getTask(taskID).getAttempt(attemptID)
        except:
            return None

    def __repr__(self):
        return "Job: %s (Submit: %d, Launch: %d, Finish: %d)" % (
            self.jobID,
            self.submitTime,
            self.launchTime,
            self.finishTime
        )

    @classmethod
    def constructFromData(cls, data):
        job = cls()

        for event in data:
            eventType = event['type']
            logging.debug("Event: %s" % str(event))

            try:
                method = getattr(job, "handle_%s" % eventType)
            except Exception as e:
                logging.exception(e)
            else:
                method(event['event'])

            logging.debug(job)

        return job

    def handle_JOB_SUBMITTED(self, event):
        event = event.values().pop()
        self.jobID = event['jobid']
        self.jobName = event['jobName']
        self.user = event['userName']
        self.submitTime = int(event['submitTime'])

    def handle_JOB_STATUS_CHANGED(self, event):
        pass #ignore this

    def handle_JOB_PRIORITY_CHANGED(self, event):
        pass #ignore this

    def handle_JOB_INITED(self, event):
        pass #ignore this

    def handle_TASK_STARTED(self, event):
        event = event.values().pop()
        task = Task()
        task.taskID = event['taskid']
        task.startTime = int(event['startTime'])
        task.taskType = event['taskType']
        self.addTask(task)

        if self.launchTime == 0:
            self.launchTime = task.startTime
        else:
            self.launchTime = min(self.launchTime, task.startTime)

    def handle_MAP_ATTEMPT_STARTED(self, event):
        event = event.values().pop()
        task = self.getTask(event['taskid'])
        if task is None:
            logging.error("TaskAttemptStarted event for '%s' which was not started!" % event['taskid'])
            return

        if task.taskType != event['taskType']:
            logging.error(
                "TastAttempt type (%s) differs from Task (%s)" % (
                event['taskType'],
                task.taskType))

        attempt = Attempt()
        attempt.attemptID = str(event['attemptId'])
        attempt.startTime = int(event['startTime'])
        task.addAttempt(attempt)

    def handle_MAP_ATTEMPT_FINISHED(self, event):
        event = event.values().pop()
        attempt = self.getAttempt(
            event['taskid'],
            event['attemptId']
        )
        if attempt is None:
            return

        attempt.finishTime = int(event['mapFinishTime'])
        attempt.result = event['taskStatus']
        if attempt.result == "SUCCEEDED": attempt.result = Task.TYPE_SUCCESS

        m = re.match(r'/.+/(.*)$', event['hostname'])
        if m is not None:
            attempt.hostName = re.sub(r'\\(.)', r'\1', m.group(1))
        # Various Counters

    def handle_REDUCE_ATTEMPT_STARTED(self, event):
        self.handle_MAP_ATTEMPT_STARTED(event)

    def handle_REDUCE_ATTEMPT_FINISHED(self, event):
        event = event.values().pop()
        attempt = self.getAttempt(
            event['taskid'],
            event['attemptId']
        )
        if attempt is None:
            return

        attempt.finishTime = int(event['finishTime'])
        attempt.shuffleFinished = int(event['shuffleFinishTime'])
        attempt.result = event['taskStatus']
        if attempt.result == "SUCCEEDED": attempt.result = Task.TYPE_SUCCESS

        m = re.match(r'/.+/(.*)$', event['hostname'])
        if m is not None:
            attempt.hostName = re.sub(r'\\(.)', r'\1', m.group(1))
        # Various Counters

    def handle_TASK_FINISHED(self, event):
        event = event.values().pop()
        task = self.getTask(event['taskid'])
        if task is None:
            logging.error("TaskFinished event for '%s' which was not started!" % event['taskid'])
            return

        task.finishTime = int(event['finishTime'])
        task.taskStatus = event['status']
        if task.taskStatus == "SUCCEEDED": task.taskStatus = Task.TYPE_SUCCESS
        # Various Counters

        if task.finishTime > self.finishTime: self.finishTime = task.finishTime

    def handle_JOB_FINISHED(self, event):
        event = event.values().pop()
        self.finishTime = int(event['finishTime'])
        if self.jobID != event['jobid']:
            logging.error("Handling job finished event for an other job!")

class Task:
    # values enum (from Pre21JobHistoryConstants.java)
    TYPE_SUCCESS = "SUCCESS"
    TYPE_FAILED = "FAILED"
    TYPE_KILLED = "KILLED"
    TYPE_MAP = "MAP"
    TYPE_REDUCE = "REDUCE"
    TYPE_CLEANUP = "CLEANUP"
    TYPE_RUNNING = "RUNNING"
    TYPE_PREP = "PREP"
    TYPE_SETUP = "SETUP"

    SHORT_TYPE_LOOKUP = {
        TYPE_MAP:       "m",
        TYPE_REDUCE:    "r"
    }

    # values enum (from Pre21JobHistoryConstants.java)
    STATUS_SUCCESS = "SUCCESS"
    STATUS_FAILED = "FAILED"
    STATUS_KILLED = "KILLED"
    STATUS_MAP = "MAP"
    STATUS_REDUCE = "REDUCE"
    STATUS_CLEANUP = "CLEANUP"
    STATUS_RUNNING = "RUNNING"
    STATUS_PREP = "PREP"
    STATUS_SETUP = "SETUP"
    STATUS_UNDEFINED = None


    JSON_ENCODE = [
        "startTime",
        "finishTime",
        "inputBytes",
        "inputRecords",
        "outputBytes",
        "outputRecords",
        "taskID",
        "attempts",
        "preferredLocations",
        "taskStatus",
        "taskType",
    ]

    counter_taskID = 0

    def __init__(self):
        Task.counter_taskID += 1

        self._job = None
        self._taskID_serial = Task.counter_taskID
        self._total_attempts = 0
        self._total_runtime = 0
        self._attempt_index = {}

        self.startTime = 0.0
        self.finishTime = 0.0
        self._startTime = None  # calculated from attempts
        self._finishTime = None # calculated from attempts
        self.inputBytes = 0
        self.inputRecords = 0
        self.outputBytes = 0
        self.outputRecords = 0
        self.taskID = "task_0_0_m_0"
        self.attempts = []
        self.preferredLocations = []
        self.taskStatus = Task.TYPE_SUCCESS
        self.taskType = Task.TYPE_MAP

    def shortTaskType(self):
        if self.taskType in Task.SHORT_TYPE_LOOKUP:
            return Task.SHORT_TYPE_LOOKUP[self.taskType]
        else:
            return "o"

    def updateTaskID(self):
        jobID_dateTime = 0
        jobID_serial = 0

        if self._job:
            jobID_dateTime = self._job._jobID_dateTime
            jobID_serial = self._job._jobID_serial

        self.taskID = "task_%d_%04d_%s_%04d" % (
            jobID_dateTime,
            jobID_serial,
            self.shortTaskType(),
            self._taskID_serial
        )

    def addAttempt(self, attempt):
        attempt._task = self
        attempt._attemptID_serial = len(self.attempts)
        self.attempts.append(attempt)
        self._total_attempts += 1
        self._total_runtime += attempt.finishTime - attempt.startTime
        self._attempt_index[attempt.attemptID] = attempt

        self._startTime = min(self._startTime, attempt.startTime) if self._startTime is not None else attempt.startTime
        self._finishTime = max(self._finishTime, attempt.finishTime) if self._finishTime is not None else attempt.finishTime

    def getStartTime(self):
        return self._startTime if self._startTime is not None else self.startTime

    def getWaitTime(self):
        return 0.0

    def getRunTime(self):
        if self._startTime is None or self._finishTime is None or self._finishTime < self._startTime:
            return ((self.finishTime - self.startTime) / Job.SCALE_TIME * 1.0) - self.getWaitTime()
        else:
            return ((self._finishTime - self._startTime) / Job.SCALE_TIME * 1.0) - self.getWaitTime()

    def getAttempt(self, attemptID):
        try:
            return self._attempt_index[attemptID]
        except:
            return None

    def __repr__(self):
        return "Task: %s" % self.taskID


class Attempt:
    JSON_ENCODE = [
        "location",
        "hostName",
        "startTime",
        "finishTime",
        "result",
        "shuffleFinished",
        "sortFinished",
        "attemptID",
        "hdfsBytesRead",
        "hdfsBytesWritten",
        "mapInputRecords",
        "mapOutputBytes",
        "mapOutputRecords",
        "combineInputRecords",
        "reduceInputGroups",
        "reduceInputRecords",
        "reduceShuffleBytes",
        "reduceOutputRecords",
        "spilledRecords"
    ]

    def __init__(self):
        self._attemptID_serial = 0
        self.location = None
        self.hostName = None
        self.startTime = 0.0
        self.finishTime = 0.0
        self.result = Task.STATUS_SUCCESS
        self.shuffleFinished = 0
        self.sortFinished = 0
        self.attemptID = 0 # taskID_0
        self.hdfsBytesRead = 0
        self.hdfsBytesWritten = 0
        self.mapInputRecords = 0
        self.mapOutputBytes = 0
        self.mapOutputRecords = 0
        self.combineInputRecords = 0
        self.reduceInputGroups = 0
        self.reduceInputRecords = 0
        self.reduceShuffleBytes = 0
        self.reduceOutputRecords = 0
        self.spilledRecords = 0

    def updateAttemptID(self):
        jobID_dateTime = 0
        jobID_serial = 0
        taskID_serial = 0
        taskID_type = "o"

        if self._task:
            taskID_serial = self._task._taskID_serial
            taskID_type = self._task.shortTaskType()
            if self._task._job:
                jobID_dateTime = self._task._job._jobID_dateTime
                jobID_serial = self._task._job._jobID_serial

        self.attemptID = "attempt_%d_%04d_%s_%04d_%d" % (
            jobID_dateTime,
            jobID_serial,
            taskID_type,
            taskID_serial,
            self._attemptID_serial
        )

    def getRunTime(self):
        return (self.finishTime - self.startTime) / Job.SCALE_TIME * 1.0

    def __repr__(self):
        return "Attempt: %s" % self.attemptID

class CDF:
    JSON_ENCODE = [
        "maximum",
        "minimum",
        "rankings",
        "numberValues"
    ]

    def __init__(self):
        self.maximum = 9223372036854775807
        self.minimum = -9223372036854775808
        self.numberValues = 0
        self.rankings = []

class Formatter:
    def __init__(self, output_file = sys.stdout):
        self.output_file = output_file

    def o(self, data):
        print >>self.output_file, data

    def format_trace(self, jobs):
        for job in jobs:
            self.format_job(job)

    def format_job(self, job):
        self.o(json.dumps(job, cls=MumakEncoder))

class Reader:
    def __init__(self, input_file = sys.stdin):
        self.input_file = input_file

    def create_iterator(self):
        for buf in self.input_file:
            if len(buf) < 1: continue
            data = json.loads(buf)
            yield self.read_job(data)

    def read_job(self, data):
        job = Job()
        for attr in Job.JSON_ENCODE:
            if attr in ["mapTasks", "reduceTasks", "otherTasks"]:
                for task_data in data[attr]:
                    job.addTask(self.read_task(task_data))

        # Adding tasks changes things... So do this later...
        for attr in Job.JSON_ENCODE:
            if attr not in ["mapTasks", "reduceTasks", "otherTasks"]:
                setattr(job, attr, data[attr])

        return job

    def read_task(self, data):
        task = Task()
        for attr in Task.JSON_ENCODE:
            if attr in ["attempts"]:
                for attempt_data in data[attr]:
                    task.addAttempt(self.read_attempt(attempt_data))

        # Adding attempts changes things... So do this later...
        for attr in Task.JSON_ENCODE:
            if attr not in ["attempts"]:
                setattr(task, attr, data[attr])

        return task

    def read_attempt(self, data):
        attempt = Attempt()
        for attr in Attempt.JSON_ENCODE:
            setattr(attempt, attr, data[attr])
        return attempt

class MumakEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Job):
            enc = {}
            obj.updateJobID()
            for attr in Job.JSON_ENCODE:
                enc[attr] = getattr(obj, attr)
            return enc
        elif isinstance(obj, Task):
            enc = {}
            obj.updateTaskID()
            for attr in Task.JSON_ENCODE:
                enc[attr] = getattr(obj, attr)
            return enc
        elif isinstance(obj, Attempt):
            enc = {}
            obj.updateAttemptID()
            for attr in Attempt.JSON_ENCODE:
                enc[attr] = getattr(obj, attr)
            return enc
        elif isinstance(obj, CDF):
            enc = {}
            for attr in CDF.JSON_ENCODE:
                enc[attr] = getattr(obj, attr)
            return enc
        else:
            return json.JSONEncoder.default(self, obj)

if __name__ == "__main__":
    j1 = Job()
    j1.addTasks([Task(), Task()])
    jobs = [j1, Job()]

    f = Formatter()
    f.format_trace(jobs)

