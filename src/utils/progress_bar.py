#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


#  Corey Goldberg - 2010
#  ascii command-line progress bar with percentage and elapsed time display
# 

import sys

if sys.platform.lower().startswith('win'):
    on_windows = True
else:
    on_windows = False


class ProgressBar:
    def __init__(self, duration, increment_step = 1):
        self.duration = duration
        self.increment_step = increment_step
        self.prog_bar = '[]'
        self.fill_char = '>'
        self.width = 70
        self.current = 0
        self.update(0)

    def done(self):
        self.duration = 100
        self.update(100)
        print

    def update(self, value):
        self.update_value(value)

        if on_windows:
            print self, '\r'
        else:
            print self, chr(27) + '[A'

    def increment(self):
        self.update(self.current + self.increment_step)

    def update_value(self, value):
        self.current = value
        if self.duration != 0:
            percent_done = round(self.current * 100.0 / self.duration)
        else:
            percent_done = 0
        all_full = self.width - 2
        num_hashes = int(percent_done / 100.0 * all_full)
        
        if self.duration != 0:
            self.prog_bar = '[' + self.fill_char * num_hashes + ' ' * (all_full -
    num_hashes) + ']'
            self.prog_bar += ' %3d%%' % percent_done
        else:
            pos = value % (all_full - 1);
            self.prog_bar = '[' + ' ' * pos + self.fill_char + ' ' * (all_full - 1) + ']' 
            

    def __str__(self):
        return str(self.prog_bar)




if __name__ == '__main__':
    """ example usage """

    p = ProgressBar(60)
    p.update(5)
    p.update(15)



"""
example output:


$ python progress_bar.py 

static progress bar:
[##########       25%                  ]  15s/60s

static progress bar:
[=================83%============      ]  25s/30s


dynamic updating progress bar:

please wait 10 seconds...

[################100%##################]  10s/10s
done

"""
