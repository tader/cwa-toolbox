#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""
Aggregate timed data

The TimeLine class is intended to be used to generate statistical data
from log files. It can for example be used to analyze the number of
active users over time.
"""

from utils.ai_statistics import CStats
import logging

class TimeLine:
    """
    The TimeLine class is intended to be used to generate statistical
    data from log files. It can for example be used to analyze the
    number of active users over time.

    Example usage:
        >>> from utils.time_line import TimeLine
        >>> t = TimeLine(600)
        >>> t.add(0, 10)
        >>> t.add(20, 20)
        >>> t.add(3700, 10)
        >>> for value in t.timeline(3600):
        ...     print value
        ...
        (0, 30)
        (3600, 10)
    """
    def __init__(self, binwidth=1):
        self._binwidth = binwidth # binwidth used in the time line
        self._time_line = {}      # dict to store the time line
        self._min_time = None     # min and max values to keep track of
        self._max_time = None     #     the timespan of the time line
        self._keys = set()        # set to store all used keys
        self.use_cstats = False   # use AI's CStats
        self.keep_values = False  # let CStats keep values
        self.keep_all_time = False
        self._all_time = {}

    def add(self, time, value, key=None):
        """
        Add data to the time line. Based on the time argument, the
        corresponding bin is chosen, and the value is added.

        Arguments:
            time -- The time indicates the time corresponding to the
                <value>. This <time> is used to calculate the bin to
                which the <value> is added.

            value -- The value to store on the time line.

        Returns:
            None
        """
        line_time = int(time / self._binwidth)

        # keep track of the timespan of the time line
        if self._min_time is None:
            self._min_time = line_time
            self._max_time = line_time
        self._min_time = min(self._min_time, line_time)
        self._max_time = max(self._max_time, line_time)

        # keep track of used keys
        self._keys.add(key)

        # add the value to a previous value, or if non existent, set the
        # value to the current value
        try:
            line_time_item = self._time_line[line_time]
        except KeyError:
            self._time_line[line_time] = {}
            line_time_item = self._time_line[line_time]


        if key in line_time_item.keys():
            if self.use_cstats:
                line_time_item[key].addValue(value)
            else:
                line_time_item[key] += value
        else:
            if self.use_cstats:
                cs = CStats()
                cs.bKeepValues = self.keep_values
                cs.addValue(value)
                line_time_item[key] = cs
            else:
                line_time_item[key] = value

        if self.keep_all_time:
            if key in self._all_time:
                if self.use_cstats:
                    self._all_time[key].addValue(value)
                else:
                    self._all_time[key] += value
            else:
                if self.use_cstats:
                    cs = CStats()
                    cs.bKeepValues = self.keep_values
                    cs.addValue(value)
                    self._all_time[key] = cs
                else:
                    self._all_time[key] = value

    def all_time(self, key):
        if key in self._all_time:
            return self._all_time[key]

    def _value(self, time):
        """
        Get the value stored in the time line dict.

        Arguments:
            time -- The time (in line time units, which is
                time/binwidth) for which to get the current value.

        Returns:
            The value for <time> stored in the time line dict. Or return
            0 if <time> is not stored in the dict.
        """
        values = {}

        if time in self._time_line:
            values = self._time_line[time]

        return values

    def value(self, time, keys=None, binwidth=None):
        """
        Calculate the value in a bin of <binwidth> at moment <time> on
        the time line.

        Arguments:
            time -- The time indicates the bin for which the value is
                requested. As a bin spans a period of time, multiple
                values for <time> can give the value for the same bin.

            binwidth -- The width of the bins, this value MUST be a
                multiple of the binwidth specified when instantiating
                the TimeLine object. A None value for this argument will
                cause the object's binwidth to be used. This default
                value of this argument is None.

        Returns:
            The total value of the indicated bin is returned.

        Example usage:
            >>> from utils.time_line import TimeLine
            >>> t = TimeLine(600)
            >>> t.add(0, 10)
            >>> t.add(20, 20)
            >>> t.add(1300, 40)
            >>> t.value(10, 1200)
            30
            >>> t.value(1400, 1200)
            40
        """
        if keys is None:
            keys = self._keys

        values = {}

        offset = int(time / self._binwidth)
        moment_count = int(binwidth / self._binwidth)

        if moment_count > 1:
            for moment in xrange(offset, offset + moment_count):
                moment_values = self._value(moment)
                for key in keys:
                    if key in moment_values:
                        if key not in values:
                            if self.use_cstats:
                                values[key] = CStats()
                            else:
                                values[key] = 0

                        values[key] += moment_values[key]
        else:
            values = self._value(offset)

        if keys == [None]:
            if None in values:
                return values[None]
            else:
                return None
        else:
            return values


    def timeline(self, keys=None, binwidth=None):
        """
        A generator to iterate over all possible bins of width
        <binwidth> on this time line.

        Arguments:
            binwidth -- The width of the bins, this value MUST be a
                multiple of the binwidth specified when instantiating
                the TimeLine object. A None value for this argument will
                cause the object's binwidth to be used. The default
                value of this argument is None.

        Returns:
            This generator yields tuples of a bin timestamp and a bin
            value. The timestamp indicates the start time of the bin.

        Example usage:
            >>> from utils.time_line import TimeLine
            >>> t = TimeLine(600)
            >>> t.add(0, 10)
            >>> t.add(20, 20)
            >>> t.add(3700, 10)
            >>> for value in t.timeline(3600):
            ...     print value
            ...
            (0, 30)
            (3600, 10)
        """
        # default to the object's binwidth if no binwidth specified
        if binwidth is None:
            binwidth = self._binwidth

        # check if the specified binwidth is a multiple of the object's
        if binwidth % self._binwidth != 0:
            raise Exception("Incorrect binwidth: %d % %d != 0" %
                (binwidth, self._binwidth)
            )

        if self._min_time is None:
            logging.error("self._min_time is None")
            return

        if self._binwidth is None:
            logging.error("self._binwidth is None")
            return

        # calulate the time line timespan in normal time units
        time_start = self._min_time * self._binwidth
        time_end = self._max_time * self._binwidth
        time = time_start

        # yield tuple for every bin of size <binwidth> on the time line
        while time <= time_end:
            yield (time, self.value(time, keys, binwidth))
            time += binwidth
