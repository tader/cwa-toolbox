#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from numpy import seterr, array, average, histogram
from math import fabs, isnan
import scipy.stats
import random
import logging

class _Single:
    def __init__(self):
        pass

    def fit(self,X,*args,**kwargs):
        if hasattr(X,"__iter__"):
            return X[0]*1.0,0.0,0.0
        else:
            return X*1.0,0.0,0.0

    def cdf(self,X,shape,*args,**kwargs):
        if hasattr(X,"__iter__"):
            return map(lambda x: 1.0 if x >= shape else 0.0, X)
        else:
            return 1.0 if X >= shape else 0.0

    def rvs(self,shape,size=1,**kwargs):
        if size <= 1:
            return shape
        else:
            return [shape]*size

single = _Single()

DISTRIBUTIONS = [
    ("Normal",      "normal",      scipy.stats.norm,      2, {},          lambda x: x,                               lambda x, shape, loc, scale, *args: scipy.stats.norm.cdf(     x,        loc=loc, scale=scale)),
    ("Exponential", "exponential", scipy.stats.expon,     2, {},          lambda x: x if x >= 0.0 else 0.0,          lambda x, shape, loc, scale, *args: scipy.stats.expon.cdf(    x,        loc=loc, scale=scale)),
    ("Weibull",     "weibull",     scipy.stats.frechet_r, 3, {},          lambda x: x if x >  0.0 else 1/1000000000, lambda x, shape, loc, scale, *args: scipy.stats.frechet_r.cdf(x, shape, loc=loc, scale=scale)),
    ("Pareto",      "pareto",      scipy.stats.pareto,    3, {'loc':0.0}, lambda x: x if x >  0.0 else 1/1000000000, lambda x, shape, loc, scale, *args: scipy.stats.pareto.cdf(   x, shape, loc=loc, scale=scale)),
    ("Log-Normal",  "lognormal",   scipy.stats.lognorm,   3, {},          lambda x: x if x >  0.0 else 1/1000000000, lambda x, shape, loc, scale, *args: scipy.stats.lognorm.cdf(  x, shape, loc=loc, scale=scale)),
    ("Gamma",       "gamma",       scipy.stats.gamma,     3, {},          lambda x: x if x >  0.0 else 1/1000000000, lambda x, shape, loc, scale, *args: scipy.stats.gamma.cdf(    x, shape, loc=loc, scale=scale)),
   #("Single Value","single",      single,                3, {},          lambda x: x,                               lambda x, shape, loc, scale, *args: single.cdf(               x, shape, loc=loc, scale=scale)),
]

GOF_TESTS = [
    ("Kolmogorov-Smirnov", "KS",   lambda X, cdf: scipy.stats.kstest(X, cdf)[1])
]

ALPHA = 0.05

SAMPLE_SIZE = 30
D_STATISTIC_SAMPLE_SIZE = 1000
ITERATIONS = 100
NUMBINS = 1000
INF = float('inf')
NINF = float('-inf')

def fit(X, output=None):
    X.sort()

#    result = ('weibull', 0.544148, 0.0, 144.756619), {
#            'weibull': {
#                'shape': 0.544148,
#                'loc': 0.0,
#                'scale': 144.756619,
#                'gof': {
#                    {'test': 'KS', 'p_value': 0.296056},
#                    {'test': 'AD', 'p_value': 0.155636}
#                },
#                'd': 0.103898
#            }
#    }

    result = {}
    fits = []

    orig_settings = seterr()
    seterr(all="ignore")

    result['empirical'] = histogram(X, bins=NUMBINS, normed=True)

    for distribution in DISTRIBUTIONS:
        distribution_parameters = fit_distribution(X, distribution)

        logging.debug("〜 %s: Shape: %f  Loc: %f  Scale: %f" % (
            distribution[0],
            distribution_parameters[0],
            distribution_parameters[1],
            distribution_parameters[2]
        ))

        result[distribution[1]] = {
            'shape': distribution_parameters[0],
            'loc': distribution_parameters[1],
            'scale': distribution_parameters[2],
            'gov': {},
            'd': None,
            'n': len(X)
        }

        cdf = lambda x, *args: distribution[6](
            x,
            distribution_parameters[0],
            distribution_parameters[1],
            distribution_parameters[2],
        )

        D = D_statistic(X, cdf, sample_size=D_STATISTIC_SAMPLE_SIZE)
        logging.debug("      ↕  D-Statistic: %f" % (D))
        result[distribution[1]]['d'] = D

        all_fit = True
        for gof_test in GOF_TESTS:
            if isnan(D) or D == NINF or D == INF:
                p_value = 0.0
            else:
                p_value = test_gof(X, cdf, gof_test)

            if p_value < ALPHA:
                all_fit = False
                bullet = "☐"
            else:
                bullet = "☒"

            logging.debug("      %s  %s: %f" % (
                bullet,
                gof_test[0],
                p_value,
            ))

            result[distribution[1]]['gov'][gof_test[1]] = p_value

            if all_fit:
                fits.append(distribution[1])

        best = None
        if len(fits) > 0:
            best_d = None
            for fit in fits:
                fit_d = result[fit]['d']
                if best_d is None or fit_d < best_d:
                    best = (
                        fit,
                        result[fit]['shape'],
                        result[fit]['loc'],
                        result[fit]['scale']
                    )
                    best_d = fit_d

    seterr(**orig_settings)

    return best, result

def fit_distribution(X, distribution):
    unused_Name, unused_name, rv_continuous, num_params, fit_args, massage, cdf = distribution

    Y = map(massage, X)
    # Calculate MLE
    if num_params == 2:
        loc, scale      = rv_continuous.fit(Y,**fit_args)
        shape           = 0.0
    else:
        shape, loc, scale = rv_continuous.fit(Y,**fit_args)

    return shape, loc, scale

def test_gof(X, cdf, gof_test, sample_size=SAMPLE_SIZE, iterations=ITERATIONS):
    unused_Name, unused_name, gof = gof_test

    if sample_size < len(X):
        return average(array([
            gof(random.sample(X, sample_size), cdf)
            for _ in xrange(iterations)
        ]))
    else:
        return gof(X, cdf)

def D_statistic(X, cdf, sample_size = None):
    if len(X) == 0:
        return -float('inf')
    else:
        if sample_size is not None and sample_size < len(X):
            X = random.sample(X,sample_size)

        Z = list(set(X))
        n = len(X) * 1.0

        return max(array([
            fabs((len(filter(lambda x: x <= z, X)) / n) - cdf(z))
            for z in Z
        ]))

#def adk(*args):
#    K = map(lambda x: sorted(map(float, x)), args)
#    ns = map(len, K)
#    ks = len(K)
#    X = [x for k in K for x in k]
#    Z = sorted(list(set(X)))
#    N = len(X)
#    L = len(Z)
#
#    l = []
#    for z in Z:
#        for k in K:
#            l.append(k.count(z))
#
#    AkN2 = 0.0
#    AakN2 = 0.0
#    for i, k in enumerate(K):
#        M = 0.0
#        Ma = 0.0
#        inner_sum = 0.0
#        inner_suma = 0.0
#
#        for j, z in enumerate(Z):
#            f = k.count(z)
#            M += f
#            Ma = M - f/2.0
#            Bj = sum(l[0:j+1])
#            Baj = Bj - l[j]/2
#
#            if j + 1 < L:
#                inner_sum += l[j] * (N * M - ns[i] * Bj) ** 2 / (Bj * (N - Bj))
#
#            inner_suma += l[j] * (N * Ma - ns[i] * Baj) ** 2 / (Baj * (N - Baj) - N * l[j]/4)
#
#        AkN2 += inner_sum / ns[i]
#        AakN2 += inner_suma / ns[i]
#
#    AkN2 /= N
#    AakN2 = (N-1) * AakN2/N**2
#
#    print AakN2
#    pass

#def ecdf(X, x):
#    x *= 1.0
#    X = map(float, X)
#
#    if max(X) <= x:
#        return 1.0
#
#    elif min(X) > x:
#        return 0.0
#
#    elif x in X:
#        return 1.0*map(lambda y: y <= x, X).index(False)/len(X)
#
#    else:
#        idx_more = map(lambda y: y < x, X).index(False)
#        idx_less = idx_more-1
#
#        val_less = X[idx_less]
#        val_more = X[idx_more]
#
#        p_less = (idx_less + 1.0) / len(X)
#        p_more = (idx_more + 1.0) / len(X)
#
#        return p_less + ((p_more - p_less) * ((x - val_less)/(val_more - val_less)))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    #for dist in DISTRIBUTIONS:
    #    print dist[0]
    #    print fit(dist[2].rvs(4.25, scale=84.29, size=1000))

    import sys

    for filename in sys.argv:
        logging.info("Modeling: %s" % filename)
        values = []

        try:
            fp = open(filename, 'r')
            for row in fp:
                row = row.strip()
                if len(row) > 0:
                    try:
                        value = float(row)
                        values.append(value)
                    except Exception as e:
                        logging.error("Failed to convert '%s' to float!" % row)
                        logging.exception(e)
                if len(values) % 100000 == 0:
                    logging.info("Read %d values!" % len(values))

            fp.close()

            print fit(values)

        except Exception as e:
            logging.exception(e)
