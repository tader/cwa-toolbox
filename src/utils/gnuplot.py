#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Calculate the top-n for some breakdown.

usage: [options] <input file> <output file>

OPTIONS:
  -n, --number=N    calculate the top N (incl. "Other") (default 5)
  -k, --keep=FIELD  leave FIELD untouched
  -1, --one-pass    do everything in a single pass
                    (keep all data in RAM)
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.31"

import logging
import os
import subprocess
import tempfile

OUTPUT_TERMINAL = "postscript eps enhanced color"
OUTPUT_EXTENSION = "eps"
#OUTPUT_TERMINAL = "pdf enhanced color"
#OUTPUT_EXTENSION = "pdf"


DOCUMENT_SIZE = {
    'a0':     ('118.9cm', '84.1cm'),
    'a1':     ( '84.1cm', '59.4cm'),
    'a2':     ( '59.4cm', '42.0cm'),
    'a3':     ( '42.0cm', '29.7cm'),
    'a4':     ( '29.7cm', '21.0cm'),
    'a5':     ( '21.0cm', '14.8cm'),
    'a6':     ( '14.8cm', '10.5cm'),
    'a7':     ( '10.5cm',  '7.4cm'),
    'a8':     (  '7.4cm',  '5.3cm'),
    'a9':     (  '5.3cm',  '3.7cm'),
    'banner': ( '14.8cm',  '4.2cm'),
    'dubbel': ( '14.8cm',  '6.4cm'),
}

LINE_STYLES = [
    "lt 1 lw 3 pt 7 pointsize 1.1  lc rgb \"#000000\"",
    "lt 1 lw 1 pt 13 pointsize 1.6 lc rgb \"#FF0000\"",
    "lt 2 lw 1 pt 0 lc rgb \"#2020C0\"",
    "lt 3 lw 1 pt 0 lc rgb \"orange\"",
    "lt 6 lw 1 pt 0 lc rgb \"#C02020\"",
    "lt 2 lw 3 pt 0 lc rgb \"#20C020\"",
    "lt 3 lw 3 pt 0 lc rgb \"#C020C0\"",
    "lt 6 lw 3 pt 0 lc rgb \"#C02020\"",
    "lt 6 lw 3 pt 0 lc rgb \"#2020C0\"",
    "lt 6 lw 3 pt 0 lc rgb \"#20C020\"",
]

QUANTILES_LINE_STYLES = [
    "lt 1 lw 3 pt 9  pointsize 1 lc rgb \"#000000\"",
    "lt 3 lw 1 pt 6  pointsize 0.1 lc rgb \"orange\"",
    "lt 6 lw 1 pt 6  pointsize 0.2 lc rgb \"#C02020\"",
    "lt 3 lw 1 pt 6  pointsize 0.3 lc rgb \"orange\"",
    "lt 6 lw 1 pt 6  pointsize 0.4 lc rgb \"#C02020\"",
    "lt 3 lw 1 pt 6  pointsize 0.5 lc rgb \"orange\"",
    "lt 2 lw 2 pt 9  pointsize 0.8 lc rgb \"#2020C0\"",
    "lt 1 lw 3 pt 5  pointsize 1 lc rgb \"#FF0000\"",
    "lt 2 lw 2 pt 11  pointsize 0.8 lc rgb \"#2020C0\"",
    "lt 3 lw 1 pt 6  pointsize 0.5 lc rgb \"orange\"",
    "lt 6 lw 1 pt 6  pointsize 0.4 lc rgb \"#C02020\"",
    "lt 3 lw 1 pt 6  pointsize 0.3 lc rgb \"orange\"",
    "lt 6 lw 1 pt 6  pointsize 0.2 lc rgb \"#C02020\"",
    "lt 3 lw 1 pt 6  pointsize 0.1 lc rgb \"orange\"",
    "lt 1 lw 3 pt 11 pointsize 1   lc rgb \"#000000\"",
]

POINT_SIZE = "set pointsize 0.05"
LMARGIN = 0.15
RMARGIN = 0.8


def escape(string):
    string = string.replace("\\", "\\\\",)
    string = string.replace("_",  "\\\\_",)
    string = string.replace("^",  "\\\\^",)
    return string


def plot(gnuplot_code, file_name, size='a4', landscape=False):
    plt = tempfile.NamedTemporaryFile()

    if landscape:
        document_size = "%s,%s" % (
            DOCUMENT_SIZE[size][0],
            DOCUMENT_SIZE[size][1]
        )
    else:
        document_size = "%s,%s" % (
            DOCUMENT_SIZE[size][1],
            DOCUMENT_SIZE[size][0]
        )

    print >> plt, "set terminal %s size %s" % (OUTPUT_TERMINAL, document_size)
    print >> plt, "set output \"%s.%s\"" % (file_name, OUTPUT_EXTENSION)
    # Most files start with a Time column...
    print >> plt, "set datafile missing 'Time'"
    print >> plt, "set lmargin at screen %f" % LMARGIN
    print >> plt, "set rmargin at screen %f" % RMARGIN
    print >> plt, gnuplot_code
    plt.flush()

    plt.seek(0)
    logging.debug("GNUPLOT CODE:\n" + "".join(plt.readlines()))
    #subprocess.Popen(["cat", plt.name]).wait()
    subprocess.Popen(["gnuplot", plt.name]).wait()
    plt.close()


def gen_multiplot(graphs, title=None):
    count = 0

    code = []

    for plot in graphs:
        count += 1
        code.append(plot)

    title = " title \"%s\"" % escape(title) if title is not None else ""

    return "\n".join(
        ["set multiplot layout %d,1%s" % (count, title),
         "set lmargin at screen %f" % LMARGIN,
         "set rmargin at screen %f" % RMARGIN,
        ] + code
    )


def gen_over_time(
    data_file,

    legend=["data"],

    xlabel="Date",
    xistime=True,
    xvalue_notation="%d %b\\n%y",

    line_type="lines",
    skip=0,
    every=1,
    reverse_legend=False,
    reverse=False,

    cumulative=False,

    **kwargs
):
    assert(len(legend) > 0)
    assert(os.path.isfile(data_file))

    kwargs.update(locals())
    code = _gen_settings(**kwargs)

    def get_column_name(col):
        if legend is not None and col < len(legend):
            return legend[col]
        else:
            return "Column %d" % (col + 1)

    num_cols = len(legend)

    code.append("plot \"%s\" \\" % data_file)

    columns = range(skip, num_cols)
    if reverse_legend:
        columns.reverse()

    for col in columns:
        if cumulative:
            # DIT HEB IK VERANDERD!!!!!!!!!!!!!!
            using = ["$%d" % i for i in xrange(
                    2 + skip,
                    num_cols - col + 2
                )] if not reverse else ["$%d" % i for i in xrange(
                    2 + skip + col,
                    num_cols + 2
                )]
        else:
            using = ["$%d" % (col + 2)]

        row = []
        row.append("\t")
        if col == columns[0]:
            row.append("   ")
        else:
            row.append("\"\" ")
        if every > 1:
            row.append("every %d " % every)
        row.append("using 1:(%s)" % "+".join(using))
        row.append(" title \"%s\"" % escape(get_column_name(col)))
        row.append(" with %s ls %d" % (line_type, col + 1 - skip))
        if col != columns[len(columns) - 1]:
            row.append(", \\")
        code.append("".join(row))

    return "\n".join(code)


def gen_pdf_cdf(
    data_file,
    ylabel="Probability",
    xlabel="Value",
    **kwargs
):
    if not os.path.isfile(data_file):
        return ""

    kwargs.update(locals())
    code = _gen_settings(**kwargs)

    code.append("set yrange [0:1.1]")
    code.append("plot \"%s\" \\" % data_file)

    row = []
    row.append("\t")
    row.append("   ")
    row.append("using 1:($3)")
    row.append(" title \"CDF\"")
    row.append(" with lines ls 1")
    row.append(", \\")
    code.append("".join(row))

    row = []
    row.append("\t")
    row.append("\"\" ")
    row.append("using 1:($2)")
    row.append(" title \"PDF\"")
    row.append(" with impulses ls 2")
    code.append("".join(row))

    return "\n".join(code)


def _gen_settings(
    title=None,
    label=None,

    ylabel=None,
    xlabel=None,
    yvalue_notation=None,
    xvalue_notation=None,
    xlog=False,
    ylog=False,
    xistime=False,
    yistime=False,

    line_styles=None,
    key="outside",

    timefmt="%s",

    **kwargs
):

    code = []

    code.append("unset label")
#    code.append("set autoscale")
#    code.append("set xtic auto")
#    code.append("set ytic auto")

    if line_styles is None:
        line_styles = LINE_STYLES

    for i, line_style in enumerate(line_styles):
        code.append("set style line %d %s" % (i + 1, line_style))
    code.append(POINT_SIZE)

    code.append("set xtics out")
    code.append("set ytics out")
    code.append("set key %s" % key)

    if not title:
        code.append("unset title")
    if not xistime:
        code.append("unset xdata")
    if not yistime:
        code.append("unset ydata")
    code.append("unset logscale")
    if not xlabel:
        code.append("unset xlabel")
    if not ylabel:
        code.append("unset ylabel")
    code.append("unset format")

    # Graph Common
    if title:
        code.append("set title \"%s\"" % escape(title))

    if label:
        code.append("set label \"%s\" at graph 0.005, graph .97" %
            escape(label)
        )

    # Time on Axes
    if xistime or yistime:
        if xistime:
            code.append("set xdata time")
        if yistime:
            code.append("set ydata time")
        code.append("set timefmt \"%s\"" % timefmt)

    # X-Axis Markup
    if xvalue_notation:
        code.append("set format x \"%s\"" % xvalue_notation)
    if xlog:
        code.append("set logscale x")
    if xlabel or xlog:
        code.append("set xlabel \"%s%s\"" % (
            escape(xlabel),
            " (log. scale)" if xlog else ""
        ))

    # Y-Axis Markup
    if yvalue_notation:
        code.append("set format y \"%s\"" % yvalue_notation)
    if ylog:
        code.append("set logscale y")
    if ylabel or ylog:
        code.append("set ylabel \"%s%s\"" % (
            escape(ylabel),
            " (log. scale)" if ylog else ""
        ))

    return code
