#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Anderson-Darling k-sample procedure to test whether
# k sampled populations are identical
#
# Anderson and Darling (1952,1954) introduced a goodness-of-fit statistic
#
#                         _inf            2
#                        /  {Fm(x) - Fo(x)}
#              ADm = m  /  ----------------- dFo(x)
#                 -inf_/    Fo(x){1 - Fo(x)}
#
# to test the hypothesis that a random sample Fm(x) comes from a continuous
# population with a specified distribution function Fo(x). It is a 
# modification of the Kolmogorov-Smirnov (K-S) test and gives more weight
# to the tails than the K-S test.
#
# The corresponding two-sample version,
#
#                              _inf            2
#                  mn         /  {Fm(x) - Gn(x)}
#          ADmn = ----       /  ----------------- dHn(x)
#                   N  -inf_/    Hn(x){1 - Hn(x)}
#
# was proposed by Darling (1957) and studied in detail by Pettitt (1976). 
# Where Gn(x) is the empirical distribution function of the second 
# independent sample G(x) and Hn(x) = {mFm(x) + nGn(x)}/N, with N = m+n, is
# the empirical distribution function of the pooled sample. It is used to
# test the hypothesis that F = G.
#
# The Anderson-Darling k-sample test was introduced by Scholz and Stephens
# (1987) as a generalization of the two-sample Anderson-Darling test. It is
# a nonparametric statistical procedure, i.e., a rank test, and, thus, 
# requires no assumptions other than that the samples are true independent 
# random samples from their respective continuous populations (although 
# provisions for tied observations are made). It tests the hypothesis that
# the populations from which two or more independent samples of data were
# drawn are identical. This test can be used to decide whether data from
# different sources may be combined, because they are judged to come from
# one common distribution, i.e., the null hypothesis Ho of same population
# distributions cannot be rejected. In its opposite use, it can be seen as
# a generalization of a one-way ANOVA  for which the k-sample Kruskal-Wallis
# test (1952, 1953) is the most commonly used rank test.
#
# It is an omnibus test because of its effectiveness against all alternatives
# to the null hypothesis Ho's (all k populations being equal). For example,
# it is effective for changes in scale while locations are matched, which is
# a weakness of the Kruskal-Wallis test.
#
# The Anderson-Darling k-sample procedure assumes that i-th sample has a 
# continuous distribution function and we are interested in testing the null
# hypothesis that all sampled populations have the same distribution, without
# specifying the nature of that common distribution,
#
#                        Ho: F1 = F2 = ... = Fk
#
# without specifying the nature of that common distribution F. The statistic
# is,
#
#                _k _          _               2
#                \           /  {Fi(x) - Hn(x)}
#        ADK  =  /_ _   ni  /  ----------------- dHn(x)
#                i=1   Bn _/    Hn(x){1 - Hn(x)}
#
# where Bn = {x E R:Hn(x) < 1}.
#
# The computational formula for ADK* adjusted for ties is,
#
#                     _k _          _L _                2
#               n-1   \        1    \      {nFij - niHj}
#        ADK  = ----  /_ _  [ ----  /_ _  ---------------- ]
#               n^2   i=1      ni   j=1   Hj(n-Hj) - nhj/4
#
#
# and the corresponding not adjusted for ties is
#
#                     _k _          L-1_               2
#                1    \        1    \       {nFij - niHj}
#        ADKn = ----  /_ _  [ ----  /_ _  ---------------- ]
#                n    i=1      ni   j=1        Hj(n-Hj)
#
# where:
#  k = number of groups (samples); i=1,2,...,k
#  ni = data values in the ith group; j=1,2,...ni
#  n = number total of observations (data); n=n1+n2+...+nk
#  xij = data in the i group and j observation within that group
#  z(j) = distinct values of all combined data ordered in ascendent way
#         denoted z(1),z(2),...,z(L)
#  L = lergest unique value, where it will be less than n with tied values
#  hj = number of values in the pooled sample equal to z(j)
#  Hj = number of values in the combined samples less than zj plus one half
#       the number of values in the combined samples equal to zj
#  Fij = number of values in the ith group which are less than zj plus one
#        half the number of values in this group which are equal to zj
#
# Under Ho and assuming continuity of the common F distribution the mean of
# ADK is k-1 and the variance* is,
#
#                          an^3 + bn^2 + cn + d 
#               var(ADK) = --------------------- 
#                             (n-1)(n-2)(n-3)
#
# where,
#               a = (4g - 6)(k-1) + (10 - 6g)S
#               b = (2g -4)k^2 + 8Tk + (2g - 14T -4)S - 8T + 4g -6
#               c = (6T + 2g -2)k^2 + (4T - 4g + 6)k + (2T -6)S + 4T
#               d = (2T +6)k^2 - 4Tk
#
# with,
#                   _k _               _n-1_
#                   \      1           \      1
#               S = /_ _  ---- ,   T = /_ _  ----  
#                          ni           i=1   i
#
# and
#                         _n-2_   _n-1_
#                         \       \        1
#                     g = /_ _    /_ _   ------  
#                         i=1     j=i+1  (n-i)j
#
# The observed k-sample Anderson-Darling statistic (ADK) is standardized
# using its exact sample mean and standard deviation to removes some of its
# dependence on the sample size. Then,
#
#                             ADK - (k-1)
#                      ADKs = ------------
#                               std(ADK)
#
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# *NOTE.-In some literature as in DOD-MIL-HDBK-17-1E_Vol 1, Ch. 8, Polymer
# Matrix Composites (1997) you can find this statistic with an equivalent
# mathematical expressions as:
#
#                         _k _          _L _                2
#                 n-1     \        1    \      {nFij - niHj}
#        ADK  = --------  /_ _  [ ----  /_ _  ---------------- ] ,
#               n^2(k-1)   i=1      ni   j=1   Hj(n-Hj) - nhj/4
#
#
#                              an^3 + bn^2 + cn + d 
#                 var(ADK) = ------------------------ ,
#                             (n-1)(n-2)(n-3)(k-1)^2
#
# and
#
#                                  ADK - 1
#                          ADKs = --------- .
#                                  std(ADK)
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# The approximate P-value of the observed ADK statistic can be calculated
# using a spline interpolation method. For the interested users, we are
# including, as a comment, the mathematical procedure to get the ADK 
# critical value.
#
# The interpolation coefficients for each alpa-value,
#
#               alpha = [0.25,0.10 0.05 0.025 0.01]
# are:
#                  bo = [0.675,1.281,1.645,1.96,2.326]
#                  b1 = [-0.245,0.25,0.678,1.149,1.822]
#                  b2 = [-0.105,-0.305,-0.362,-0.391,-0.396]
#
# and the general formula interpolation is,
#
#                  qnt = bo + b1/sqrt(k-1) + b2/(k-1)
#
# We give the Anderson-Darling k-sample procedure with and without 
# adjustment for ties.
#
# Finally, we compare the P-value with the desired significance level alpha
# to facilitate a decision about the null hypothesis Ho. 
#
# Syntax: function AnDarksamtest(X,alpha) 
#
# Inputs:
#      X - data matrix (Size of matrix must be n-by-2; data=column 1,
#          sample=column 2) 
#  alpha - significance level (default = 0.05)
#
# Output:
#        - Complete Anderson-Darling k-sample test
#
# Example: From the example given by Scholz and Stephens (1987, p.922). It
# is interested to test if the four sets of eight measurements each of the
# smoothness of a certain type of paper obtained by four independent 
# laboratories are homogeneous or identical to consider them
# unstructurated. We use an alpha-value = 0.05. Data are,
#
#  -----------------------------------------------------------------
#    Laboratory                      Smoothness
#  -----------------------------------------------------------------
#        1          38.7  41.5  43.8  44.5  45.5  46.0  47.7  58.0
#        2          39.2  39.3  39.7  41.4  41.8  42.9  43.3  45.8
#        3          34.0  35.0  39.0  40.0  43.0  43.0  44.0  45.0
#        4          34.0  34.8  34.8  35.4  37.2  37.8  41.2  42.8
#  -----------------------------------------------------------------
#
# Data vector is:
#  X=[38.7 1;41.5 1;43.8 1;44.5 1;45.5 1;46.0 1;47.7 1;58.0 1;
#  39.2 2;39.3 2;39.7 2;41.4 2;41.8 2;42.9 2;43.3 2;45.8 2;
#  34.0 3;35.0 3;39.0 3;40.0 3;43.0 3;43.0 3;44.0 3;45.0 3;
#  34.0 4;34.8 4;34.8 4;35.4 4;37.2 4;37.8 4;41.2 4;42.8 4];
#
# Calling on Matlab the function: 
#            AnDarksamtest(X)
#
# Answer is:
#
# K-sample Anderson-Darling Test
# ----------------------------------------------------------------------------
# Number of samples: 4
# Sample sizes: 8 8 8 8
# Total number of observations: 32
# Number of ties (identical values): 3
# Mean of the Anderson-Darling rank statistic: 3
# Standard deviation of the Anderson-Darling rank statistic: 1.2037664
# ----------------------------------------------------------------------------
# Not adjusted for ties.
# ----------------------------------------------------------------------------
# Anderson-Darling rank statistic: 8.3558723
# Standardized Anderson-Darling rank statistic: 4.4492622
# Probability associated to the Anderson-Darling rank statistic = 0.0022547
#
# With a given significance = 0.050
# The samples were drawn from different populations: data may be considered
# structurated with respect to the random of fixed effect in question.
# ----------------------------------------------------------------------------
# Adjusted for ties.
# ----------------------------------------------------------------------------
# Anderson-Darling rank statistic: 8.3926093
# Standardized Anderson-Darling rank statistic: 4.4797806
# Probability associated to the Anderson-Darling rank statistic = 0.0021700
#
# With a given significance = 0.050
# The samples were drawn from different populations: data may be considered
# structurated with respect to the random of fixed effect in question.
# ----------------------------------------------------------------------------
#
# Created by A. Trujillo-Ortiz, R. Hernandez-Walls, K. Barba-Rojo, 
#             L. Cupul-Magana and R.C. Zavala-Garcia
#             Facultad de Ciencias Marinas
#             Universidad Autonoma de Baja California
#             Apdo. Postal 453
#             Ensenada, Baja California
#             Mexico.
#             atrujo@uabc.mx
#
# Copyright. November 1, 2007.
#
# ---Special thanks are given to Michael Singer, Department of Earth & 
#    Planetary Science, Institute for Computational Earth System Science,
#    University of California at Berkely, for encouraging us to create
#    this m-file--
# ---We are grateful to Fritz Scholz and Michael Stephens, the authors of
#    of this test for their valuable suggestions to improve the clarity of
#    the presentation.
#
#  To cite this file, this would be an appropriate format:
#  Trujillo-Ortiz, A., R. Hernandez-Walls, K. Barba-Rojo, L. Cupul-Magana
#    and R.C. Zavala-Garcia. (2007). AnDarksamtest:Anderson-Darling k-sample
#    procedure to test the hypothesis that the populations of the drawned
#    groups are identical. A MATLAB file. [WWW document]. URL http://
#    www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=17451
#
# References:
# Anderson, T.W. and Darling, D.A. (1952), Asymptotic Theory of Certain
#     Goodness of Fit Criteria Based on Stochastic Processes. Annals of
#     Mathematical Statistics, 23:193-212.
# Anderson, T.W. and Darling, D.A. (1954), A Test of Goodness of Fit. 
#     Journal of the American Statistical Association, 49:765-769.
# Darling, D.A. (1957), The Kolmogorov-Smirnov, Cramer-von Mises
#     Tests. Annals of Mathematical Statistics, 28:823-838.
# Kruskal, W.H. and Wallis, W.A. (1952), Use of ranks in one-criterion
#     variance analysis. Journal of the American Statistical Association,
#     47(260):583�621.
# Kruskal, W.H. and Wallis, W.A. (1953), Errata to Use of ranks in one-
#     criterion variance analysis. Journal of the American Statistical 
#     Association, 48:907-911.
# MIL-HDBK-17-1E (1997), The Composite Materials Handbook, Volume 1.
#     Polymer Matrix Composites: Guidelines for Characterization of
#     Structural Materials. Chapter 8, Department of Defense, 
#     Washington, D.C.
# Pettitt, A.N. (1976), A Two-Sample Anderson-Darling Rank Statistic.
#     Biometrika, 63:161-168.
# Scholz, F.W. and Stephens, M.A. (1987), K-Sample Anderson-Darling Tests.
#     Journal of the American Statistical Association, 82:918-924.
#

import numpy as np
from scipy import optimize
import math

def adk(*samples):
    k = len(samples)

    if k < 2:
        raise Exception("I need at least two samples")

    if min(map(len, samples)) == 0:
        raise Exception("At least one of the samples has no observations")

    # construct an array of the samples
    # and sort the observations in the samples
    samples = np.asarray(samples)
    samples.sort(axis=1)
    n = samples.size
    ns = np.array([sample.size for sample in samples])

    # select the unique observations in all the samples
    Z = np.unique(samples)
    L = Z.size

    # count for each unique observed value
    # the total times it occurs in the samples
    l = np.array([sum(sum(samples == z)) for z in Z])

    AkN2  = 0.0
    AakN2 = 0.0

    for sample in samples:
        s         = sample.size
        M         = 0.0
        Ma        = 0.0
        innersum  = 0.0
        innersuma = 0.0

        for j, z in enumerate(Z):
            fij = sum(sample == z)
            M  += fij
            Ma  = M - fij/2.0

            Bj  = sum(l[:j+1])
            Baj = Bj - l[j] / 2.0

            if j < L-1:
                innersum += (
                    l[j]
                    * (n * M - s * Bj) ** 2
                    / (Bj * (n - Bj))
                )

            innersuma += (
                l[j]
                * (n * Ma - s * Baj) ** 2
                / (Baj * (n - Baj) - n * l[j]/4.0)
            )

        AkN2  += innersum  / s
        AakN2 += innersuma / s

    # k-sample Anderson-Darling observed statistic
    AkN2  /= n                      # not adjusted for ties
    AakN2  = (n - 1) * AakN2 / n**2 # adjusted for ties

    H = sum(1.0/ns)
    h = sum(1.0/np.arange(1,n))
    g = sum(
        (1.0/(n - i)) * sum(1.0/np.arange(i+1, n))
        for i in xrange(1, n-1)
    )

    a = ((4 * g - 6) * (k - 1) 
         + (10 - 6 * g) * H)
    b = ((2 * g - 4) * k**2 
         + 8 * h * k 
         + (2 * g - 14 * h - 4) * H - 8 * h 
         + 4 * g 
         - 6)
    c = ((6 * h + 2 * g - 2) * k**2 
         + (4 * h - 4 * g + 6) * k 
         + (2 * h - 6) * H 
         + 4 * h)
    d = ((2 * h + 6) * k**2 - 4 * h * k)

    sig = math.sqrt(
        (a * n**3 + b * n**2 + c * n + d)/((n - 1) * (n - 2) * (n - 3))
    )

    print sig

    TkN      = (AkN2  - (k - 1))/sig # standardized k-sample Anderson-Darling
                                     # statistic not adjusted for ties

    TakN     = (AakN2 - (k - 1))/sig # standardized k-sample Anderson-Darling
                                     # statistic adjusted for ties

    pvalTkN  = pval(TkN,  k - 1)
    pvalTakN = pval(TakN, k - 1)

    print 'samples:\n', samples
    print 'Z:\n', Z
    print 'n', n, 'L', L
    print 'l:\n', l
    print 'AkN2:', AkN2
    print 'AakN2:', AakN2

    return k, ns, n, n - L, sig

def pval(tx, m):
    table = np.array([
        [1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 10.0, float('inf')],
        [0.326, 0.449, 0.498, 0.525, 0.557, 0.576, 0.59, 0.674],
        [1.225, 1.309, 1.324, 1.329, 1.332, 1.33, 1.329, 1.282],
        [1.96, 1.945, 1.915, 1.894, 1.859, 1.839, 1.823, 1.645],
        [2.719, 2.576, 2.493, 2.438, 2.365, 2.318, 2.284, 1.96],
        [3.752, 3.414, 3.246, 3.139, 3.005, 2.92, 2.862, 2.326]
        ])

    mt = table[0]
    sqm1 = 1.0/(mt ** 0.5)
    sqm2 = sqm1**2

    p = np.array([0.25, 0.1, 0.05, 0.025, 0.01])
    lp = np.array(map(lambda x: math.log(x/(1.0-x)), p))

    #for i in xrange(1,5):
    #    coef = lsfit(np.asarray([sqm1, sqm2]), table])
    #    print coef

    return 0.05

def lsfit(X,y):
    coef, _, _, _  = np.linalg.lstsq(X,y)
    return coef

if __name__ == '__main__':
    import random
    print adk(
        random.sample(xrange(20), 10),
        random.sample(xrange(20), 10),
        random.sample(xrange(20), 10),
    )
