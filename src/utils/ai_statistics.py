#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""
A module with commonly used statistical tools.
"""

__author__ = 'Alexandru Iosup <A.Iosup@tudelft.nl>';
__version__ = '11.3.23';

import math

DEFAULT_PERCENTILES = [0.001, 0.01, 0.1, 1, 2, 5, 25, 50, 75, 95, 98, 99, 99.9, 99.99, 99.999]

def merge(array1, array2):
    merged_array = []
    pointer1, pointer2 = 0, 0
    while pointer1 < len(array1) and pointer2 < len(array2):
        if array1[pointer1] < array2[pointer2]:
            merged_array.append(array1[pointer1])
            pointer1 += 1
        else:
            merged_array.append(array2[pointer2])
            pointer2 += 1
    while pointer1 < len(array1):
        merged_array.append(array1[pointer1])
        pointer1 += 1

    while pointer2 < len(array2):
        merged_array.append(array2[pointer2])
        pointer2 += 1

    return merged_array

class CStats:
    """a class for quickly summarizing comparable (e.g., numeric) data"""
    def __init__(self, bIsNumeric=True, bKeepValues=True, bAutoComputeStats=False):
        self.Min = None
        self.Max = None
        self.Sum = 0
        self.SumOfSquares = 0
        self.Avg = None
        self.StdDev = None
        self.COV = None
        self.NItems = 0
        self.Values = []
        self.bIsNumeric = bIsNumeric
        self.bKeepValues = bKeepValues
        self.bAutoComputeStats = bAutoComputeStats
        self._isSorted = True

    def __add__(self, other):
        result = CStats(
            self.bIsNumeric or other.bIsNumeric,
            self.bKeepValues and other.bKeepValues,
            self.bAutoComputeStats or other.bAutoComputeStats
        )

        result.NItems = self.NItems + other.NItems
        if result.bKeepValues:
            if self._isSorted and other._isSorted:
                result.Values = merge(self.Values, other.Values)
                result._isSorted = True
            else:
                result.Values = self.Values[:]
                result.Values.extend(other.Values)
                result._isSorted = False
                
        result.Min = min(self.Min, other.Min)
        result.Max = max(self.Max, other.Max)
        if result.bIsNumeric:
            result.Sum = self.Sum + other.Sum
            result.SumOfSquares += self.SumOfSquares + other.SumOfSquares
            if result.bAutoComputeStats:
                result._update_stats()
        return result

    def addValue(self, Value):
        self.NItems += 1
        
        self.Values.append(Value)
        if Value < self.Max: self._isSorted = False
        
        self.Min = min(self.Min, Value)
        self.Max = max(self.Max, Value)
        if self.bIsNumeric:
            self.Sum += Value
            self.SumOfSquares += Value * Value
            if self.bAutoComputeStats:
                self._update_stats()

    def _update_stats(self):
        if self.NItems != 0:
            self.Avg = float(self.Sum) / self.NItems

        if self.NItems - 1 > 0:
            Diff = abs(self.NItems * self.SumOfSquares - self.Sum * self.Sum)
            self.StdDev = math.sqrt(Diff / (self.NItems * (self.NItems - 1)))
        else:
            self.StdDev = 0.0

        if self.NItems != 0 and abs(self.Avg) > 0.0001:
            self.COV = self.StdDev / self.Avg
        else:
            self.COV = 0.0

    def doComputeStats(self):
        if self.NItems > 0:
            self.Avg = float(self.Sum) / self.NItems
            if self.NItems - 1 > 0:
                Diff = (self.NItems * self.SumOfSquares - self.Sum * self.Sum)
                if Diff < 0.0: Diff = 0.0 # adding small figures may lead to a diff ~0.0..1)
                self.StdDev = math.sqrt(Diff / (self.NItems * (self.NItems - 1)))
            else:
                self.StdDev = 0.0
            if abs(self.Avg) > 0.0001:
                self.COV = self.StdDev / self.Avg
            else:
                self.COV = 0.0

    def calculate_quartiles(self):
        """
        Compute Quartiles

        This requires CStats.bKeepValues to be set.

        RETURNS:
            [
                  0% = min,
                 25%,
                 50% = median,
                 75%,
                100% = max
            ]
        """
        return self.calculate_percentiles([0, 25, 50, 75, 100])

    def _ensureSorted(self):
        if not self._isSorted:
            self.Values.sort()
            
    def calculate_percentiles(self, percentiles=None):
        if not self.bKeepValues:
            raise Exception("values have not been kept")

        self._ensureSorted()
        values = self.Values
        n = len(values)

        if percentiles is None:
            percentiles = DEFAULT_PERCENTILES

        def calculate_percentile(percentile):
            if n <= 0:
                return None

            if percentile == 50:
                return values[(n - 1) / 2] if n % 2 == 1 else (values[(n - 1) / 2] + values[n / 2]) / 2

            index = int(percentile / 100.0 * n)
            if index > n - 1: index = n - 1
            if index < 0: index = 0
            return values[index]

        return [
            calculate_percentile(p) for p in percentiles
        ]
    
    def calculate_pdf_cdf(self, width=1):
        if not self.bKeepValues:
            raise Exception("values have not been kept")

        self._ensureSorted()
        values = self.Values
        n = len(values)
        
        if n > 0:
            current = values[0]
            current_count = 0.0
            total_count = 0.0
            cumulative = 0.0
            
            for value in values:
                total_count += 1.0
                if value <= current + width:
                    current_count += 1.0
                else:
                    impulse = current_count / n
                    cumulative = total_count / n
                    yield current, impulse, cumulative
                    current = value
                    current_count = 1.0
            
            impulse = current_count / n
            cumulative = total_count / n
            yield current, impulse, cumulative


class CHistogram:
    """ a class for creating histograms of comparable (e.g., numeric) data """
    def __init__(self, bIsNumeric=True, bKeepValues=True):
        self.Stats = CStats(bIsNumeric, bKeepValues, bAutoComputeStats=False)
        self.NItems = 0
        self.Values = {}
        self.MaxHeight = 0
        self.CDF = None

    def addValue(self, Value):
        self.NItems += 1
        #-- map value to histogram
        if Value not in self.Values: self.Values[Value] = 0
        self.Values[Value] += 1
        if self.MaxHeight < self.Values[Value]: self.MaxHeight = self.Values[Value]
        #-- create value statistics
        self.Stats.addValue(Value)

    def getMinValue(self):
        return self.Stats.Min

    def getMaxValue(self):
        return self.Stats.Max

    def computeCDF(self, StepSize=1, bMustRecompute=True):
        if bMustRecompute or self.CDF is None:
            self.Stats.doComputeStats()
            self.CDF = {}
            if self.NItems > 0:
                Counter = 0
                for Value in xrange(self.Stats.Min, self.Stats.Max + 1, StepSize):
                    if Value in self.Values:
                        Counter += self.Values[Value]
                    self.CDF[Value] = float(Counter) / self.NItems
        return self.CDF
