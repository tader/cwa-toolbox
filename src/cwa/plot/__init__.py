#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
"""Plot something
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.11.18"

from cwa import analyze
from utils import gnuplot
from utils.tr_reader import TRReader
import getopt
import logging
import main
import os
import re
import sys

def run(argv):
    try:
        opts, argv = getopt.getopt(
            argv,
            "f",
            ["force"]
        )
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(2)

    force = False

    for o, unused_a in opts:
        if o in ("-f", "--force"):
            force = True

    plotter = Plotter()
    plotter.overwrite = force
    plotter.run()

def human(seconds):
    units = [
             (86400, "one day",    "days"),
             (3600,  "one hour",   "hours"),
             (600,   "one minute", "minutes"),
             (1,     "one second", "seconds")
    ]

    for interval, singular, plural in units:
        if seconds % interval == 0:
            if seconds == interval:
                return singular
            return "%d %s" % (seconds / 86400, plural)

    return "%d seconds" % seconds

def ordinal(rang):
    if rang == 1:  return "first"
    if rang == 2:  return "second"
    if rang == 3:  return "third"
    if rang == 4:  return "fourth"
    if rang == 5:  return "fifth"
    if rang == 6:  return "sixth"
    if rang == 7:  return "seventh"
    if rang == 8:  return "eighth"
    if rang == 9:  return "nineth"
    if rang == 10: return "tenth"

    return "%dth" % rang


class FieldType:
    def __init__(self,unit=None,abbreviation=None,format=None):
        self.unit = unit
        self.abbreviation = abbreviation
        self.format = format

    def legend(self, name):
        if self.unit is None:
            if self.abbreviation is None:
                return name
            else:
                return "%s (%s)" % (name, self.abbreviation)
        else:
            return "%s (%s)" % (name, self.unit)

TYPE_ANY   = FieldType()
TYPE_TIME  = FieldType("seconds", "s.") #, "%.1t{/Symbol \327}10^{%L}")
TYPE_BYTES = FieldType("bytes",   "B",  "%.0s %cB")
TYPE_RATIO = TYPE_ANY
TYPE_COUNT = TYPE_ANY

metric_properties = {
    "WaitTime":              (TYPE_TIME,  "Wait Time"           ),
    "RunTime":               (TYPE_TIME,  "Run Time"            ),
    "TotalWallClockTime":    (TYPE_TIME,  "Total Wallclock Time"),
    "Memory":                (TYPE_BYTES, "Memory"              ),
    "Network":               (TYPE_BYTES, "Network"             ),
    "Disk":                  (TYPE_BYTES, "Disk"                ),
    "DiskIORatio":           (TYPE_RATIO, "Disk I/O Ratio"      ),
    "MR_map_input":          (TYPE_BYTES, "Map Input"           ),
    "MR_map_output":         (TYPE_BYTES, "Map Output"          ),
    "MR_total_hdfs_read":    (TYPE_BYTES, "Read from HDFS"      ),
    "MR_total_hdfs_written": (TYPE_BYTES, "Written to HDFS"     ),
    "MR_total_map_time":     (TYPE_TIME,  "Map Time"            ),
    "MR_total_reduce_time":  (TYPE_TIME,  "Reduce Time"         ),
    "MR_total_from_job":     (TYPE_COUNT, "Tasks"               ),
    "MR_total_launched":     (TYPE_COUNT, "Tasks"               ),
    "MR_total_failed":       (TYPE_COUNT, "Failed Tasks"        ),
    "MR_total_killed":       (TYPE_COUNT, "Killed Tasks"        ),
}

def get_type(name):
    try:
        return metric_properties[name][0]
    except:
        return TYPE_ANY

def get_title(name):
    try:
        return metric_properties[name][1]
    except:
        return name


class Plotter:
    def __init__(self):
        self.config = main.get_config()
        self.overwrite = False

    def run(self):
        interval = int(float(self.config.get("plot", "interval")))
        every    = int(float(self.config.get("plot", "every")))
        job_metrics = [x.strip() for x in self.config.get("plot", "job_metrics").split()]
        task_metrics = [x.strip() for x in self.config.get("plot", "task_metrics").split()]
        job_breakdowns = [x.strip() for x in self.config.get("plot", "job_breakdowns").split()]
        task_breakdowns = [x.strip() for x in self.config.get("plot", "task_breakdowns").split()]

        if interval <= 0: interval = 86400
        if every    <= 0: every    = 1

        for breakdown in set([None] + job_breakdowns + task_breakdowns):
            bd_string = "" if breakdown is None else "_per_%s" % breakdown
            cumulative_jobs_plt = ""
            cumulative_tasks_plt = ""

            # Number of Jobs/Tasks over time
            if breakdown is None or breakdown in job_breakdowns:
                data = os.path.join(self.config.get("plot", "input"), "cumulative_jobs%s_%d.dat" % (bd_string, interval))
                if os.path.exists(data):
                    kwargs = {'legend':["Job Count"]} if breakdown is None else {}
                    cumulative_jobs_plt = self.gen_cumulative(
                        data,
                        ylabel="Running Jobs Count",
                        label="cumulative per %s" % human(interval),
                        reverse=True,
                        **kwargs
                    )

                    self.plot(
                        self.path(os.path.join(self.config.get("plot", "output"), "running_jobs%s" % bd_string)),
                        cumulative_jobs_plt,
                        size="banner", landscape = True
                    )

            if breakdown is None or breakdown in task_breakdowns:
                data = os.path.join(self.config.get("plot", "input"), "cumulative_tasks%s_%d.dat" % (bd_string, interval))
                if os.path.exists(data):
                    kwargs = {'legend':["Task Count"]} if breakdown is None else {}
                    cumulative_tasks_plt = self.gen_cumulative(
                        data,
                        ylabel="Running Tasks Count",
                        label="cumulative per %s" % human(interval),
                        **kwargs
                    )

                    self.plot(
                        self.path(os.path.join(self.config.get("plot", "output"), "running_tasks%s" % bd_string)),
                        cumulative_tasks_plt,
                        size="banner", landscape = True
                    )

            if breakdown is None or (breakdown in job_breakdowns and breakdown in task_breakdowns):
                self.plot(
                    self.path(os.path.join(self.config.get("plot", "output"), "running_jobs_and_tasks%s" % bd_string)),
                    gnuplot.gen_multiplot(
                        [
                            cumulative_jobs_plt,
                            cumulative_tasks_plt,
                        ],
                        title="%s, Count of Running Jobs and Tasks" % self.config.get("trace", "title")
                    )
                )

        for job in [True, False]:
            job_or_task = "job" if job else "task"
            metrics = job_metrics if job else task_metrics
            for metric in metrics:

                tme_plt = self.gen_cumulative(
                    os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_%d.dat" % (job_or_task, metric, interval)),
                    ylabel=get_type(metric).legend(get_title(metric)),
                    yvalue_notation=get_type(metric).format,
                    legend=["%s Sum" % (metric)],
                    every=every,
                    label="cumulative per %s, every %s interval shown" % (human(interval), ordinal(every))
                )
                qtl_plt = self.gen_quantiles(
                    os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_%d.dat" % (job_or_task, metric, interval)),
                    ylabel=get_type(metric).legend(get_title(metric)),
                    yvalue_notation=get_type(metric).format,
                    every=every,
                    label="intervals of %s, every %s interval shown" % (human(interval), ordinal(every))
                )
                cdf_plt = gnuplot.gen_pdf_cdf(
                    os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_pdf_cdf.dat" % (job_or_task, metric)),
                    xlabel=get_type(metric).legend(get_title(metric)),
                    xvalue_notation=get_type(metric).format,
                    xlog=True
                )

                self.plot(
                    self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_over_time" % (job_or_task, metric))),
                    tme_plt,
                    size="banner", landscape = True
                )

                self.plot(
                    self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_quantiles" % (job_or_task, metric))),
                    qtl_plt,
                    size="dubbel", landscape = True
                )

                self.plot(
                    self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_cdf_pdf" % (job_or_task, metric))),
                    cdf_plt,
                    size="banner", landscape = True
                )


                self.plot(
                    self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s" % (job_or_task, metric))),
                    gnuplot.gen_multiplot(
                        [tme_plt, qtl_plt, cdf_plt],
                        title="%s, %s %s" % (self.config.get("trace", "title"), job_or_task.title(), get_title(metric))
                    )
                )

                for breakdown in job_breakdowns if job else task_breakdowns:
                    tme_plt = self.gen_cumulative(
                        os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_per_%s_%d.dat" % (job_or_task, metric, breakdown, interval)),
                        ylabel=get_type(metric).legend(get_title(metric)),
                        yvalue_notation=get_type(metric).format,
                        every=every,
                        reverse=True,
                        label="cumulative per %s, every %s interval shown" % (human(interval), ordinal(every))
                    )
                    self.plot(
                        self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_per_%s_over_time" % (job_or_task, metric, breakdown))),
                        tme_plt,
                        size="banner", landscape = True
                    )

                    try:
                        breakdown_values = self.config.get("plot", "%s_%s" % (job_or_task, breakdown))
                    except:
                        breakdown_values = ""

                    for breakdown_value in [x.strip() for x in breakdown_values.split()]:
                        tme_plt = self.gen_cumulative(
                            os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_per_%s_[%s]_%d.dat" % (job_or_task, metric, breakdown, breakdown_value, interval)),
                            ylabel=get_type(metric).legend(get_title(metric)),
                            yvalue_notation=get_type(metric).format,
                            legend=["%s Sum" % (metric)],
                            every=every,
                            label="cumulative per %s, every %s interval shown" % (human(interval), ordinal(every))
                        )
                        qtl_plt = self.gen_quantiles(
                            os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_per_%s_[%s]_%d.dat" % (job_or_task, metric, breakdown, breakdown_value, interval)),
                            ylabel=get_type(metric).legend(get_title(metric)),
                            yvalue_notation=get_type(metric).format,
                            every=every,
                            label="intervals of %s, every %s interval shown" % (human(interval), ordinal(every))
                        )
                        cdf_plt = gnuplot.gen_pdf_cdf(
                            os.path.join(self.config.get("plot", "input"), "cumulative_%s_statistics_%s_per_%s_[%s]_pdf_cdf.dat" % (job_or_task, metric, breakdown, breakdown_value)),
                            xlabel=get_type(metric).legend(get_title(metric)),
                            xvalue_notation=get_type(metric).format,
                            xlog=True
                        )

                        self.plot(
                            self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_per_%s_[%s]_over_time" % (job_or_task, metric, breakdown, breakdown_value))),
                            tme_plt,
                            size="banner", landscape = True
                        )

                        self.plot(
                            self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_per_%s_[%s]_quantiles" % (job_or_task, metric, breakdown, breakdown_value))),
                            qtl_plt,
                            size="dubbel", landscape = True
                        )

                        self.plot(
                            self.path(os.path.join(self.config.get("plot", "output"), "cumulative_%s_statistics_%s_per_%s_[%s]_cdf_pdf" % (job_or_task, metric, breakdown, breakdown_value))),
                            cdf_plt,
                            size="banner", landscape = True
                        )

    def gen_cumulative(self, file_name, **kwargs):
        kwargs.update(locals())

        if not os.path.exists(file_name):
            return ""

        matches = re.match(
            "^(?:.*/)*cumulative_(.*)_per_(.*)_(\d+)\.dat$",
            file_name
        )

        ylabel = "Count"
        title = None
        label="cumulative per time unit"
        cols = []

        if matches is not None:
            thing, breakdown, unused_binsize = matches.groups()
            ylabel = "Number of %s" % thing
            #title = "%s per %s" % (thing, breakdown)

        if not "legend" in kwargs.keys():
            try:
                reader = TRReader(file_name)
                iterator = reader.__iter__()
                cols = iterator.column_headers
            except:
                pass
            kwargs["legend"] = cols[1:]

        for key in ["ylabel", "title", "label"]:
            if key not in kwargs:
                kwargs[key] = locals()[key]

        return gnuplot.gen_over_time(
            file_name,
            cumulative=True,
            **kwargs
        )

    def gen_quantiles(self, file_name, **kwargs):
        kwargs.update(locals())

        if not os.path.exists(file_name):
            return ""

        ylabel = "Count"
        title = None
        cols = []
        ylog = True
        line_type = "lines"
        line_styles = gnuplot.QUANTILES_LINE_STYLES

        if not "legend" in kwargs.keys():
            try:
                reader = TRReader(file_name)
                iterator = reader.__iter__()
                cols = iterator.column_headers
            except:
                pass
            kwargs["legend"] = cols[1:]

        for key in ["ylabel", "ylog", "title", "line_type", "line_styles"]:
            if key not in kwargs:
                kwargs[key] = locals()[key]

        return gnuplot.gen_over_time(
            file_name,
            cumulative=False,
            skip=4,
            reverse_legend=True,
            **kwargs
        )

    def path(self, path):
        parent = os.path.dirname(path)
        if not os.path.isdir(parent):
            analyze.mkdir(parent)

        return path

    def plot(self, file_name, code, **kwargs):
        logging.info("Plotting \"%s\"" % file_name)

        if 'size' not in kwargs:
            kwargs['size'] = 'a5'

        if self.overwrite or not os.path.exists(file_name):
            if callable(code):
                code = code()

            gnuplot.plot(code, file_name, **kwargs)
        else:
            logging.info("Skipping as \"%s\" already exists!" % file_name)

if __name__ == "__main__":
    run(sys.argv[1:])
