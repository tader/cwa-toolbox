#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Import raw data into Cloud Trace Archive standard format

Options:
    -h, --help             display this text
        --doc              display pydoc for this module
    -p, --progress         show progress bar during processing
    -t, --time             measure the total processing time
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.15"

import logging
import main
import os
import sys

def run(argv):
    config = main.get_config()

    if config is None:
        logging.error("unable to load config file (usually '.cta/config')")
        sys.exit(1)

    try:
        import_input = config.get("import", "input")
    except:
        import_input = None

    try:
        import_output = config.get("import", "output")
    except:
        import_output = None

    import_module = config.get("import", "module")

    try:
        import_options = config.get("import", "options")
    except:
        import_options = None

    if import_input is None:
        import_input = []
    else:
        import_input = [import_input]

    if import_output is None:
        import_output = []
    else:
        import_output = [import_output]

    if import_options is None:
        import_options = []
    else:
        import_options = str(import_options).split()

    if import_module is None:
        logging.error("invalid configuration")
        sys.exit(1)

    module_name, module = main.get_module(import_module)
    if module is None:
        logging.error("unable to load module: '%s'" % import_module)

    if hasattr(module, "run"):
        run_function = getattr(module, "run")
        logging.info("running importer: '%s'" % str(import_options + import_input +
            import_output + argv))
        run_function(import_options + import_input + import_output + argv)
    else:
        logging.error("module '%s' does not have a function 'run'" % module_name)
        sys.exit(1)


if __name__ == "__main__":
    run(sys.argv[1:])
