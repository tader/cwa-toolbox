#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Import Hadoop Log into CWA Trace Format
    <log-path> <cwa-path>
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.11.14"

import logging
#import getopt
import sys
import os
import fnmatch
import json
import re
from utils.hadoop.trace_format import Job
from utils.tr_writer import TRWriter
import cwa.trace

def run(argv):
    #try:
    #    opts, argv = getopt.getopt(argv, "", [
    #    ])
    #except getopt.GetoptError, err:
    #    logging.error(str(err))
    #    sys.exit(2)
    #
    #for o, a in opts:
    #    if o in ("--timespan"):
    #        pass

    if len(argv) < 2:
        logging.error("See usage")
        sys.exit(3)

    importer = HadoopConverter()
    importer.convert(argv[0], argv[1])

class HadoopConverter:
    def __init__(self):
        self.fout = None
        self.jobID = 0
        self.taskID = 0
        self.hostID = 0
        self.attemptID = 0
        self.map_jobID = {}
        self.map_taskID = {}
        self.map_attemptID = {}
        self.map_hostID = {}

        self.re_exec = re.compile(r"^.*\(application[\-_ \t]*(\d+)\).*$")
        
    def read_jobs(self, src):
        for logfile in HadoopConverter.logfiles(src):
            logging.debug("Reading: %s" % logfile)
            data = self.load_log(logfile)
            if data is not None:
                yield Job.constructFromData(data)

    def convert(self, src, dst):
        self.fout = {
            'CWJ':  TRWriter(dst + ".cwj"),
            'CWT':  TRWriter(dst + ".cwt"),
            'CWJD': TRWriter(dst + ".cwjd"),
            'CWTD': TRWriter(dst + ".cwtd"),
        }

        for file_type, writer in self.fout.items():
            writer.column_titles = cwa.trace.FIELDS[file_type]
            writer.header(__file__, __version__, __author__)

        counter = 0
        for job in self.read_jobs(src):
            if job is not None:
                counter += 1
                if counter % 100 == 0:
                    logging.info("imported %d jobs" % counter)
                # reset counters and save some memory
                # Assumes jobID's are unique
                self.taskID = 0
                self.attemptID = 0
                self.map_jobID = {}
                self.map_taskID = {}
                self.map_attemptID = {}


                if job is not None:
                    record = self.process_job(job)
                    self.fout['CWJ'].writeRecord(record)
                    self.fout['CWJD'].writeRecord(record)

                    for task_record in record['_tasks']:
                        merged_task_record = self.merge_job_task_records(record, task_record)
                        self.fout['CWT'].writeRecord(merged_task_record)
                        self.fout['CWTD'].writeRecord(merged_task_record)

            #if counter > 10000:
            #    break

        for writer in self.fout.values():
            writer.close()

    def process_job(self, job):
        record = {}
        record['_tasks'] = []

        record['JobID'] = self.getJobID(job)
        record['SubmitTime'] = int(job.submitTime / 1000)
        record['WaitTime'] = job.getWaitTime()
        record['RunTime'] = job.getRunTime()
        record['StructuralChanges'] = 'MR/Detailed'
        record['UserID'] = 0
        record['ExecutableID'] = 0
        record['QueueID'] = 0
        record['PartitionID'] = 0

        # FIXME
        record['Status'] = 1
        record['DiskIORatio'] = 0
        record['Disk'] = 0
        record['Network'] = 0
        record['Memory'] = 0

        matches = self.re_exec.match(job.jobName)
        if matches:
            record['ExecutableID'] = int(matches.group(1))

        # caclulate later
        record['CPUs'] = 0
        record['TotalWallClockTime'] = 0
        record['MR_total_launched'] = 0
        record['MR_total_failed'] = 0
        record['MR_total_killed'] = 0
        record['MR_map_launched'] = 0
        record['MR_map_failed'] = 0
        record['MR_map_killed'] = 0
        record['MR_reduce_finished'] = 0
        record['MR_total_map_time'] = 0
        record['MR_total_reduce_time'] = 0

        for task in job.getTasks():
            record['_tasks'].append(
                self.process_task(job, task)
            )

        for task_record in record['_tasks']:
            record['TotalWallClockTime'] += task_record['RunTime']

            # launches
            record['MR_total_launched'] += 1
            if task_record['MR_task_type'] == 0: #map
                record['MR_map_launched'] += 1
                record['MR_total_map_time'] += task_record['RunTime']
            elif task_record['MR_task_type'] == 1: #reduce
                record['MR_total_reduce_time'] += task_record['RunTime']

            # failures
            if task_record['Status'] in [0,4]: # failed
                record['MR_total_failed'] += 1
                if task_record['MR_task_type'] == 0: #map
                    record['MR_map_failed'] += 1
            elif task_record['Status'] in [1,3]: # succes
                if task_record['MR_task_type'] == 1: #reduce
                    record['MR_reduce_finished'] += 1
            elif task_record['Status'] == 5: # killed
                record['MR_total_killed'] += 1
                if task_record['MR_task_type'] == 0: #map
                    record['MR_map_killed'] += 1

        # FIXME what is difference?
        record['MR_total_from_job'] = record['MR_total_launched']
        record['MR_map_total'] = record['MR_map_launched']

        record['CPUs'] += task_record['CPUs']

        return record

    def process_task(self, job, task):
        record = {}
        record['_attempts'] = []

        record['TaskID'] = self.getTaskID(job, task)
        record['SubmitTime'] = int(task.getStartTime() / 1000)
        record['WaitTime'] = task.getWaitTime()
        record['RunTime'] = task.getRunTime()
        record['Status'] = self.getTaskStatus(job, task)
        record['MR_task_type'] = self.getTaskType(job, task)
        record['CPUs'] = 1
        record['TotalWallClockTime'] = 0 # calculate later

        for attempt in task.attempts:
            record['_attempts'].append(self.process_attempt(job, task, attempt))

        for attempt_record in record['_attempts']:
            if attempt_record['Result'] in ["SUCCESS", "SUCCEEDED"]:
                # should be only one SUCCEEDED attempt
                record['TotalWallClockTime'] += attempt_record['RunTime']
                record['MR_task_attempt_host'] = self.getHostID(attempt_record['MR_task_attempt_host'])

        return record

    def process_attempt(self, job, task, attempt):
        record = {}
        record['Result'] = attempt.result
        record['SubmitTime'] = int(attempt.startTime / 1000)
        record['RunTime'] = attempt.getRunTime()
        record['MR_task_attempt_host'] = attempt.hostName

        return record

    def getJobID(self, job):
        if job.jobID not in self.map_jobID:
            self.map_jobID[job.jobID] = self.jobID
            self.jobID += 1

        return self.map_jobID[job.jobID]

    def getTaskID(self, job, task):
        if task.taskID not in self.map_taskID:
            self.map_taskID[task.taskID] = self.taskID
            self.taskID += 1

        return self.map_taskID[task.taskID]

    def getHostID(self, hostName):
        if hostName not in self.map_hostID:
            self.map_hostID[hostName] = self.hostID
            self.hostID += 1

        return self.map_hostID[hostName]

    def getTaskType(self, job, task):
        lookuptable = {
            'MAP': 0,
            'REDUCE': 1
        }

        if task.taskType in lookuptable:
            return lookuptable[task.taskType]
        else:
            return None

    def getTaskStatus(self, job, task):
        lookuptable = {
            'FAILED': 0,
            'SUCCESS': 1,
            'SUCCEEDED': 1,
            'KILLED': 5
        }

        if task.taskStatus in lookuptable:
            return lookuptable[task.taskStatus]
        else:
            return None

    JOB_TASK_MERGE_KEYS = [
        'JobID',
        'ExecutableID',
        'QueueID',
        'PartitionID',
        'JobProperties',
        'StructuralChanges',
        'StructuralChangeParams'
    ]

    def merge_job_task_records(self, job, task):
        record = task

        for key in HadoopConverter.JOB_TASK_MERGE_KEYS:
            if key in job and not key in task:
                record[key] = job[key]

        return record

    @staticmethod
    def logfiles(src):
        for path, unused_dirs, files in os.walk(os.path.abspath(src)):
            for filename in fnmatch.filter(files, 'job_*_*_*[!.xml]'):
                yield os.path.join(path, filename)

    def load_log(self, path):
        fp = open(path, 'r')
        data = []

        try:
            buf = fp.next()
            if buf.lower().strip() != "avro-json":
                fp.close()
                logging.debug("file '%s' does not start with 'avro-json'" % path)
                return None
        except:
            return None

        try:
            buf = fp.next()
            if buf is None or len(buf) == 0:
                fp.close()
                logging.error("file '%s' ends unexpectedly" % path)
                return None
        except:
            return None

        try:
            buf = json.loads(buf)
        except ValueError as e:
            logging.exception(e)
            logging.error("Unable to parse '%s'" % path)

        if (buf is None
            or 'namespace' not in buf
            or buf['namespace'] !=
                'org.apache.hadoop.mapreduce.jobhistory'
        ):
            fp.close()
            logging.debug("file '%s' is not a jobhistory" % path)
            return None

        for buf in fp:
            try:
                data.append(json.loads(buf))
            except ValueError as e:
                logging.exception(e)
                logging.error("Error while parsing '%s'" % path)

        fp.close()
        return data

