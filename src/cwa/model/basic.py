#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import os

import main
import logging
from cwa.model import load
from cwa.model.compare import JOB_PROPERTIES, TASK_PROPERTIES
from utils.table import Table, t, nf

import numpy as np
import scipy as sp


def run(argv):
    config = main.get_config()
    if config is None:
        logging.error("unable to load config file (usually '.cta/config')")
        sys.exit(1)

    output = config.get('model', 'output')

    filename = os.path.join(output, "properties")
    properties = load(filename)
    report_basic(output, *properties)


def report_basic(output, job_properties, task_properties):
    table = Table(os.path.join(output, "basic_statistics"))

    for n, title in enumerate([
        'Min',
        '1\%-tile',
        '10\%-tile',
        '25\%-tile',
        'Mean',
        'Median',
        '75\%-tile',
        '90\%-tile',
        '99\%-tile',
        'Max',
        'Std',
        'CoV'
    ]):
        table[1, 2 + n] = t(title, bold=True)

    row = 1
    for property, values in job_properties.items():
        values = np.sort(np.array(values))
        row += 1
        try:
            report_property(table, row, property, True, values)
        except Exception as e:
            logging.exception(e)

    for property, jobs_values in task_properties.items():
        values = []
        for job in jobs_values:
            for list_values in job.values():
                values.extend(list_values)
        values = np.sort(np.array(values))
        row += 1
        try:
            report_property(table, row, property, False, values)
        except Exception as e:
            logging.exception(e)

    table.write()


def report_property(table, row, property, is_job_property, values):
    job_or_task_string = "Job" if is_job_property else "Task"

    if is_job_property and property in JOB_PROPERTIES:
        title = JOB_PROPERTIES[property]['label']
    elif not is_job_property and property in TASK_PROPERTIES:
        title = TASK_PROPERTIES[property]['label']
    else:
        title = property

    title = "%s %s" % (job_or_task_string, title)

    logging.info("Filling: %s" % title)

    table[row, 1] = t(title, bold=True)

    for i, fun in enumerate([
        lambda values: sp.stats.tmin(values),
        lambda values: sp.stats.scoreatpercentile(values, 1),
        lambda values: sp.stats.scoreatpercentile(values, 10),
        lambda values: sp.stats.scoreatpercentile(values, 25),
        lambda values: sp.stats.tmean(values),
        lambda values: sp.stats.scoreatpercentile(values, 50),
        lambda values: sp.stats.scoreatpercentile(values, 75),
        lambda values: sp.stats.scoreatpercentile(values, 90),
        lambda values: sp.stats.scoreatpercentile(values, 99),
        lambda values: sp.stats.tmax(values, None),
        lambda values: sp.stats.tstd(values),
        lambda values: sp.stats.tstd(values) / sp.stats.tmean(values)
    ]):
        try:
            value = nf(fun(values))
        except Exception as e:
            logging.exception(e)
            value = '--'

        table[row, 2 + i] = t(value)

    if table[row, 11] == '--':
        table[row, 12] = '--'
