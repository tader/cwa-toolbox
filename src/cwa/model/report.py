#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import logging
import main

from numpy import cumsum
import matplotlib.pyplot as plt
from utils.fitter import DISTRIBUTIONS

from cwa.model import load
from cwa.model.compare import JOB_PROPERTIES, TASK_PROPERTIES



def run(argv):
    config = main.get_config()
    if config is None:
        logging.error("unable to load config file (usually '.cta/config')")
        sys.exit(1)

    output = config.get('model', 'output')

    # Job
    filename = os.path.join(output, "job_model")
    model = load(filename)
    report_job_model(model, output)

    # Tasks
    filename = os.path.join(output, "task_model")
    model = load(filename)
    report_task_model(model, output)


def report_job_model(job_model, output):
    logging.info("Creating Job Model Report")
    for property in job_model:
        logging.info("  Graphing Job %s" % property)
        counts, bin_edges = job_model[property]['fits']['empirical']
        cdf = cumsum(counts)
        m = max(cdf) * 1.0
        if m != 0.0:
            cdf = map(lambda x: x / m, cdf)
        plt.hold(True)
        plt.ylabel("CDF")
        plt.ylim(0, 1.1)

        if property in JOB_PROPERTIES:
            plt.xlabel(JOB_PROPERTIES[property]['label'])
            if JOB_PROPERTIES[property]['log']:
                axes = plt.axes()
                axes.set_xscale('log')
        else:
            plt.xlabel(property)

        # Empirical
        plt.plot(bin_edges[1:],
                 cdf,
                 label="Empirical",
                 drawstyle="steps-post",
                 linewidth=3
        )

        if job_model[property]['best'] is not None:
            best, _, _, _ = job_model[property]['best']
        else:
            best = ""

        for dist in DISTRIBUTIONS:
            if dist[1] in job_model[property]['fits']:
                if dist[1] == best:
                    kwargs = {'lw': 3}
                else:
                    kwargs = {}

                d_fit = job_model[property]['fits'][dist[1]]
                d_cdf = lambda x, *args: dist[6](
                    x,
                    d_fit['shape'],
                    d_fit['loc'],
                    d_fit['scale']
                )

                plt.plot(
                    bin_edges,
                    d_cdf(bin_edges),
                    label=dist[0],
                    drawstyle="steps-post",
                    **kwargs
                )

        plt.legend(loc="lower right")
        for f in ['png', 'pdf', 'eps']:
            plt.savefig(
                "%s/job_%s.%s" % (output, property, f),
                format=f,
                dpi=300
            )
        plt.hold(False)
        plt.clf()
        print job_model[property]['best']


def report_task_model(task_model, output):
    logging.info("Creating Task Model Report")
    for property in task_model:
        logging.info("  Graphing Task %s" % property)
        if 'model' in task_model[property]:
            for t in ['map', 'reduce']:
                for param in ['shape', 'loc', 'scale']:
                    property_task_model = task_model[property]['model'][t]
                    if len(property_task_model) == 0:
                        continue

                    counts, bin_edges = \
                        property_task_model[param]['fits']['empirical']
                    cdf = cumsum(counts)
                    m = max(cdf) * 1.0
                    cdf = map(lambda x: x / m, cdf)
                    plt.hold(True)
                    plt.ylabel("CDF")
                    plt.ylim(0, 1.1)

                    #if property in TASK_PROPERTIES:
                    #    plt.xlabel(TASK_PROPERTIES[property]['label'])
                    #    if JOB_PROPERTIES[property]['log']:
                    #        axes = plt.axes()
                    #        axes.set_xscale('log')
                    #else:
                    #    plt.xlabel(property)

                    # Empirical
                    plt.plot(
                        bin_edges[1:],
                        cdf,
                        label="Empirical",
                        drawstyle="steps-post",
                        linewidth=3
                    )

                    #if task_model[property]['best'] is not None:
                    #    best,_,_,_ = job_model[property]['best']
                    #else:
                    best = ""

                    for dist in DISTRIBUTIONS:
                        if dist[1] in property_task_model[param]['fits']:
                            if dist[1] == best:
                                kwargs = {'lw': 3}
                            else:
                                kwargs = {}

                            d_fit = property_task_model[param]['fits'][dist[1]]
                            d_cdf = lambda x, *args: dist[6](
                                x,
                                d_fit['shape'],
                                d_fit['loc'],
                                d_fit['scale']
                            )

                            plt.plot(
                                bin_edges,
                                d_cdf(bin_edges),
                                label=dist[0],
                                drawstyle="steps-post",
                                **kwargs
                            )

                    plt.legend(loc="lower right")
                    for f in ['png', 'pdf', 'eps']:
                        plt.savefig(
                            "%s/task_dist_%s_%s_%s.%s" % (
                                output,
                                t,
                                property,
                                param,
                                f
                            ),
                            format=f,
                            dpi=300
                        )
                    plt.hold(False)
                    plt.clf()
