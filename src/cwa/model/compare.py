#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os
import logging
import main
import math

from utils.table import Table, nf, pf, t
from utils.fitter import DISTRIBUTIONS, GOF_TESTS
from cwa.model import load

JOB_PROPERTIES = {
    'DiskA':        {'label': 'Disk parameter a'},
    'DiskB':        {'label': 'Disk parameter b'},
    'InterArrival': {'log': True,
                     'label': 'Inter-arrival Time (seconds)'},
    'NumTasks':     {'log': False,
                     'label': 'Total number of tasks (count)'},
    'ReduceRatio':  {'log': False,
                     'label': 'Reduce ratio (fraction of total tasks)'},
    'FailFraction': {'log': False,
                     'label': 'Fail fraction (fraction of total tasks)'},
    'ForcedQuit':   {'log': False,
                     'label': 'Forced-quit Time (seconds)'},
    'QueueID':      {'log': False,
                     'label': 'Queue ID'},
    'ExecutableID': {'log': False,
                     'label': 'Executable ID'},
    'PartitionID':  {'log': False,
                     'label': 'Partition ID'},
    'UserID':       {'log': False,
                     'label': 'User ID'},
    'GroupID':      {'log': False,
                     'label': 'Group ID'},
    'WaitTime':     {'log': False,
                     'label': 'Wait Time (seconds)'},
    'RunTime':      {'log': False,
                     'label': 'Run Time (seconds)'},
}

TASK_PROPERTIES = {
    'InterArrival': {'label': 'Inter-arrival Time (seconds)'},
    'RunTime':      {'label': 'Run Time (seconds)'},
    'DiskIORatio':  {'label': 'Disk IO Ratio'},
    'CPUs':         {'label': 'CPUs'},
    'Memory':       {'label': 'Memory'},
}

DIRECTLY_MODELED = JOB_PROPERTIES
INDIRECTLY_MODELED = TASK_PROPERTIES


def run(argv):
    models = []

    for path in argv:
        npath = main.find_cwa_instance_root(path)
        if npath is not None:
            config = main.get_config(npath)
            models.append(config)
        else:
            logging.info("Skipping: %s" % path)

    run_complex(models)
    run_relaxed(models)
    run_simple(models)


def run_simple(models):
    tables = {}

    for name, settings in INDIRECTLY_MODELED.items():
        tables[False, name] = table_directly_modeled(
            'simple_%s' % name,
            settings,
            2
        )

    for model in models:
        fill_tables_simple(model, tables)

    for table in tables.values():
        table.write()


def run_relaxed(models):
    tables = {}

    for name, settings in INDIRECTLY_MODELED.items():
        tables[None, name] = table_indirectly_matches(
            'relaxed_indirectly_matches_%s' % name,
            settings
        )

        tables[False, name] = table_indirectly_modeled(
            'relaxed_indirectly_%s' % name,
            settings
        )

    for model in models:
        fill_tables(model, tables, True)

    for table in tables.values():
        table.write()


def run_complex(models):
    tables = {}
    for name, settings in DIRECTLY_MODELED.items():
        tables[True, name] = table_directly_modeled(
            'directly_%s' % name,
            settings
        )

    for name, settings in INDIRECTLY_MODELED.items():
        tables[None, name] = table_indirectly_matches(
            'indirectly_matches_%s' % name,
            settings
        )

        tables[False, name] = table_indirectly_modeled(
            'indirectly_%s' % name,
            settings
        )

    for model in models:
        fill_tables(model, tables)

    for table in tables.values():
        table.write()


def fill_tables(model_config, tables, relaxed=False):
    output = model_config.get('model', 'output')

    try:
        # Job
        filename = os.path.join(output, "job_model")
        job_model = load(filename)
        fill_job_model_tables(model_config, job_model, tables)
    except Exception as e:
        logging.exception(e)

    # Task
    prefix = "relaxed_" if relaxed else ""

    try:
        filename = os.path.join(output, "%stask_model" % prefix)
        task_model = load(filename)
        fill_task_model_matches_tables(model_config, task_model, tables)
        fill_task_model_tables(model_config, task_model, tables)
    except Exception as e:
        if relaxed:
            logging.exception(e)
        else:
            try:
                filename = os.path.join(output, "partial_task_model")
                task_model = load(filename)
                fill_task_model_matches_tables(model_config, task_model, tables)
            except Exception as e:
                logging.exception(e)


def fill_tables_simple(model_config, tables):
    output = model_config.get('model', 'output')

    try:
        filename = os.path.join(output, "simple_task_model")
        task_model = load(filename)
        fill_simple_task_model_tables(model_config, task_model, tables)
    except Exception as e:
        logging.exception(e)


def table_directly_modeled(name, settings, start_row=1):
    table = Table(name)
    for row in xrange(1, start_row + 1):
        table[row, 1] = ''
        table.colspan(2)

    for dist in DISTRIBUTIONS:
        row = table.height + 1
        col = 1
        table[row, col] = t(dist[0], bold=True)
        col = 2

        table[row + 0, col] = t('shape', bold=True)
        table[row + 1, col] = t('loc',   bold=True)
        table[row + 2, col] = t('scale', bold=True)

        for i, gof in enumerate(GOF_TESTS):
            table[row + 3 + i, col] = t(gof[1])

        table[row + 4 + i, col] = t('D-stat.', bold=True)

    return table


def table_indirectly_modeled(name, settings):
    table = Table(name)
    table[1, 1] = ''
    table.colspan(2)
    table[2, 1] = ''
    table.colspan(2)
    table[3, 1] = ''
    table.colspan(2)
    table[4, 1] = ''
    table.colspan(2)

    for dist in DISTRIBUTIONS:
        row = table.height + 1
        col = 1
        table[row, col] = t(dist[0], bold=True)
        col = 2

        table[row + 0, col] = t('shape', bold=True)
        table[row + 1, col] = t('loc',   bold=True)
        table[row + 2, col] = t('scale', bold=True)

        for i, gof in enumerate(GOF_TESTS):
            table[row + 3 + i, col] = t(gof[1], bold=True)

        table[row + 4 + i, col] = t('D-stat.', bold=True)

    return table


def table_indirectly_matches(name, settings):
    table = Table(name)
    table[2, 1] = ''

    for dist in DISTRIBUTIONS:
        row = table.height + 1
        col = 1
        table[row, col] = t(dist[0], bold=True)

    return table


def fill_job_model_tables(model_config, job_model, tables):
    title = model_config.get('trace', 'title')
    logging.info("Filling Job Model Tables: %s" % title)

    for property in job_model:
        logging.debug("Filling %s" % property)
        try:
            table = tables[True, property]
        except KeyError:
            logging.error("No table for job property: %s" % property)
            continue

        try:
            row = 1
            col = table.width + 1

            table[row, col] = t(title, bold=True)
            row += 1

            if job_model[property]['best'] is not None:
                best, _, _, _ = job_model[property]['best']
            else:
                best = ""

            for dist in DISTRIBUTIONS:
                if dist[1] in job_model[property]['fits']:
                    d_fit = job_model[property]['fits'][dist[1]]
                    dist[1] == best
                    table[row + 0, col] = t(
                        d_fit['shape'],
                        convert=nf,
                        bold=dist[1] == best
                    )
                    table[row + 1, col] = t(
                        d_fit['loc'],
                        convert=nf,
                        bold=dist[1] == best
                    )
                    table[row + 2, col] = t(
                        d_fit['scale'],
                        convert=nf,
                        bold=dist[1] == best
                    )

                    for i, gof in enumerate(GOF_TESTS):
                        try:
                            table[row + 3 + i, col] = t(
                                d_fit['gov'][gof[1]],
                                convert=nf,
                                bold=d_fit['gov'][gof[1]] >= 0.05
                            )
                        except Exception as e:
                            logging.exception(e)
                    table[row + 4 + i, col] = t(
                        d_fit['d'],
                        convert=nf,
                        bold=dist[1] == best
                    )
                row += 4 + len(GOF_TESTS)

        except Exception as e:
            logging.exception(e)


def fill_simple_task_model_tables(model_config, task_model, tables):
    title = model_config.get('trace', 'title')
    logging.info("Filling Simple Task Model Tables: %s" % title)

    for property in task_model:
        logging.debug("Filling %s" % property)
        try:
            table = tables[False, property]
        except KeyError:
            logging.error("No table for simple task model property: %s" % property)
            continue

        try:
            row = 1
            col = table.width + 1

            table[row, col] = t(title, bold=True)
            table.colspan(2)
            start_row = row + 1

            for c, tt in enumerate(['map', 'reduce']):
                try:
                    table[start_row, col + c] = t(tt, bold=True)
                    row = start_row + 1

                    if task_model[property][tt]['best'] is not None:
                        best, _, _, _ = task_model[property][tt]['best']
                    else:
                        best = ""

                    for dist in DISTRIBUTIONS:
                        if dist[1] in task_model[property][tt]['fits']:
                            d_fit = task_model[property][tt]['fits'][dist[1]]
                            dist[1] == best
                            table[row + 0, col + c] = t(
                                d_fit['shape'],
                                convert=nf,
                                bold=dist[1] == best
                            )
                            table[row + 1, col + c] = t(
                                d_fit['loc'],
                                convert=nf,
                                bold=dist[1] == best
                            )
                            table[row + 2, col + c] = t(
                                d_fit['scale'],
                                convert=nf,
                                bold=dist[1] == best
                            )

                            for i, gof in enumerate(GOF_TESTS):
                                try:
                                    table[row + 3 + i, col + c] = t(
                                        d_fit['gov'][gof[1]],
                                        convert=nf,
                                        bold=d_fit['gov'][gof[1]] >= 0.05
                                    )
                                except Exception as e:
                                    logging.exception(e)
                            table[row + 4 + i, col + c] = t(
                                d_fit['d'],
                                convert=nf,
                                bold=dist[1] == best
                            )
                        row += 4 + len(GOF_TESTS)

                except Exception as e:
                    logging.exception(e)
        except Exception as e:
            logging.exception(e)


def fill_task_model_matches_tables(model_config, task_model, tables):
    title = model_config.get('trace', 'title')
    logging.info("Filling Task Model Matches Table: %s" % title)

    #NUMBER_OF_TASKS_PER_TRACE = {
    #    'SN2': 9365863,
    #    'Yahoo!': 27317243,
    #    'Google': 44920671
    #}

    # tail -n +7 yahoo-m.cwtd |awk -F '\t' '{ print $39 }' |grep 1 |wc -l

    NUMBER_OF_TASKS_PER_TRACE = {
        'Yahoo!': {'map': 22004024, 'reduce': 5313212},
        'Google': {'map': 44920671, 'reduce': 0}
    }

    for property in task_model:
        logging.info("  Filling Task %s" % property)
        try:
            model = task_model[property]
            model['map']['shape']['fits']['normal']
        except:
            logging.error("No model for property: %s" % property)
            continue

        try:
            table = tables[None, property]
        except:
            logging.error("No table for job property: %s" % property)
            continue

        try:
            row = 3
            col = table.width
            table[1, col + 1] = t(title, bold=True)
            table.colspan(2)

            for tt in ['map', 'reduce']:
                col += 1
                table[2, col] = t(tt, bold=True)

                try:
                    top_fits = max(filter(lambda x: not math.isnan(x), [
                        task_model[property]['best'][tt][dist[1]]
                        if dist[1] in task_model[property]['best'][tt]
                        else float('-inf')
                        for dist in DISTRIBUTIONS
                    ]))
                except Exception as e:
                    logging.exception(e)
                    top_fits = float('inf')

                for i, dist in enumerate(DISTRIBUTIONS):
                    try:
                        count = task_model[property]['best'][tt][dist[1]]
                        try:
                            total = NUMBER_OF_TASKS_PER_TRACE[title][tt]
                        except KeyError:
                            total = 0

                        if total != 0:
                            percentage = 100.0 * count / total
                            table[row + i, col] = t(
                                percentage,
                                convert=pf,
                                bold=count==top_fits
                            )
                        else:
                            table[row + i, col] = t(
                                count,
                                bold=count==top_fits
                            )
                    except:
                        table[row + i, col] = t(0, convert=pf)

        except Exception as e:
            logging.exception(e)


def fill_task_model_tables(model_config, task_model, tables):
    """
            1        2       3     4     5     6     7     8     9
                            .-----------------------------------.---
        1                   | Google                            | Ya
                            |-----------------.-----------------|---
        2                   | map             | reduce          | ma
                            |-----------------.-----------------|---
        3                   | Log-Normal      | Normal          |
                            |-----.-----.-----.-----.-----.-----|---
        4                   | sha | loc | sca | sha | loc | sca |
           .--------.-------+-----+-----+-----+-----+-----+-----+---
        5  | Normal | shape |     |     |     |     |     |     |
           |        |-------+-----+-----+-----+-----+-----+-----+---
        6  |        | loc   |     |     |     |     |     |     |
           |        |-------+-----+-----+-----+-----+-----+-----+---
        7  |        | scale |     |     |     |     |     |     |
           |        |-------+-----+-----+-----+-----+-----+-----+---
        8  |        | KS    |     |     |     |     |     |     |
           |        |-------+-----+-----+-----+-----+-----+-----+---
        9  |        | d     |     |     |     |     |     |     |
           |--------+-------+-----+-----+-----+-----+-----+-----+---
        10 | Pareto | shape |     |     |     |     |     |     |

    """
    title = model_config.get('trace', 'title')
    logging.info("Filling Task Model Table: %s" % title)

    for property in task_model:
        logging.info("  Filling Task %s" % property)
        try:
            model = task_model[property]
            model['map']['shape']['fits']['normal']
        except:
            logging.error("No model for property: %s" % property)
            continue

        try:
            table = tables[False, property]
        except:
            logging.error("No table for property: %s" % property)
            continue

        try:
            # This model contains "property", we add columns for the
            # models of he shape, loc, and scale parameters.
            # Both for map and reduce.
            col = table.width + 1
            table[1, col] = t(title, bold=True)
            table.colspan(6)

            for i, tt in enumerate(['map', 'reduce']):
                cc = col + i * 3
                table[2, cc] = t(tt, bold=True)
                table.colspan(3)

                try:
                    property_task_model = model[tt]
                except:
                    logging.info("No %s model." % tt)
                    continue

                if not property_task_model['distribution']:
                    continue

                table[3, cc] = t(
                    property_task_model['distribution'],
                    bold=True
                )
                table.colspan(3)

                for p, par in enumerate(['shape', 'loc', 'scale']):
                    table[4, cc + p] = t(par, bold=True)

                    property_task_par_model = property_task_model[par]

                    try:
                        best_dist = property_task_par_model['best'][0]
                    except:
                        best_dist = None

                    if best_dist == None:
                        try:
                            dmin = min(filter(lambda x: not math.isnan(x), [
                                property_task_par_model['fits'][dist[1]]['d']
                                for dist in DISTRIBUTIONS
                            ]))

                            for dist in DISTRIBUTIONS:
                                if (
                                 dmin ==
                                 property_task_par_model['fits'][dist[1]]['d']
                                ):
                                    best_dist = dist[1]
                                    break
                        except Exception as e:
                            logging.exception(e)
                            pass

                    for d, dist in enumerate(DISTRIBUTIONS):
                        is_best = dist[1] == best_dist

                        try:
                            fill_fit(
                                table,
                                5 + d * (4 + len(GOF_TESTS)),
                                cc + p,
                                property_task_par_model['fits'][dist[1]],
                                is_best
                            )
                        except Exception as e:
                            logging.exception(e)

        except Exception as e:
            logging.exception(e)


def fill_fit(table, row, col, fit, is_best=False):
    """
                      col

                   -+-----+
        row+0  sha  |XXXXX|
                   -+-----+
        row+1  loc  |XXXXX|
                   -+-----+
        row+2  sca  |XXXXX|
                   -+-----+
        row+3  KS   |XXXXX|
                   -+-----+
        row+4  d    |XXXXX|
                   -+-----+
                    |     |
    """
    for par in ['shape', 'loc', 'scale']:
        table[row, col] = t(
            fit[par],
            convert=nf,
            bold=is_best
        )
        row += 1

    for gof in GOF_TESTS:
        table[row, col] = t(
            fit['gov'][gof[1]],
            convert=nf,
            bold=fit['gov'][gof[1]] >= 0.05
        )
        row += 1

    table[row, col] = t(
            fit['d'],
            convert=nf,
            bold=is_best
    )
