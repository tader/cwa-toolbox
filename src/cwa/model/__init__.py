#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import logging
from time import time

try:
    import cPickle as pickle
except:
    import pickle

from utils import tspickler

import main
from utils.tr_reader import TRReader
from cwa.trace import cwjd_by_name, cwtd_by_name
from utils.fitter import fit, DISTRIBUTIONS, GOF_TESTS, ALPHA
from numpy import array, polyfit
from multiprocessing import Pool, TimeoutError
import sys
import os
from time import time, sleep

RELAXED_ALPHA = 0.02
UPDATE_INTERVAL = 600  # Progress update once every ten minutes.
FITTING_TIMEOUT = 300  # Give up on fitting a job after five minutes.


def run(argv):
    config = main.get_config()
    if config is None:
        logging.error("unable to load config file (usually '.cta/config')")
        sys.exit(1)

    modeler = Modeler(config)
    modeler.run()


def to_float_or_None(x):
    if not (x and x[0] in "0123456789."):
        return None

    try:
        return float(x)
    except:
        return None


def save(data, name):
    filename = "%s.tspickle" % name
    logging.info("TSPickle to \"%s\"" % filename)
    tspickler.save(data, filename)


def load(name):
    try:
        filename = "%s.tspickle" % name
        logging.info("Untspickle from \"%s\"" % filename)
        data = tspickler.load(filename)
        logging.info("Finished unpickle from \"%s\"" % filename)
        return data

    except:
        filename = "%s.pickle" % name
        logging.info("Unpickle from \"%s\"" % filename)
        ifp = open(filename, 'rb')
        data = pickle.load(ifp)
        ifp.close()
        logging.info("Finished unpickle from \"%s\"" % filename)
        return data


class Modeler:
    def __init__(self, config):
        self.config = config
        self.input = config.get('model', 'input')
        self.output = config.get('model', 'output')
        self.properties = None

    def run(self):
        self.generate_job_model()
        self.generate_simple_task_model()
        self.generate_relaxed_task_model()
        self.generate_task_model()
        print "Modeling results saved in \"%s/*_model.pickle\"" % (self.output)
        print "Suggestion: cwa model report"

    def generate_properties(self):
        if self.properties is None:
            logging.info("Getting job and task properties")
            try:
                self.properties = self.load("properties")
            except:
                logging.info("Unpickle failed...")
                self.properties = self.get_job_and_task_properties()
                self.save(self.properties, "properties")
                return self.properties

        return self.properties

    def generate_job_model(self):
        logging.info("Getting job model")
        try:
            return self.load("job_model")
        except:
            logging.info("Unpickle failed...")
            job_properties, _ = self.generate_properties()
            logging.info("Fitting job properties")
            job_model = self.model_job_properties(job_properties)
            self.save(job_model, "job_model")
            return job_model

    def yield_partial_task_model_items(self):
        logging.info("Getting partial task model")
        try:
            for item in self.load("partial_task_model").items():
                yield item
        except:
            logging.info("Unpickle failed...")
            _, task_properties = self.generate_properties()
            logging.info("Fitting task properties")
            for item in self.yield_model_task_properties(task_properties):
                yield item

    def generate_partial_task_model(self):
        logging.info("Getting partial task model")
        try:
            return self.load("partial_task_model")
        except:
            logging.info("Unpickle failed...")
            _, task_properties = self.generate_properties()
            logging.info("Fitting task properties")
            return self.model_task_properties(task_properties)

    def generate_task_model(self):
        logging.info("Getting task model")
        try:
            return self.load("task_model")
        except:
            logging.info("Unpickle failed...")
            task_model = self.generate_partial_task_model()
            logging.info("Fitting task properties model parameters")
            task_model = dict(map(
                model_single_task_property_distribution_parameters,
                task_model.items()
            ))
            self.save(task_model, "task_model")
            return task_model

    def generate_relaxed_task_model(self):
        logging.info("Getting relaxed task model")
        try:
            return self.load("relaxed_task_model")
        except:
            logging.info("Unpickle failed...")
            #task_model = self.generate_partial_task_model()
            logging.info("Fitting relaxed task properties model parameters")
            task_model = dict(map(
                relaxed_model_single_task_property_distribution_parameters,
                #task_model.items()
                (item for item in self.yield_partial_task_model_items())
            ))
            self.save(task_model, "relaxed_task_model")
            return task_model

    def generate_simple_task_model(self):
        logging.info("Getting simple task model")
        try:
            return self.load("simple_task_model")
        except:
            logging.info("Unpickle failed...")
            _, task_properties = self.generate_properties()
            logging.info("Fitting simple task properties model parameters")
            task_model = model_simple_task_properties(task_properties)
            self.save(task_model, "simple_task_model")
            return task_model

    def save(self, data, name):
        filename = os.path.join(self.output, name)
        save(data, filename)

    def load(self, name):
        filename = os.path.join(self.output, name)
        return load(filename)

    def get_job_and_task_properties(self):
        # task_properties ≈ {
        #     'InterArrival':[{'map':[0.0, 58.0, ...],'reduce':[...]}, ...],
        #     'RunTime':[...],
        #     ...
        # }

        logging.info("Reading job traces")
        job_info = self.read_job_info()
        logging.info("Reading task traces")
        task_info_per_job = self.read_task_info()

        job_properties = {}
        logging.info(
            "Extract various simple job properties (ExecutableID, ...)"
        )
        job_properties.update(self.extract_job_values(
            job_info, [
                'RunTime',
                'WaitTime',
                'ExecutableID',
                'UserID',
                'GroupID',
                'QueueID',
                'PartitionID'
            ]
        ))
        logging.info("Extract job inter-arrival times")
        job_properties.update(self.extract_job_inter_arrival_times(job_info))
        logging.info("Extract job forced-quit times")
        job_properties.update(self.extract_job_forced_quit_times(job_info))

        # This may cause additional large memory usage
        logging.info(
            "Extract task properties at workload level"
        )
        job_properties.update(self.extract_task_values_for_job(
            task_info_per_job,
            {'TaskRunTimes': 'RunTime'}
        ))

        if len(task_info_per_job) > 0:
            logging.info("Extract reduce fraction")
            job_properties.update(self.extract_numtasks_ratio_from_tasks(
                task_info_per_job
            ))
            logging.info("Extract run time vs disk usage")
            job_properties.update(self.extract_runtime_vs_disk(
                task_info_per_job
            ))
            logging.info("Extract fail fraction")
            job_properties.update(self.extract_fail_ratio_from_tasks(
                task_info_per_job
            ))
        else:
            logging.info("Extract reduce ratios")
            job_properties.update(self.extract_numtasks_ratio(job_info))
            logging.info("Extract fail fraction")
            job_properties.update(self.extract_fail_ratio(job_info))

        task_properties = {}
        logging.info("Extract task inter-arrival times")
        task_properties.update(self.extract_task_inter_arrival_times(
            task_info_per_job
        ))
        logging.info(
            "Extract various simple task properties (RunTime, ...)"
        )
        task_properties.update(self.extract_task_values(task_info_per_job, [
            'RunTime',
            'DiskIORatio',
            'CPUs',
            'Memory'
        ]))
        # Disk is derived from the run time!

        logging.info("Clean empty job properties")
        properties = job_properties.keys()
        for property in properties:
            if len(job_properties[property]) == 0:
                del job_properties[property]

        return job_properties, task_properties

    def read_job_info(self):
        job_info = {}

        reader = TRReader('%s.cwjd' % self.input)
        reader.progress = True

        try:
            for record in reader:
                JobID = cwjd_by_name(record, 'JobID')
                job_info[JobID] = map(to_float_or_None, record)
        except StopIteration:
            pass

        return job_info

    def read_task_info(self):
        task_info_per_job = {}

        reader = TRReader('%s.cwtd' % self.input)
        reader.progress = True

        try:
            for record in reader:
                JobID = cwtd_by_name(record, 'JobID')
                record = map(to_float_or_None, record)
                try:
                    task_info_per_job[JobID].append(record)
                except KeyError:
                    task_info_per_job[JobID] = [record]

        except StopIteration:
            pass

        return task_info_per_job

    def extract_job_values(self, job_info, fields):
        result = {}
        for field in fields:
            result[field] = []

        for JobID in job_info:
            for field in fields:
                value = cwjd_by_name(job_info[JobID], field)
                if value is not None:
                    result[field].append(value)

        return result

    def extract_job_inter_arrival_times(self, job_info):
        result = []
        submit_times = []

        for JobID in job_info:
            value = cwjd_by_name(job_info[JobID], 'SubmitTime')
            if value is not None:
                submit_times.append(value)

        if len(submit_times) > 1:
            submit_times.sort()
            last = submit_times[0]
            for submit_time in submit_times[1:]:
                result.append(submit_time - last)
                last = submit_time

        return {'InterArrival': result}

    def extract_job_forced_quit_times(self, job_info):
        result = []

        for JobID in job_info:
            if cwjd_by_name(job_info[JobID], 'Status') == 5:
                rvalue = cwjd_by_name(job_info[JobID], 'RunTime')
                wvalue = cwjd_by_name(job_info[JobID], 'WaitTime')

                if rvalue is not None and wvalue is not None:
                    fqvalue = rvalue + wvalue
                elif rvalue is not None:
                    fqvalue = rvalue
                elif wvalue is not None:
                    fqvalue = wvalue
                else:
                    fqvalue = 1.0

                result.append(fqvalue)
            else:
                result.append(-1.0)

        return {'ForcedQuit': result}

    def extract_numtasks_ratio(self, job_info):
        count = []
        ratio = []

        for JobID in job_info:
            t_from_job = cwjd_by_name(job_info[JobID], 'MR_total_from_job')
            t_launched = cwjd_by_name(job_info[JobID], 'MR_total_launched')

            m_from_job = cwjd_by_name(job_info[JobID], 'MR_map_total')
            m_launched = cwjd_by_name(job_info[JobID], 'MR_map_launched')

            if t_launched >= 0.0:
                total = t_launched
            elif t_from_job >= 0.0:
                total = t_from_job
            else:
                continue

            if m_launched >= 0.0:
                maps = m_launched
            elif m_from_job >= 0.0:
                maps = m_from_job
            else:
                continue

            reduces = total - maps

            count.append(total)
            if total > 0.0:
                ratio.append(reduces / total * 1.0)

        return {'NumTasks': count, 'ReduceRatio': ratio}

    def extract_fail_ratio(self, job_info):
        ratio = []

        for JobID in job_info:
            t_from_job = cwjd_by_name(job_info[JobID], 'MR_total_from_job')
            t_launched = cwjd_by_name(job_info[JobID], 'MR_total_launched')
            failed = cwjd_by_name(job_info[JobID], 'MR_total_failed')
            killed = cwjd_by_name(job_info[JobID], 'MR_total_killed')

            m_total = cwjd_by_name(job_info[JobID], 'MR_map_total')
            m_failed = cwjd_by_name(job_info[JobID], 'MR_map_failed')
            m_killed = cwjd_by_name(job_info[JobID], 'MR_map_killed')

            if t_launched >= 0.0:
                total = t_launched
            elif t_from_job >= 0.0:
                total = t_from_job
            else:
                continue

            if m_total > total:
                total = m_total

            f = 0
            if failed is not None:
                f += failed
            if killed is not None:
                f += killed

            m = 0
            if m_failed is not None:
                m += m_failed
            if m_killed is not None:
                m += m_killed

            if m > f:
                f = m

            if total > 0.0:
                ratio.append(f / total * 1.0)

        return {'FailFraction': ratio}

    def extract_numtasks_ratio_from_tasks(self, task_info_per_job):
        count = []
        ratio = []

        for JobID in task_info_per_job:
            total = len(task_info_per_job[JobID])
            reduces = 0
            for record in task_info_per_job[JobID]:
                if cwtd_by_name(record, "MR_task_type") == 1:
                    reduces += 1

            count.append(total)
            if total > 0:
                ratio.append(reduces / total * 1.0)

        return {'NumTasks': count, 'ReduceRatio': ratio}

    def extract_fail_ratio_from_tasks(self, task_info_per_job):
        ratio = []

        for JobID in task_info_per_job:
            total = len(task_info_per_job[JobID])
            failed = 0
            for record in task_info_per_job[JobID]:
                status = cwtd_by_name(record, "Status")
                if status is not None and int(status) in [0, 4, 5]:
                    failed += 1

            if total > 0:
                ratio.append(failed / total * 1.0)

        return {'FailFraction': ratio}

    def extract_runtime_vs_disk(self, task_info_per_job):
        disk_a = []
        disk_b = []

        for JobID in task_info_per_job:
            runtimes = []
            disks = []
            for record in task_info_per_job[JobID]:
                r = cwtd_by_name(record, 'RunTime')
                d = cwtd_by_name(record, 'Disk')

                if r is not None and d is not None:
                    runtimes.append(r)
                    disks.append(d)

            if len(runtimes) == 0:
                continue

            p = polyfit(array(runtimes), array(disks), 1)

            disk_a.append(p[1])
            disk_b.append(p[0])

        return {'DiskA': disk_a, 'DiskB': disk_b}

    def extract_task_values_for_job(self, task_info_per_job, fields):
        result = {}
        for key in fields.keys():
            result[key] = []

        for JobID in task_info_per_job:
            for record in task_info_per_job[JobID]:
                for key, field in fields.items():
                    value = cwtd_by_name(record, field)
                    if value is not None:
                        result[key].append(value)

        return result

    def extract_task_values(self, task_info_per_job, fields):
        result = {}
        for field in fields:
            result[field] = []

        for JobID in task_info_per_job:
            job_result = {}
            for field in fields:
                job_result[field] = {'map': [], 'reduce': []}

            for record in task_info_per_job[JobID]:
                if cwtd_by_name(record, "MR_task_type") == 1:
                    t = 'reduce'
                else:
                    t = 'map'

                for field in fields:
                    value = cwtd_by_name(record, field)
                    if value is not None:
                        job_result[field][t].append(value)

            for field in fields:
                result[field].append(job_result[field])

        return result

    def extract_task_inter_arrival_times(self, task_info_per_job):
        result = []

        for JobID in task_info_per_job:
            submit_times = {'map': [], 'reduce': []}
            inter_arrival_times = {'map': [], 'reduce': []}

            for record in task_info_per_job[JobID]:
                if cwtd_by_name(record, "MR_task_type") == 1:
                    t = 'reduce'
                else:
                    t = 'map'

                s = cwtd_by_name(record, 'SubmitTime')
                if s is not None:
                    submit_times[t].append(s)

            for t in ['map', 'reduce']:
                if len(submit_times[t]) > 1:
                    submit_times[t].sort()

                    last = submit_times[t][0]
                    for submit_time in submit_times[t][1:]:
                        inter_arrival_times[t].append(submit_time - last)
                        last = submit_time

            result.append(inter_arrival_times)

        return {'InterArrival': result}

    def model_job_properties(self, job_properties):
        pool = Pool(8)
        result = dict(pool.map(
            model_single_job_property,
            job_properties.items()
        ))
        pool.close()
        pool.join()
        return result

    def yield_model_task_properties(self, task_properties):
        for key, values in task_properties.items():
            try:
                r = self.load('tasks-%s' % key)
            except:
                logging.info("Unpickle failed...")
                r = model_single_task_property((key, values))
                self.save(r, 'tasks-%s' % key)

            yield r

    def model_task_properties(self, task_properties):
        result = []
        for key, values in task_properties.items():
            try:
                r = self.load('tasks-%s' % key)
            except:
                logging.info("Unpickle failed...")
                r = model_single_task_property((key, values))
                self.save(r, 'tasks-%s' % key)

            result.append(r)
        return dict(result)


def model_single_job_property(item):
    property, values = item

    logging.debug("Fitting %s of Jobs (%d jobs)" % (property, len(values)))
    best, fits = fit(values)

    return property, {'fits': fits, 'best': best}


def model_single_task_property(item):
    property, list_of_values = item

    model_fits = {'map': [], 'reduce': []}
    model_best = {'map': {}, 'reduce': {}}

    total = len(list_of_values)
    done = 0
    last_update = None

    pool = TimeoutPool(8, FITTING_TIMEOUT)
    for (map_best, map_fits, map_count,
         reduce_best, reduce_fits, reduce_count) in pool.imap_unordered(
                model_single_task_property_job, list_of_values):
        if map_fits is not None:
            model_fits['map'].append(map_fits)

        if reduce_fits is not None:
            model_fits['reduce'].append(reduce_fits)

        if map_best is not None and len(map_best) > 0:
            if map_best[0] in model_best['map']:
                model_best['map'][map_best[0]] += map_count
            else:
                model_best['map'][map_best[0]] = map_count

        if reduce_best is not None and len(reduce_best) > 0:
            if reduce_best[0] in model_best['reduce']:
                model_best['reduce'][reduce_best[0]] += reduce_count
            else:
                model_best['reduce'][reduce_best[0]] = reduce_count

        done += 1
        now = time()
        if last_update is None or now - last_update > UPDATE_INTERVAL:
            last_update = now
            logging.info("%s done %d of %d (%.2f%%)" % (
                property,
                done,
                total,
                done * 100.0 / total
            ))
        logging.debug("%s done %d of %d" % (property, done, total))

    logging.info("%s done!" % (property))

    pool.close()
    pool.join()

    logging.debug("%s" % str(model_best))

    model = {}
    model['fits'] = model_fits
    model['best'] = model_best
    model['model'] = {'map': {}, 'reduce': {}}
    return property, model


def model_single_task_property_job(values):
    results = []

    for t in ['map', 'reduce']:
        if len(values[t]) == 0:
            results.extend([None, None, 0])
        else:
            job_best, job_fits = fit(values[t])
            results.extend([job_best, job_fits, len(values[t])])

    return tuple(results)


def model_single_task_property_distribution_parameters(item, alpha=ALPHA):
    property, model = item

    result = {}
    try:
        logging.info(
            "Task %s best fits: %s" % (
                property,
                str(model['best'])
            )
        )

        result['best'] = model['best']

        for t in ['map', 'reduce']:
            best = None
            count = None
            for d, c in model['best'][t].items():
                if best is None:
                    best = d
                    count = c
                elif c > count:
                    best = d
                    count = c

            if best is None:
                continue

            logging.info(
                "%s-task %s is best modeled by: %s" % (
                    t,
                    property,
                    best
                )
            )

            result[t] = {
                'distribution': best,
                'shape':        None,
                'loc':          None,
                'scale':        None
            }

            logging.info(
                "Gathering %s values for shape, loc, and scale" % property
            )
            values = {'shape': [], 'loc': [], 'scale': []}
            for f in model['fits'][t]:
                if best in f:
                    skip = False
                    for gof in GOF_TESTS:
                        try:
                            if f[best]['gov'][gof[1]] < alpha:
                                skip = True
                                continue
                        except Exception as e:
                            logging.exception(e)

                    # Only use good fitting parameters
                    if skip:
                        continue

                    for i in ['shape', 'loc', 'scale']:
                        values[i].append(f[best][i])

            for i in ['shape', 'loc', 'scale']:
                logging.info(
                    "Fitting %s dist %s parameter %s (%d %s fits)" % (
                        property,
                        best,
                        i,
                        len(values[i]),
                        t
                    )
                )
                job_best, job_fits = fit(values[i])
                result[t][i] = {'best': job_best, 'fits': job_fits}
    except:
        pass

    return property, result

def relaxed_model_single_task_property_distribution_parameters(item):
    property, model = item
    model = sanitize(model)

    return model_single_task_property_distribution_parameters(
        (property, model),
        RELAXED_ALPHA
    )

def sanitize(model):
    best = {
        'map': {k[1]: 0 for k in DISTRIBUTIONS},
        'reduce': {k[1]: 0 for k in DISTRIBUTIONS}
    }

    # check missing gof tests here
    # ...

    # loop map / reduce
    for t in best.keys():
        if t in model['fits']:
            # loop all jobs
            for job_fits in model['fits'][t]:
                for dist in DISTRIBUTIONS:
                    dist = dist[1]  # Set dist to the short name of the distr.
                    try:
                        good_fit = True
                        for gof in GOF_TESTS:
                            if job_fits[dist]['gov'][gof[1]] < RELAXED_ALPHA:
                                good_fit = False
                                break
                        if good_fit:
                            best[t][dist] += job_fits[dist]['n']
                    except Exception as e:
                        logging.exception(e)

    model['best'] = best
    return model


def model_simple_task_properties(task_properties):
    pool = TimeoutPool(8, 3600 * 4)
    return dict(x for x in pool.imap_unordered(
        model_single_simple_task_property,
        merge_task_properties(task_properties).items()
    ))


def merge_task_properties(task_properties):
    result = {}

    for property, jobs_values in task_properties.items():
        result[property] = {'map': [], 'reduce': []}
        logging.info("Merge %s jobs..." % property)

        for j, job in enumerate(jobs_values):
            if j % 1000 == 0:
                logging.info("  Merging job %d of %d." % (
                    j,
                    len(jobs_values)
                ))

            for t in ['map', 'reduce']:
                try:
                    result[property][t].extend(job[t])
                except KeyError:
                    pass

    return result


def model_single_simple_task_property(item):
    property, values = item

    result = {'map': {}, 'reduce': {}}

    for t in ['map', 'reduce']:
        logging.info("Fitting simple %s %s" % (
            t,
            property
        ))

        if len(values[t]) > 0:
            try:
                best, fits = fit(values[t])
                result[t] = {'fits': fits, 'best': best}
            except Exception as e:
                logging.exception(e)

    return property, result


class TimeoutPool:
    def __init__(self, processes=4, timeout=300):
        self.pool = Pool(processes)
        self.processes = processes
        self.timeout = timeout
        self.running = {}

    def close(self):
        self.terminate()

    def join(self):
        self.pool.join()

    def terminate(self):
        self.pool.terminate()

    def imap_unordered(self, function, values):
        iterator = iter(values)
        input_available = True

        while input_available or len(self.running) > 0:
            try:
                item = iterator.next()
                p = self.pool.apply_async(function, (item,))
                self.running[p] = time()
            except StopIteration:
                input_available = False

            now = time()
            for p, start_time in self.running.items():
                try:
                    result = p.get(0)
                    del self.running[p]
                    yield result
                except TimeoutError:
                    if now - start_time > self.timeout:
                        logging.info(
                            "Terminating TimeoutError process of age %f"
                            % (now - start_time)
                        )

                        try:
                            del self.running[p]
                        except Exception as e:
                            logging.exception(e)

            sleep(0.1)
