#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Generate a synthetic workload based on a model.

Options:

    Size of the workload. These options complement each other, jobs are
    generated until all conditions are met.
    (default: --jobs 1)

    --timespan <hours>    Generate jobs submitted during at least <hours> hours.

    --jobs <jobs>         Generate at least <jobs> jobs.

    --tasks <tasks>       Generate jobs until at least <tasks> tasks have been
                          generated.

    --attempts <attempts> Generate jobs until at least <attempts> task attempts
                          have been generated.

    --runtime <hours>     Generate jobs until the cumulative runtime of all
                          task attempts is at least <hours> hours.

    --load <level>        Generate a workload with at load level <level>. The
                          level signifies the average number of task attempts
                          over time in the system.

    --load-error <percentage> Allow for <percentage> error in the generated
                          workload load.

    Cheating.
    (default: --scale 1000 --seed None)

    --scale <msecs>       A second contains <msecs> milliseconds. This setting
                          scales the interarrival, wait, and run times.

    --seed <seed>         Use <seed> as seed for the random number generator.
                          This can be used to generate the same workloads,
                          given the same inputs. If the "None" is given as
                          <seed> value, the current system time is used, or
                          os.urandom() if available on the current platform.

    --start <time>        Use unix timestamp <time> as start time for the
                          workload generation. Use with --seed <seed> to get
                          identical generated workloads.

Mandatory arguments to long options are mandatory for short options too.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.11.14"

import logging
import getopt
import sys
import random
import time
import json
from utils.hadoop.trace_format import Job, Task, Attempt, Formatter

WEEK = 60 * 60 * 24 * 7

def run(args):
    try:
        opts, argv = getopt.getopt(args, "", [
            "timespan=",
            "jobs=",
            "tasks=",
            "attempts=",
            "runtime=",
            "load=",
            "load-error=",
            "scale=",
            "seed=",
            "start="
        ])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(2)

    generator = WorkloadGenerator()

    for o, a in opts:
        if o in ("--timespan"):
            generator.setTimespan(int(a))
        elif o in ("--jobs"):
            generator.setJobs(int(a))
        elif o in ("--tasks"):
            generator.setTasks(int(a))
        elif o in ("--attempts"):
            generator.setAttempts(int(a))
        elif o in ("--runtime"):
            generator.setRuntime(int(a))
        elif o in ("--load"):
            generator.setLoad(int(a))
        elif o in ("--load-error"):
            generator.setLoadError(int(a))
        elif o in ("--scale"):
            Job.SCALE_TIME = int(a) if int(a) > 0 else Job.SCALE_TIME
        elif o in ("--seed"):
            generator.setSeed(int(a))
        elif o in ("--start"):
            generator.setStartTime(int(a))
        else:
            logging.error("Unknown option: %s" % o)

    if len(argv) < 1:
        sys.exit(2)

    if len(argv) > 1:
        generator.setOutputFile(argv[1])

    generator.loadModel(argv[0])
    generator.generate()

DISTS = {
    'weibull':   (random.weibullvariate, 2),
    'pareto':    (random.paretovariate,  1),
    'normal':    (random.normalvariate,  2),
    'lognormal': (random.lognormvariate, 2),
    'gamma':     (random.gammavariate,   2),
    'exp':       (random.expovariate,    1)
}

def bounded_sample(min_value, max_value, dist, *args, **kwargs):
    return max(min(sample(dist, *args, **kwargs),max_value),min_value)

def sample(dist, *args, **kwargs):
    """Sample a value from a distribution.

    Arguments:
        dist - Name of distribution to sample from (string).
        *args,**kwargs - Parameters for the given distribution (floats).

    Distributions:
        Single Parameter:
            pareto
            exp

        Two Parameters:
            weibull
            normal
            lognormal
            gamma

    Returns:
        A float value sampled from the distribution.
    """
    if dist in DISTS:
        (fun, nargs) = DISTS[dist]
        if len(args) + len(kwargs) == nargs:
            return fun(*args, **kwargs)
        else:
            logging.error("distribution %s expects %d arguments, not %d" % (dist, nargs, len(args) + len(kwargs)))
    else:
        logging.error("distribution %s unknown" % dist)


class WorkloadGenerator:
    def __init__(self):
        self.total_jobs          = 0
        self.total_tasks         = 0
        self.total_attempts      = 0
        self.total_runtime       = 0
        self.generate_timespan   = 0
        self.generate_jobs       = 1
        self.generate_tasks      = 0
        self.generate_attempts   = 0
        self.generate_runtime    = 0
        self.generate_load       = None
        self.generate_load_error = 5
        self.generate_seed       = None
        self.generate_start_time = time.time()
        self.generate_iat_multiplier = 1.0
        self.too_high_iat_multiplier = None
        self.too_low_iat_multiplier = 0
        self.change_multiplier   = 10
        self.model               = {}
        self.arrivalTimes        = []
        self.runTimes_sum        = 0
        self.runTimes_count      = 0
        self.output_file         = None        
        self.last_job_submit_time = 0
        self.last_job_longest_task_runtime = 0

    def setTimespan(self, x):  self.generate_timespan   = int(x) if int(x) >= 0 else 0
    def setJobs(self, x):      self.generate_jobs       = int(x) if int(x) >= 0 else 0
    def setTasks(self, x):     self.generate_tasks      = int(x) if int(x) >= 0 else 0
    def setAttempts(self, x):  self.generate_attempts   = int(x) if int(x) >= 0 else 0
    def setRuntime(self, x):   self.generate_runtime    = int(x) if int(x) >= 0 else 0
    def setLoad(self, x):      self.generate_load       = int(x) if int(x) >= 0 else None
    def setLoadError(self, x): self.generate_load_error = int(x) if int(x) >= 0 else 100
    def setStartTime(self, x): self.generate_start_time = int(x)
    def setSeed(self, x):      self.generate_seed       = int(x) if int(x) >= 0 else None
    def setOutputFile(self, x): self.output_file = x if x != "-" else None

    def loadModel(self, filename):
        try:
            fp = open(filename, "r")
            model = json.load(fp)
            fp.close()
        except Exception as e:
            logging.error("Failed to read model from file: %s" % filename)
            logging.exception(e)
            sys.exit(3)
        else:
            logging.debug("Loaded model from file: %s" % filename)
            logging.debug(model)
            self.model = model
        finally:
            try:
                fp.close()
            except:
                pass
            
    def recordArrivalTime(self, arrivalTime):
        self.arrivalTimes.append(arrivalTime)
    
    def recordRunTime(self, runTime):
        self.runTimes_sum   += runTime
        self.runTimes_count += 1

    def generate(self):
        last_load = None
        forbid_seeding = False
        for_real = self.generate_load is None
        
        while True:
            print >>sys.stderr, "Current IAT Multiplier: %f" % self.generate_iat_multiplier

            # Re-Seeding might speed-up finding a fitting workload,
            # but it might also prevent it... So, disable if we keep
            # generating the exact same load...
            
            if not forbid_seeding:
                random.seed(self.generate_seed)
                
            avgLoad = self.real_generate(for_real)
                        
            if avgLoad == last_load: forbid_seeding = True
            last_load = avgLoad
            
            if self.generate_load is None:
                break
            else:
                allow_error = self.generate_load * self.generate_load_error / 100.0
                error = self.generate_load - avgLoad
                print >>sys.stderr, "Error: %f Margin: %f" % (error, allow_error)
                
                if allow_error - abs(error) > 0:
                    if for_real:
                        break
                    else:
                        for_real = True
                    
                if (error > 0):
                    self.too_high_iat_multiplier = self.generate_iat_multiplier 
                else:
                    self.too_low_iat_multiplier = self.generate_iat_multiplier

                if self.too_high_iat_multiplier is None:
                    errorFraction = abs(allow_error/(error * self.change_multiplier))                        
                    self.generate_iat_multiplier /= errorFraction
                    self.change_multiplier = max(1.5, self.change_multiplier * 0.5)
                    self.generate_iat_multiplier = max(self.generate_iat_multiplier,self.too_low_iat_multiplier)
                else:
                    self.generate_iat_multiplier = (self.too_high_iat_multiplier + self.too_low_iat_multiplier) / 2.0                      

                # clean environment
                self.total_jobs          = 0
                self.total_tasks         = 0
                self.total_attempts      = 0
                self.total_runtime       = 0
                
                self.runTimes_count = 0
                self.runTimes_sum   = 0
                self.arrivalTimes   = []
                self.last_job_submit_time = 0
                    
            
        
    def real_generate(self, for_real=True):
        if for_real:
            if self.output_file is None:
                f = Formatter()
            else:
                ofp = open(self.output_file, 'w')
                f = Formatter(ofp)
            
        current_time = int(self.generate_start_time * Job.SCALE_TIME)
        finish_time  = int(current_time + (self.generate_timespan * 3600 * Job.SCALE_TIME))
        runtime      = int(self.generate_runtime * 3600 * Job.SCALE_TIME)

        next_submit = {}
        for klass in self.model.keys():
            next_submit[klass] = int(
                current_time
                + (
                   (self.generate_iat_multiplier * bounded_sample(0, WEEK, *self.model[klass]['interarrivaltime']))
                   * Job.SCALE_TIME
                )
            )

        while (
            current_time < finish_time
            or
            self.total_runtime < runtime
            or
            self.total_jobs < self.generate_jobs
            or
            self.total_tasks < self.generate_tasks
            or
            self.total_attempts < self.generate_attempts
        ):
            current_time = min(next_submit.values())

            for klass, submit_time in next_submit.items():
                if submit_time == current_time:
                    self.last_job_longest_task_runtime = 0
                    job = self.generate_job(submit_time, self.model[klass])
                    job.jobName = "%s (%s)" % (job.jobName, str(klass))
                    job.user = str(klass)
                    if for_real:
                        f.format_job(job)
                    self.total_jobs     += 1
                    self.total_tasks    += job._total_tasks
                    self.total_attempts += job._total_attempts
                    self.total_runtime  += job._total_runtime
                    
                    if job.submitTime < self.last_job_submit_time:
                        logging.error("Generated job submitTime in the past!")                        
                    self.last_job_submit_time = job.submitTime

                    next_submit[klass] = int(
                        current_time
                        + (
                           (self.generate_iat_multiplier * bounded_sample(0, WEEK, *self.model[klass]['interarrivaltime']))
                           * Job.SCALE_TIME
                        )
                    )
        else:
            self.arrivalTimes.sort()
            interArrivalTimes = [j-i for i, j in zip(self.arrivalTimes[:-1], self.arrivalTimes[1:])]
            
            avgRunTime = (self.runTimes_sum / self.runTimes_count / Job.SCALE_TIME) if self.runTimes_count > 0 else 0.0
            avgArrivalTime = (sum(interArrivalTimes) / (len(interArrivalTimes) * 1.0) / Job.SCALE_TIME) if len(interArrivalTimes) > 0 else 0.0
            arrivalRate = (1.0 / avgArrivalTime) if avgArrivalTime > 0 else 0
            avgLoad = arrivalRate * avgRunTime
            
            meanLoad = self.total_runtime / (self.last_job_submit_time - (self.generate_start_time * Job.SCALE_TIME) + self.last_job_longest_task_runtime)
            
            print >>sys.stderr, """Done:

                current_time (%d) < (%d) finish_time
                or
                self.total_runtime (%d) < (%d) runtime
                or
                self.total_jobs (%d) <  (%d) self.generate_jobs
                or
                self.total_tasks (%d) < (%d) self.generate_tasks
                or
                self.total_attempts (%d) < (%d) self.generate_attempts
                
                average task attempt:
                    - runtime: %f seconds
                    - inter arrival time: %f seconds
                    - arrival rate: %f/second
                    - load: %f (Little's Law)
                    - load: %f (Total Run time / Time span)
            """ % (
                current_time, finish_time,
                self.total_runtime, runtime,
                self.total_jobs, self.generate_jobs,
                self.total_tasks, self.generate_tasks,
                self.total_attempts, self.generate_attempts,
                avgRunTime, avgArrivalTime, arrivalRate, avgLoad, meanLoad
            )
            
            if for_real:
                ofp.close()
            return meanLoad

    def generate_job(self, submit_time, job_model):
        job = Job()

        job.submitTime = submit_time
        job.launchTime = int(job.submitTime
                             + (bounded_sample(0, WEEK, *job_model['waittime'])*Job.SCALE_TIME))

        numtasks = max(1,int(sample(*job_model['numtasks'])))
        ratio    = max(0, min(1,sample(*job_model['mapreduceratio'])))
        maps     = max(1,int(numtasks*ratio))
        reduces  = max(0,numtasks - maps)

        Task.counter_taskID = 0
        task_time = job.launchTime
        for unused_i in xrange(maps):
            task, task_time = self.generate_task(task_time, job_model)
            job.addTask(task)
            job.finishTime = max(job.launchTime, job.finishTime, task.finishTime)

        for unused_i in xrange(reduces):
            task, task_time = self.generate_task(task_time, job_model)
            task.taskType = Task.TYPE_REDUCE
            job.addTask(task)
            job.finishTime = max(job.launchTime, job.finishTime, task.finishTime)

        job.heapMegabytes = 640

        return job

    def generate_task(self, current_time, task_model):
        task = Task()

        task.submitTime = int(current_time
                             + (bounded_sample(0,WEEK,*task_model['attempt_interarrivaltime'])*Job.SCALE_TIME))

        task.launchTime = int(task.submitTime
                             + (bounded_sample(0,WEEK,*task_model['waittime'])* Job.SCALE_TIME))
        # caluclate launch time from first attempt

        attempt_time = task.launchTime
        attempt, attempt_time = self.generate_attempt(attempt_time, task_model)
        task.addAttempt(attempt)

        task.finishTime = attempt.finishTime

        return task, task.submitTime

    def generate_attempt(self, current_time, attempt_model):
        waitTime = bounded_sample(0,WEEK,*attempt_model['waittime']) * Job.SCALE_TIME
        runTime  = bounded_sample(0,WEEK,*attempt_model['runtime'])  * Job.SCALE_TIME
        
        attempt = Attempt()
        attempt.startTime  = int(current_time      + waitTime)        
        attempt.finishTime = int(attempt.startTime + runTime)
        
        self.last_job_longest_task_runtime = max(self.last_job_longest_task_runtime, runTime)

        self.recordArrivalTime(attempt.startTime)
        self.recordRunTime(runTime)
        
        return attempt, attempt.startTime

