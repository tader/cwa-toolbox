#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Count IO breakdown by executable id

Takes some files as input and outputs to a file.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.17"

from utils.time_line import TimeLine
from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
import getopt
import sys
import logging

SUBMIT_TIME = "SubmitTime"
WAIT_TIME = "WaitTime"
RUN_TIME = "RunTime"

def read_input(filename, timeline, variable, breakdown, progress=False):
    if progress:
        print "Reading '%s':" % filename
    reader = TRReader(filename)
    reader.return_as_dict = True
    #reader.max_columns = 9
    reader.progress = progress

    for record in reader:
        try:
            index = -1
            if breakdown in record and record[breakdown] != "None":
                index = int(float(record[breakdown]))
                
            #for offset in xrange(0, int(record[RUN_TIME]), timeline._binwidth):
            timeline.add(
                int(float(record[SUBMIT_TIME]))
                    + int(float(record[WAIT_TIME])),
                    #+ offset,
                int(float(record[variable])) if record[variable] != "None" else 0,
                index
            )
        except ValueError as e:
            logging.exception(e)

    return timeline

def write_output(outputfile, timeline, binwidth, progress=False):
    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Time"] + generate_column_headers(timeline)
    writer.header(__name__, __version__, __author__)
    writer.write(generate_output(timeline.timeline(binwidth)))

def generate_output(timeline_iterator):
    for time, values in timeline_iterator:
        yield [time] + [values[key] for key in sorted(values.keys())]

def generate_column_headers(timeline):
    return ["Exe_%d" % id for id in sorted(map(int, timeline._keys))]

def usage():
    print """use --help for usage"""

def run(argv):
    try:
        opts, argv = getopt.getopt(
            argv,
            "p",
            ["progress", "variable=", "breakdown="]
        )
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    variable_of_interest = "Disk"
    breakdown = "ExecutableID"

    for o, a in opts:
        if o in ("-p", "--progress"):
            progress = True
        elif o in ("--variable"):
            variable_of_interest = a
        elif o in ("--breakdown"):
            breakdown = a if a is not "None" else None

    if len(argv) < 2:
        usage()
        sys.exit(2)

    process_data(argv[:-1], variable_of_interest, breakdown, argv[-1], progress)

def process_data(input_files, variable, breakdown, output_basename, progress = False):
    timeline = TimeLine(600)
    for filename in input_files:
        read_input(filename, timeline, variable, breakdown, progress)

    name_parts = output_basename.split(".")
    # I guess serial I/O is faster than parallel I/O?
    for binwidth in [60 * 10, 60 * 60, 60 * 60 * 24]:
        filename = ".".join(name_parts[:-2] + [ name_parts[-2] + "_" + str(binwidth), name_parts[-1]])
        write_output(filename, timeline, binwidth, progress)


if __name__ == "__main__":
    run(sys.argv[1:])
