#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Calculates statistics over time

Takes some files as input and outputs to some files.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.11.23"

from utils.time_line import TimeLine
from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
from utils.progress_bar import ProgressBar
import getopt
import logging
import sys
import bisect
import operator

SUBMIT_TIME = "SubmitTime"
WAIT_TIME = "WaitTime"
RUN_TIME = "RunTime"

DEFAULT_PERCENTILES = [
     0, 0.01, 0.1,
     1, 2, 5,
    25, 50, 75,
    95, 98, 99,
    99.9, 99.99, 100
]

COLUMNS = [
    "Sum",
    "Avg",
    "StdDev",
    "CoV"
]

def read_input(filename, timeline, VOIs, BDs, top=[], progress=False):
    if progress:
        print "Reading '%s':" % filename
    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.progress = progress

    # process all rows in the input file
    for record in reader:
        try:
            values_variables = []
            values_breakdowns = []

            submit_time_string = record[SUBMIT_TIME]
            wait_time_string = record[WAIT_TIME]

            try:
                submit_time = float(submit_time_string)
            except ValueError as e:
                if (submit_time_string.strip() != ""):
                    logging.exception(e)
                continue

            try:
                wait_time = float(wait_time_string)
            except ValueError as e:
                if (wait_time_string.strip() != ""):
                    logging.exception(e)
                wait_time = 0

            # all values are stored on the launch time
            launch_time = submit_time + wait_time

            # get values for all variables of interest
            for variable in VOIs:
                try:
                    raw = record[variable]
                    if len(raw) == 0 or raw == "None":
                        value = None
                    else:
                        value = float(raw)
                except Exception as e:
                    #logging.exception(e)
                    value = None
                values_variables.append(value)

            # get values for all breakdowns
            for breakdown in BDs:
                try:
                    raw = record[breakdown]
                    if len(raw) == 0 or raw == "None":
                        value = None
                    else:
                        value = int(float(raw))

                except KeyError as e:
                    pass
                except Exception as e:
                    logging.exception(e)
                    value = None

                values_breakdowns.append(value)

            # insert all values in the timeline for "all-over" and all breakdowns
            for variable_index, variable_name in enumerate(VOIs):
                variable_value = values_variables[variable_index]
                if variable_value is None: continue

                # insert for "all-over"
                timeline.add(launch_time, variable_value,
                    (variable_name, None, None)
                )

                # insert for all breakdowns
                for breakdown_index, breakdown_name in enumerate(BDs):
                    breakdown_value = values_breakdowns[breakdown_index]
                    if breakdown_value is None: continue

                    timeline.add(launch_time, variable_value,
                        (variable_name, breakdown_name, breakdown_value)
                    )
        except Exception as e:
            logging.exception(e)

def write_breakdown_output(outputfile, timeline, binwidth, variable, breakdown, values, top_n, progress=False):
    if len(timeline._keys) == 0: return

    keys = [(variable, breakdown, value) for value in values]
    top = find_top(timeline, binwidth, variable, breakdown, values)
    
    
    if (variable,breakdown,-1) in top:
        minusone = True
        top.remove((variable,breakdown,-1))
    else:
        minusone = False
    
    if len(top) <= top_n:
        others = []
    else:
        others = top[top_n:]
        top    = top[:top_n]
        
    if minusone:
        others.append((variable,breakdown,-1))
            
    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Time"]
    writer.column_titles.extend(generate_breakdown_column_headers(top))
    writer.header(__name__, __version__, __author__)

    writer.write(generate_breakdown_output(timeline.timeline(keys, binwidth), top, others))

def find_top(timeline, binwidth, variable, breakdown, values):
    keys = [(variable, breakdown, value) for value in values]
    sums = {}
    for key in keys:
        sums[key]=0.0
    
    for _, values in timeline.timeline(keys, binwidth * 10):
        for key in keys:
            if key in values:
                value = values[key]
                value._update_stats()
                sums[key] += value.Sum
                
    top = [x[0] for x in sorted(sums.iteritems(), key=operator.itemgetter(1))]
    top.reverse()
    
    return top

def generate_breakdown_column_headers(top):
    headers = ["%s %s" % (b, x) for (_, b, x) in top]
    return headers + ["Others"]
    
def write_output(outputfile, timeline, binwidth, variables, breakdowns, progress=False):
    if len(timeline._keys) == 0: return

    keys = []
    for variable_name in variables:
        if breakdowns == {}:
            keys = [(variable_name, None, None)]
        else:
            for breakdown_name, breakdown_values in breakdowns.items():
                for breakdown_value in breakdown_values:
                    keys.extend([(variable_name, breakdown_name, breakdown_value)])

    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Time"]
    writer.column_titles.extend(generate_column_headers(variables, breakdowns))
    writer.header(__name__, __version__, __author__)

    if binwidth is None:
        writer.write(generate_output(one_item_yielder(("all", {keys[0]:timeline.all_time(keys[0])})), variables, breakdowns))
    else:
        writer.write(generate_output(timeline.timeline(keys, binwidth), variables, breakdowns))
    
def one_item_yielder(item):
    yield item

def generate_output(timeline_iterator, variables, breakdowns):
    for time, values in timeline_iterator:
        output = [time]

        for variable_name in variables:
            if breakdowns == {}:
                key = (variable_name, None, None)
                if key in values and values[key] is not None:
                    value = values[key]
                    value._update_stats()
                    output.append(value.Sum)
                    output.append(value.Avg)
                    output.append(value.StdDev)
                    output.append(value.COV)
                    output.extend(value.calculate_percentiles(DEFAULT_PERCENTILES))
                else:
                    output.extend([0]* len(COLUMNS))
                    output.extend([None] * (len(DEFAULT_PERCENTILES)))
            else:
                for breakdown_name, breakdown_values in breakdowns.items():
                    for breakdown_value in breakdown_values:
                        key = (variable_name, breakdown_name, breakdown_value)

                        if key in values and values[key] is not None:
                            value = values[key]
                            value._update_stats()
                            output.append(value.Sum)
                            output.append(value.Avg)
                            output.append(value.StdDev)
                            output.append(value.COV)
                            output.extend(value.calculate_percentiles(DEFAULT_PERCENTILES))
                        else:
                            output.extend([0]* len(COLUMNS))
                            output.extend([None] * (len(DEFAULT_PERCENTILES)))
        yield output

def generate_breakdown_output(timeline_iterator, top, others):
    for time, values in timeline_iterator:
        output = [time]

        for t in top:
            if t in values:
                value = values[t]
                value._update_stats()
                output.append(value.Sum)
            else:
                output.append(0.0)
        
        others_sum = 0.0
        for t in others:
            if t in values:
                value = values[t]
                value._update_stats()
                others_sum += value.Sum
                
        output.append(others_sum)                
        yield output

def generate_variables_and_breakdowns(timeline, VOIs, BDs, top):
    variables = VOIs
    breakdowns = {}
    for bd in BDs:
        breakdowns[bd]=[]

    for (variable_name, breakdown_name, breakdown_value) in timeline._keys:
        if variable_name not in variables:
            variables.append(variable_name)

        if breakdown_name is not None and breakdown_name not in breakdowns:
            breakdowns[breakdown_name] = []

        if breakdown_value is not None and breakdown_value not in breakdowns[breakdown_name]:
            bisect.insort(breakdowns[breakdown_name], breakdown_value)

    return variables, breakdowns


def generate_column_headers(variables, breakdowns):
#    headers = []
    properties = COLUMNS[:]
    properties.extend(["%f-Pct" % pct for pct in DEFAULT_PERCENTILES])
    return properties

#    for variable_name in variables:
#        if breakdowns == {}:
#            for property_name in properties:
#                headers.append(property_name)
#        else:
#            for breakdown_name, breakdown_values in breakdowns.items():
#                for breakdown_value in breakdown_values:
#                    for property_name in properties:
#                        headers.append(property_name)
#    return headers

def write_pdf_cdf_output(outputfile, timeline, variable, breakdown = None, key = None, progress=False):
    if len(timeline._keys) == 0: return

    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Value","PDF", "CDF"]
    writer.header(__name__, __version__, __author__)

    stats = timeline.all_time((variable,breakdown,key))
    if stats is not None:
        writer.write(stats.calculate_pdf_cdf())
        
    writer.close()

def usage():
    print """use --help for usage"""

def run(argv):
    try:
        opts, argv = getopt.getopt(
            argv,
            "p",
            ["progress", "variable=", "breakdown=", "top="]
        )
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    top = []
    VOIs = []
    BDs = []

    for o, a in opts:
        if o in ("-p", "--progress"):
            progress = True
        elif o in ("--variable"):
            VOIs.append(a)
        elif o in ("--breakdown"):
            BDs.append(a)
        elif o in ("--top"):
            top += map(str.strip, a.split(","))

    if len(argv) < 2:
        usage()
        sys.exit(2)

    process_data(argv[:-1], VOIs, BDs, argv[-1], top, progress)

def process_data(
    input_files,
    VOIs,
    BDs,
    output_basename,
    top,
    progress=False,
    binwidths=[600, 3600, 86400]
):
    timeline = TimeLine(600)
    timeline.use_cstats = True
    timeline.keep_values = True
    timeline.keep_all_time = True

    for filename in input_files:
        # read data is stored in the timeline object
        read_input(filename, timeline, VOIs, BDs, top, progress)

    variables, breakdowns = generate_variables_and_breakdowns(timeline, VOIs, BDs, top)
    name_parts = output_basename.split(".")

    output_binwidths = binwidths[:]
    output_binwidths.sort()
    output_binwidths.insert(0, None)

    if progress:
        filecount = 0
        for key, values in breakdowns.items():
            filecount += (2 + len(values)) * len(variables)


    for binwidth in output_binwidths:
        if progress:
            if binwidth is None:
                print "Writing output for all time."
            else:
                print "Writing output for binwidth: %d" % binwidth
            bar = ProgressBar(filecount, 1)

        for variable in variables:
            filename = ".".join(
                name_parts[:-2]
                + [ "%s_%s_%s" %
                    (name_parts[-2],
                     variable,
                     str(binwidth) if binwidth is not None else "all_time"),
                    name_parts[-1]
                  ]
            )
            if binwidth is None:
                filename2 = ".".join(
                    name_parts[:-2]
                    + [ "%s_%s_%s" %
                        (name_parts[-2],
                         variable,
                         "pdf_cdf"),
                        name_parts[-1]
                      ]
                )
            logging.info("Writing output to: \"%s\"" % filename)
            try:
                if binwidth is None:
                    write_output(filename, timeline, binwidth, [variable], {}, False)
                    write_pdf_cdf_output(filename2, timeline, variable, None, None, False)
                else:
                    write_output(filename, timeline, binwidth, [variable], {}, False)
                
            except Exception as e:
                logging.exception(e)

            if progress: bar.increment()

            for key, values in breakdowns.items():
                # First, generate file with top-n breakdown items.
                if binwidth is not None:
                    filename = ".".join(
                        name_parts[:-2]
                        + [ "%s_%s_per_%s_%s" %
                            (name_parts[-2],
                             variable,
                             key,
                             str(binwidth)),
                            name_parts[-1]
                          ]
                    )
                    logging.info("Writing output to: \"%s\"" % filename)
                    try:
                        write_breakdown_output(filename, timeline, binwidth, variable, key, values, 4, False)
                    except Exception as e:
                        logging.exception(e)

                    if progress: bar.increment()
                
                # Then, generate detailed stats for each breakdown item.
                for value in values:
                    filename = ".".join(
                        name_parts[:-2]
                        + [ "%s_%s_per_%s_[%s]_%s" %
                            (name_parts[-2],
                             variable,
                             key,
                             value,
                             str(binwidth) if binwidth is not None else "all_time"),
                            name_parts[-1]
                          ]
                    )
                    if binwidth is None:
                        filename2 = ".".join(
                            name_parts[:-2]
                            + [ "%s_%s_per_%s_[%s]_%s" %
                                (name_parts[-2],
                                 variable,
                                 key,
                                 value,
                                 "pdf_cdf"),
                                name_parts[-1]
                              ]
                        )
                    logging.info("Writing output to: \"%s\"" % filename)
                    try:
                        if binwidth is None:
                            write_output(filename, timeline, binwidth, [variable], {key:[value]}, False)
                            write_pdf_cdf_output(filename2, timeline, variable, key, value, False)
                        else:
                            write_output(filename, timeline, binwidth, [variable], {key:[value]}, False)
                    except Exception as e:
                        logging.exception(e)

                    if progress: bar.increment()
        if progress: bar.done()

if __name__ == "__main__":
    run(sys.argv[1:])
