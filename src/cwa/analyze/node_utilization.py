#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Calculate node utilization.

Takes some files as input and outputs to a file.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "12.1.1"

from utils.time_line import TimeLine
from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
import getopt
import logging
import sys

SUBMIT_TIME = "SubmitTime"
WAIT_TIME = "WaitTime"
RUN_TIME = "RunTime"


def read_input(filename, timelines, breakdown, progress=False):
    if progress:
        print "Reading '%s':" % filename
    base_time = None

    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.progress = progress

    for record in reader:
        try:
            submit_time = float(record[SUBMIT_TIME])
            if base_time is None:
                base_time = submit_time
            else:
                base_time = min(base_time, submit_time)
        except:
            pass

    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.progress = progress

    error_count = 0

    for record in reader:
        try:
            try:
                index = record['MR_task_attempt_host']
            except Exception as e:
                if error_count < 1000:
                    error_count += 1
                    logging.exception(e)
                index = None

            submit_time = float(record[SUBMIT_TIME])
            run_time = float(record[RUN_TIME])

            if base_time is None:
                base_time = submit_time

            submit_time -= base_time

            try:
                wait_time = float(record[WAIT_TIME])
            except ValueError:
                wait_time = 0

            start_time = submit_time + wait_time
            end_time   = start_time  + run_time

            for binwidth, timeline in timelines.items():
                for time in xrange(int(start_time), int(end_time), binwidth):
                    timeline.add(
                        time,
                        1,
                        index
                    )
        except ValueError as e:
            if record[SUBMIT_TIME] != "" and record[RUN_TIME] != "":
                if error_count < 1000:
                    error_count += 1
                    logging.exception(e)
        except KeyError as e:
            if error_count < 1000:
                error_count += 1
                logging.exception(e)

def write_output(outputfile, timeline, binwidth, progress=False):
    writer = TRWriter(outputfile)
    writer.progress = progress
    #writer.column_titles = ["Time"] + generate_column_headers(timeline)
    writer.column_titles = ["Time", "HostCount"]
    writer.header(__name__, __version__, __author__)
    writer.write(generate_output(timeline.timeline(binwidth=binwidth),sorted(timeline._keys,key=int)))

def generate_output(timeline_iterator, keys):
    time = None

    for time, values in timeline_iterator:
        count = 0

        for key in keys:
            try:
                value = values[key]
                if value > 0:
                    result = value
                else:
                    result = 0
            except:
                result =0

            if result > 0:
                count += 1

        yield [time, count]

    if time is not None:
        yield [time+3600, 0]

def generate_column_headers(timeline):
    return ["%s" % id for id in sorted(timeline._keys, key=int)]

def usage():
    print """use --help for usage"""

def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "p", ["progress"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False

    for o, unused_a in opts:
        if o in ("-p", "--progress"):
            progress = True

    if len(argv) < 2:
        usage()
        sys.exit(2)

    name_parts = argv[-1].split(".")

    timelines = {}
    for binwidth in [60 * 60]:
        timelines[binwidth] = TimeLine(binwidth)

    for filename in argv[:-1]:
        read_input(filename, timelines, progress)

    for binwidth in [60 * 60]:
        filename = ".".join(name_parts[:-2] + [ name_parts[-2] + "_" + str(binwidth), name_parts[-1]])
        if progress:
            print "Writing '%s':" % filename
        write_output(filename, timelines[binwidth], binwidth, progress)

if __name__ == "__main__":
    run(sys.argv[1:])
