#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import os
import sys
import logging
import main
import node_utilization
import jobs_per_executableid
import io_per_executableid
import cio_per_executableid
import statistics
import top

def run(argv):
    analyzer = Analyzer()
    analyzer.analyze()

def mkdir(path):
    if os.path.isdir(path):
        pass
    elif os.path.isfile(path):
        raise OSError("a file with desired dir name exists: %s" % path)
    else:
        head, tail = os.path.split(path)

        if head:
            mkdir(head)

        if tail:
            os.mkdir(path)

class Analyzer:
    ARGS = ["-p"]

    JOB_METRICS = [
        "WaitTime",
        "RunTime",
        "CPUs",
        "TotalWallClockTime",
        "Memory",
        "Network",
        "Disk",
        "DiskIORatio",
        "MR_total_launched",
#        "MR_total_from_job",
#        "MR_total_failed",
#        "MR_total_killed",
#        "MR_total_splits",
        "MR_total_hdfs_read",
        "MR_total_hdfs_written",
#        "MR_total_local_read",
#        "MR_total_local_written",
#        "MR_total_spilled_records",
        "MR_map_launched",
#        "MR_map_finished",
#        "MR_map_failed",
#        "MR_map_killed",
#        "MR_map_hdfs_read",
#        "MR_map_hdfs_written",
#        "MR_map_local_read",
#        "MR_map_local_written",


        "MR_map_input",
        "MR_map_output",
        "MR_total_map_time",
        "MR_total_reduce_time",
    ]

    JOB_BREAKDOWNS = [
        "Status",
        #"ExecutableID",
        #"UserID",
        #"GroupID",
        "QueueID",
        #"PartitionID",
    ]

    TASK_METRICS = [
        "WaitTime",
        "RunTime",
        "CPUs",
        "TotalWallClockTime",
        "Memory",
        "Network",
        "Disk",
        "DiskIORatio",
        "MR_total_hdfs_read",
        "MR_total_hdfs_written",
#        "MR_total_local_read",
#        "MR_total_local_written",
#        "MR_total_spilled_records",
        "MR_map_input",
        "MR_map_output",
        "MR_map_input_records",
        "MR_map_output_records",
        "MR_spilled_records"
    ]

    TASK_BREAKDOWNS = [
        "Status",
        #"ExecutableID",
        #"QueueID",
        #"PartitionID",
        "MR_task_type",
    ]

    def __init__(self):
        self.config = main.get_config()
        if self.config is None:
            logging.error("unable to load config file (usually '.cta/config')")
            sys.exit(1)

    def analyze(self):
        over_time_path = self.config.get("analyze", "output")
        if not os.path.exists(over_time_path): mkdir(over_time_path)

        self.generate_Utilization()
        self.generate_Jobs()
        self.generate_job_statistics()
        self.generate_Tasks()
        self.generate_task_statistics()

    def generate_job_statistics(self):
        logging.info("Analyzing job statistics...")
        target = os.path.join(self.config.get("analyze", "output"), "cumulative_job_statistics.dat")
        if os.path.exists(target+".done"): return

        metrics    = [x.strip() for x in self.config.get("analyze", "job_metrics").split()]
        breakdowns = [x.strip() for x in self.config.get("analyze", "job_breakdowns").split()]

        arguments = []
        arguments.extend(Analyzer.ARGS)

        for variable in metrics:
            arguments.append("--variable=%s" % variable)

        for breakdown in breakdowns:
            arguments.append("--breakdown=%s" % breakdown)

        arguments.append("%s.cwjd" % self.config.get("import", "output"))
        arguments.append(target)

        try:
            statistics.run(arguments)
            open(target+".done","w").close()
        except Exception as e:
            logging.exception(e)

    def generate_task_statistics(self):
        logging.info("Analyzing job statistics...")
        target = os.path.join(self.config.get("analyze", "output"), "cumulative_task_statistics.dat")
        if os.path.exists(target+".done"): return

        metrics    = [x.strip() for x in self.config.get("analyze", "task_metrics").split()]
        breakdowns = [x.strip() for x in self.config.get("analyze", "task_breakdowns").split()]

        arguments = []
        arguments.extend(Analyzer.ARGS)

        for variable in metrics:
            arguments.append("--variable=%s" % variable)

        for breakdown in breakdowns:
            arguments.append("--breakdown=%s" % breakdown)

        arguments.append("%s.cwtd" % self.config.get("import", "output"))
        arguments.append(target)

        try:
            statistics.run(arguments)
            open(target+".done","w").close()
        except Exception as e:
            logging.exception(e)

    def generate_Utilization(self):
        try:
            logging.info("Analyzing node utilization")
            target = os.path.join(self.config.get("analyze", "output"), "node_utilization.dat")
            if os.path.exists(target+".done"): return
            node_utilization.run(Analyzer.ARGS + [
                "%s.cwtd" % self.config.get("import", "output"),
                target
            ])
            open(target+".done","w").close()
        except Exception as e:
            logging.exception(e)

    def generate_Jobs(self):
        breakdowns = ['None']
        breakdowns.extend([x.strip() for x in self.config.get("analyze", "job_breakdowns").split()])

        for breakdown in breakdowns:
            try:
                logging.info("Analyzing jobs (Breakdown: %s)" % breakdown)
                target = os.path.join(self.config.get("analyze", "output"), "cumulative_jobs%s.dat" % ("" if breakdown == 'None' else "_per_%s" % (breakdown)))
                if os.path.exists(target+".done"): return
                jobs_per_executableid.run(Analyzer.ARGS + [
                    "--breakdown=%s" % breakdown,
                    "%s.cwj" % (self.config.get("import", "output")),
                    target
                ])
                open(target+".done","w").close()
            except Exception as e:
                logging.exception(e)

    def generate_Tasks(self):
        breakdowns = ['None']
        breakdowns.extend([x.strip() for x in self.config.get("analyze", "task_breakdowns").split()])

        for breakdown in breakdowns:
            try:
                logging.info("Analyzing tasks")
                target = os.path.join(self.config.get("analyze", "output"), "cumulative_tasks%s.dat" % ("" if breakdown == 'None' else "_per_%s" % (breakdown)))
                if os.path.exists(target+".done"): return
                jobs_per_executableid.run(Analyzer.ARGS + [
                    "--breakdown=%s" % breakdown,
                    "%s.cwtd" % self.config.get("import", "output"),
                    target
                ])
                open(target+".done","w").close()
            except Exception as e:
                logging.exception(e)
