#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Count Distinct Field Value Occurrences

Takes some files as input and outputs to a file. Counts the number of
occurrences of distinct values for a field in the input in between.

Options:
    -h, --help            display this text
        --doc             display pydoc for this module
    -p, --progress        show progress bar during processing
    -t, --time            measure the total processing time
    -f, --field=FIELD     field to count
        --to-lowercase    change all text to lowercase
        --ignore-numbers  changes each number into a "#"
        --ignore-days     change day and month names into "@@@"

Mandatory arguments to long options are mandatory for short options too.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.2"

import sys
import getopt
from utils.tr_reader    import TRReader
from utils.tr_writer    import TRWriter
from utils.progress_bar import ProgressBar
from datetime           import datetime
import math
import re

def read_input(filename, field = 0, field_value_count = {},
               progress = False, to_lowercase = False,
               ignore_numbers = False, ignore_days = False):
    if progress:
        print "Reading '%s':" % filename

    reader = TRReader(filename)
    reader.header = False
    reader.progress = progress

    for record in reader:
        if type(record) is dict:
            if field in record:
                field_value = record[field]
            elif int(field) in record:
                field_value = record[int(field)]
        else:
            field_value = record[int(field)]

        if to_lowercase:
            field_value = field_value.lower()

        if ignore_numbers:
            field_value = re.sub('[0-9]', '#', field_value)

        ignore_months = False
        if ignore_days:
            ignore_months = True
            field_value = re.sub("""(?ix)
                (?P<left>\W|_)
                (
                    mon(day)?
                    | tue(sday)?
                    | wed(nesday)?
                    | thu(rsday)?
                    | fri(day)?
                    | sat(urday)?
                    | sun(day)?
                )
                (?P<right>\W|_)
            """,
            '\\g<left>@@@\\g<right>',
            field_value)

        if ignore_months:
            field_value = re.sub("""(?ix)
                (?P<left>\W|_)
                (
                    jan(uary)?
                    | feb(ruary)?
                    | mar(ch)?
                    | apr(il)?
                    | may
                    | jun(e)?
                    | jul(y)?
                    | aug(ust)?
                    | sep(tember)?
                    | oct(ober)?
                    | nov(ember)?
                    | dec(ember)?
                )
                (?P<right>\W|_)
            """,
            '\\g<left>@@@\\g<right>',
            field_value)

        if field_value in field_value_count:
            field_value_count[field_value] += 1
        else:
            field_value_count[field_value] = 1

    return field_value_count

def write_output(outputfile, field_value_count, progress = False):
    if progress:
        print "Sorting:"

        n = len(field_value_count)
        bar = ProgressBar(n * math.log(n, 2))
        bar.i = 0

    def compare(x, y):
        if progress:
            bar.i += 1
            if bar.i % 100000 == 0:
                bar.update(bar.i)
        return cmp(x, y)

    s = sorted(
        field_value_count.items(),
        lambda (a, x), (b, y): compare(x, y),
        reverse = True
    )

    if progress:
        bar.done()
        print "Writing '%s':" % outputfile

    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Value", "Count"]
    writer.header(__name__, __version__, __author__)
    writer.write(s)

def usage():
    print """usage: %s [options] <inputfile>... <outputfile>
""" % " ".join(__name__.split("."))
    print __doc__

def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "hptf:",
        ["help", "doc", "progress", "time", "field=", "to-lowercase",
        "ignore-numbers", "ignore-days"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    time = False
    field = 0
    to_lowercase = False
    ignore_numbers = False
    ignore_days = False

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("--doc"):
            help(sys.modules[__name__])
            sys.exit()
        elif o in ("-p", "--progress"):
            progress = True
        elif o in ("-t", "--time"):
            time = True
        elif o in ("-f", "--field"):
            field = a
        elif o in ("--to-lowercase"):
            to_lowercase = True
        elif o in ("--ignore-numbers"):
            ignore_numbers = True
        elif o in ("--ignore-days"):
            ignore_days = True

    if len(argv) < 2:
        usage()
        sys.exit(2)

    if time:
        start = datetime.now()
    counts = {}

    for filename in argv[:-1]:
        counts = read_input(filename, field, counts, progress,
                            to_lowercase, ignore_numbers, ignore_days)
    write_output(argv[-1], counts, progress)

    if time:
        stop = datetime.now()
        print stop - start

def main():
    run(sys.argv[1:])


if __name__ == "__main__":
    main()
