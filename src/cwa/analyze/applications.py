#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Match Rules on a Field

The rules expressed as regular expressions in the rules file are matched
against the first field in the input file. The second field in the input
file is taken as a weight for the field, for each rule the weight of the
matched fields is summed.

The first non-empty/non-comment row in the rules/input file is assumed
to contain column headers and is ignored.

Options:
    -h, --help             display this text
        --doc              display pydoc for this module
    -p, --progress         show progress bar during processing
    -t, --time             measure the total processing time
        --output-rule      use rule in output instead of rule number
    -r, --rules=RULESFILE  name of file containing the rules to match

Mandatory arguments to long options are mandatory for short options too.

Example Rules File:
    Regular Expression
    ^select\s.*$
    ^insert\s.*$
    ^update\s.*$

Example Input File:
    Command    Count
    select * from foo;    8
    select * from bar;    2
    insert into baz...    1
    
Example Output File:
    Rule    Count
    ^select\s.*$    10
    ^insert\s.*$    1
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.9"

from datetime import datetime
from utils.progress_bar import ProgressBar
from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
import getopt
import math
import re
import sys

output_unmatched = False # FIXME!

def compile_patterns(filename):
    reader = TRReader(filename)
    reader.header = True
    reader.multi_line = True

    return [
        (rule[0], re.compile(rule[0]))
        for rule in reader
    ]

def match(patterns, string):
    for index, (unused_rule, pattern) in enumerate(patterns):
        if pattern.match(string):
            return index
    return None

def read_input(filename, patterns, counts={}, output_rule=False,
               progress=False):
    if progress:
        print "Reading '%s':" % filename
    reader = TRReader(filename)
    reader.header = True
    reader.progress = progress

    unmatched = []

    for [appname, count] in reader:
        index = match(patterns, appname)

        if index is None:
            unmatched.append([appname, count])

        if output_rule:
            if index is None:
                rule = None
            else:
                (rule, unused_pattern) = patterns[index]
        else:
            if index is None:
                rule = None
            else:
                rule = index

        if rule in counts:
            counts[rule] += int(count)
        else:
            counts[rule] = int(count)

    if output_unmatched:
        if progress:
            print "Writing 'unmatched.txt':"
        writer = TRWriter("unmatched.txt")
        writer.progress = progress
        writer.column_titles = ["App", "Count"]
        writer.header(__name__, __version__, __author__)
        writer.write(unmatched)

    return counts

def write_output(outputfile, counts, progress=False):
    if progress:
        print "Sorting:"
        n = len(counts)
        if n > 1:
            upper_limit = n * math.log(n, 2)
        else:
            upper_limit = 1
        bar = ProgressBar(upper_limit)
        bar.i = 0

    def compare(x, y):
        if progress:
            bar.i += 1
            if bar.i % 100000 == 0:
                bar.update(bar.i)
        return cmp(x, y)

    unmatched = counts.pop(None)

    s = sorted(
        counts.items(),
        lambda (a, x), (b, y): compare(x, y),
        reverse=True
    )


    if progress:
        bar.done()
        print "Writing '%s':" % outputfile

    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Rule", "Matches"]
    writer.header(__name__, __version__, __author__)
    writer.write(s + [("Other", unmatched)])

def usage():
    print """usage: %s [options]
           --rules=<rulesfile>
           <inputfile>... <outputfile>
    """ % " ".join(__name__.split("."))
    print __doc__

def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "hptr:",
        ["help", "doc", "progress", "time", "rules=", "output-rule"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    time = False
    rules_filename = ""
    output_rule = False

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("--doc"):
            help(sys.modules[__name__])
            sys.exit()
        elif o in ("-p", "--progress"):
            progress = True
        elif o in ("-t", "--time"):
            time = True
        elif o in ("-r", "--rules"):
            rules_filename = a
        elif o in ("--output-rule"):
            output_rule = True

    if len(argv) < 2 or len(rules_filename) == 0:
        usage()
        sys.exit(2)

    patterns = compile_patterns(rules_filename)
    counts = {}

    if time:
        start = datetime.now()

    for filename in argv[:-1]:
        counts = read_input(filename, patterns, counts, output_rule,
                            progress)
    write_output(argv[-1], counts, progress)

    if time:
        stop = datetime.now()
        print stop - start

def main():
    run(sys.argv[1:])


if __name__ == "__main__":
    main()
