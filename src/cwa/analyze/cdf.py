#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Calculate

Options:
    -h, --help            display this text
        --doc             display pydoc for this module
    -p, --progress        show progress bar during processing
    -t, --time            measure the total processing time

Mandatory arguments to long options are mandatory for short options too.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.9"

import sys
import getopt
from utils.tr_reader    import TRReader
from utils.tr_writer    import TRWriter
from datetime           import datetime

def read_input(filename, progress=False):
    # expecting: id value
    # output: id normalized_value cummulative_value
    if progress:
        print "Reading '%s':" % filename

    reader = TRReader(filename)
    reader.header = True
    reader.progress = progress

    values = {}
    for record in reader:
        if record[0] in values:
            values[record[0]] += int(record[1])
        else:
            values[record[0]] = int(record[1])

    return values

def analyze(values, progress=False):
    #max_value = float(max(values.values()))
    sum_value = float(sum(values.values()))

    unmatched = values.pop("Other")

    as_list = [
        [id, float(value) / sum_value, float(value) / sum_value]
        for (id, value) in values.items()
    ]

    sorted_by_norm = sorted(
        as_list,
        lambda x, y: cmp(x[1], y[1]),
        reverse=True
    )

    cummulative = 0
    rank = 0

    as_cummulative_list = [[0, "\"\"", 0, 0]]
    for i in sorted_by_norm + [["Other", float(unmatched) / sum_value, float(unmatched) / sum_value]]:
        rank += 1
        cummulative += i[2]
        as_cummulative_list.append(
            [rank]
            + ["\"" + str(i[0])
                        .replace("\\", "\\\\")
                        .replace("\"", "\\\"")
                + "\""]
            + [i[1]]
            + [cummulative]
        )
    as_cummulative_list.append([rank + 1, "\"\"", 0, 1])

    return as_cummulative_list


def write_output(filename, values, progress=False):
    if progress:
        print "Writing '%s':" % filename

    writer = TRWriter(filename)
    writer.progress = progress
    writer.column_titles = ["Rank", "Id", "Norm", "Port"]
    writer.header(__name__, __version__, __author__)
    writer.write(values)

def usage():
    print """usage: %s [options] <inputfile>... <outputfile>
""" % " ".join(__name__.split("."))
    print __doc__

def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "hpt",
        ["help", "doc", "progress", "time"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    time = False

    for o, unused_a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("--doc"):
            help(sys.modules[__name__])
            sys.exit()
        elif o in ("-p", "--progress"):
            progress = True
        elif o in ("-t", "--time"):
            time = True

    if len(argv) < 2:
        usage()
        sys.exit(2)

    if time:
        start = datetime.now()

    values = read_input(argv[0], progress)
    values = analyze(values, progress)
    write_output(argv[-1], values, progress)

    if time:
        stop = datetime.now()
        print stop - start

def main():
    run(sys.argv[1:])


if __name__ == "__main__":
    main()
