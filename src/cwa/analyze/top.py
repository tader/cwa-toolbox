#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Calculate the top-n for some breakdown.

usage: [options] <input file> <output file>

OPTIONS:
  -n, --number=N    calculate the top N (incl. "Other") (default 5)
  -k, --keep=FIELD  leave FIELD untouched
  -1, --one-pass    do everything in a single pass
                    (keep all data in RAM)
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.31"

from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
import getopt
import sys
import logging


def run(argv):
    try:
        opts, argv = getopt.getopt(
            argv,
            "pn:k:1",
            ["progress", "number=", "keep=", "one-pass"]
        )
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(2)

    progress = False
    keep = ["__line__"]
    number = 5
    one_pass = False

    for o, a in opts:
        if o in ("-p", "--progress"):
            progress = True
        elif o in ("-k", "--keep"):
            keep.append(a)
        elif o in ("-n", "--number"):
            number = int(a)
        elif o in ("-1", "--one-pass"):
            one_pass = True

    if len(argv) != 2:
        sys.exit(2)

    process_data(argv[0], argv[-1], number, keep, one_pass, progress)

def process_data(
        input_file,
        output_file,
        number,
        keep,
        one_pass,
        progress=False):
    sums = {}
    titles = []

    if progress:
        print "Reading '%s':" % input_file
    reader = TRReader(input_file)
    reader.return_as_dict = True
    reader.progress = progress

    for record in reader:
        if len(titles) == 0:
            titles = record.keys()

        for (field, value) in record.items():
            if field in keep:
                continue

            try:
                value = float(value)
            except ValueError as e:
                logging.exception(e)
                value = 0.0

            if not field in sums:
                sums[field] = 0.0

            sums[field] += value

    top_keys = [key for (key, sum) in
                    sorted(
                        sums.items(),
                        lambda (a, x), (b, y): cmp(x, y),
                        reverse=True
                    )[:number - 1]
               ]


    if progress:
        print "Reading '%s':" % input_file
    reader = TRReader(input_file)
    reader.return_as_dict = True
    reader.progress = progress

    def generate_output():
        others = filter(
                     lambda x: x not in keep and x not in top_keys,
                     titles
                 )

        for record in reader:
            other = 0
            result = []

            for key in others:
                try:
                    value = float(record[key])
                except ValueError as e:
                    logging.exception(e)
                    value = 0.0
                other += value

            for key in titles:
                if key == "__line__":
                    continue

                if key in keep:
                    result.append(record[key])

            for key in top_keys:
                result.append(record[key])

            result.append(other)
            yield result

    writer = TRWriter(output_file)
    writer.progress = progress
    writer.column_titles = [title for title in
                            filter(lambda x: x in keep[1:], titles)
                           ] + top_keys + ["Other"]
    writer.header(__name__, __version__, __author__)
    writer.write(generate_output())


if __name__ == "__main__":
    run(sys.argv[1:])
