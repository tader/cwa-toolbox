#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Count IO

Takes some files as input and outputs to a file.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.3"

import sys
import getopt
from utils.tr_reader    import TRReader
from utils.tr_writer    import TRWriter
from utils.time_line    import TimeLine
from datetime           import datetime

def read_input(filename, timeline, progress = False):
    if progress:
        print "Reading '%s':" % filename
    reader = TRReader(filename)
    #reader.max_columns = 9
    reader.progress = progress

    for record in reader:
        timeline.add(
            int(record[1]) + int(record[2]), # submittime + waittime
            int(record[8])                   # diskio
        )

    return timeline

def write_output(outputfile, timeline, binwidth, progress = False):
    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Time", "Bytes"]
    writer.header(__name__, __version__, __author__)
    writer.write(timeline.timeline(binwidth))

def usage():
    print """usage: %s [-h|--help] [--doc] <inputfile> ...

Count the number of occurrences of distinct values for a field in the input.

Options:
    -h, --help:     display this text
         --doc:     display pydoc for this module
""" % " ".join(__name__.split("."))
def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "hpt:",
        ["help", "doc", "progress", "time"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    time = False

    for o, unused_a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("--doc"):
            help(sys.modules[__name__])
            sys.exit()
        elif o in ("-p", "--progress"):
            progress = True
        elif o in ("-t", "--time"):
            time = True

    if len(argv) < 2:
        usage()
        sys.exit(2)

    timeline = TimeLine(60 * 10) # 10 minutes bins

    if time:
        start = datetime.now()

    for filename in argv[:-1]:
        read_input(filename, timeline, progress)

    name_parts = argv[-1].split(".")

    # I guess serial I/O is faster than parallel I/O?
    for binwidth in [60 * 10, 60 * 60, 60 * 60 * 24]:
        filename = ".".join(name_parts[:-2] + [ name_parts[-2] + "_" + str(binwidth), name_parts[-1]])
        write_output(filename, timeline, binwidth, progress)

    if time:
        stop = datetime.now()
        print stop - start

def main():
    run(sys.argv[1:])


if __name__ == "__main__":
    main()
