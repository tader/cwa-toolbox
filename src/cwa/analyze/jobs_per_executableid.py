#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Count num jobs breakdown over executable id

Takes some files as input and outputs to a file.
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.16"

from utils.time_line import TimeLine
from utils.tr_reader import TRReader
from utils.tr_writer import TRWriter
import getopt
import logging
import sys
import main

SUBMIT_TIME = "SubmitTime"
WAIT_TIME = "WaitTime"
RUN_TIME = "RunTime"


def read_input(filename, timelines, breakdown, progress=False):
    if progress:
        print "Reading '%s':" % filename
    base_time = None

    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.progress = progress

    config = main.get_config()
    if config.get("analyze", "rebase_time") == "True":
        for record in reader:
            try:
                submit_time = float(record[SUBMIT_TIME])
                if base_time is None:
                    base_time = submit_time
                else:
                    base_time = min(base_time, submit_time)
            except:
                pass

    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.progress = progress

    for record in reader:
        try:
            try:
                index = int(float(record[breakdown]))
            except:
                index = -1

            submit_time = float(record[SUBMIT_TIME])
            run_time = float(record[RUN_TIME])

            if base_time is not None:
                submit_time -= base_time

            try:
                wait_time = float(record[WAIT_TIME])
            except ValueError:
                wait_time = 0

            start_time = submit_time + wait_time
            end_time   = start_time  + run_time

            for binwidth, timeline in timelines.items():
                for time in xrange(int(start_time), int(end_time), binwidth):
                    timeline.add(
                        time,
                        1,
                        index
                    )
        except ValueError as e:
            if record[SUBMIT_TIME] != "" and record[RUN_TIME] != "":
                logging.exception(e)
        except KeyError as e:
            logging.exception(e)

def write_output(outputfile, timeline, binwidth, breakdown = None, progress=False):
    writer = TRWriter(outputfile)
    writer.progress = progress
    writer.column_titles = ["Time"] + generate_column_headers(timeline, breakdown)
    writer.header(__name__, __version__, __author__)
    writer.write(generate_output(timeline, timeline.timeline(binwidth)))

def generate_output(timeline, timeline_iterator):
    keys = sorted(map(int, timeline._keys))
    if keys and keys[0] == -1: keys = keys[1:] + [-1]

    for time, values in timeline_iterator:
        yield [time] + [values[key] if key in values else 0 for key in keys]

def generate_column_headers(timeline, breakdown = None):
    prefix = "%s " % breakdown if breakdown is not None else "ID_"
    
    keys = sorted(map(int, timeline._keys))
    if keys and keys[0] == -1: keys = keys[1:] + [-1]

    return ["Others" if id == -1 else "%s%d" % (prefix, id)  for id in keys]

def usage():
    print """use --help for usage"""

def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "p", ["progress", "breakdown="])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    progress = False
    breakdown = "ExecutableID"

    for o, a in opts:
        if o in ("-p", "--progress"):
            progress = True
        elif o in ("--breakdown"):
            breakdown = a if a is not "None" else None

    if len(argv) < 2:
        usage()
        sys.exit(2)

    name_parts = argv[-1].split(".")

    timelines = {}
    for binwidth in [60 * 10, 60 * 60, 60 * 60 * 24]:
        timelines[binwidth] = TimeLine(binwidth)

    for filename in argv[:-1]:
        read_input(filename, timelines, breakdown, progress)

    for binwidth in [60 * 10, 60 * 60, 60 * 60 * 24]:
        filename = ".".join(name_parts[:-2] + [ name_parts[-2] + "_" + str(binwidth), name_parts[-1]])
        if progress:
            print "Writing '%s':" % filename
        write_output(filename, timelines[binwidth], binwidth, breakdown, progress)

if __name__ == "__main__":
    run(sys.argv[1:])
