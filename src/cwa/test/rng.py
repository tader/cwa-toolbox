#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import matplotlib.pyplot as plt

SIZE = 100000


def run(argv):
    scatter('scatter', SIZE)
    histogram('histogram', SIZE)


def scatter(filename, size):
    X = np.random.rand(size)
    Y = np.random.rand(size)
    plt.scatter(X, Y, s=0.1, lw=0, facecolor='0')
    for f in ['png', 'pdf', 'eps']:
        plt.savefig(
            "%s.%s" % (filename, f),
            format=f,
            dpi=300
        )


def histogram(filename, size):
    X = np.random.rand(size)
    plt.hist(X, bins=100)
    for f in ['png', 'pdf', 'eps']:
        plt.savefig(
            "%s.%s" % (filename, f),
            format=f,
            dpi=300
        )
