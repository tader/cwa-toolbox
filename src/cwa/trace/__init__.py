#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


CWJ_FIELDS = [
    "JobID",
    "SubmitTime",
    "WaitTime",
    "RunTime",
    "CPUs",
    "TotalWallClockTime",
    "Memory",
    "Network",
    "Disk",
    "Status",
    "UserID",
    "GroupID",
    "ExecutableID",
    "QueueID",
    "PartitionID",
    "JobProperties",
    "StructuralChanges",
    "StructuralChangeParams",
    "DiskIORatio"
]

CWT_FIELDS = [
    "JobID",
    "TaskID",
    "SubmitTime",
    "WaitTime",
    "RunTime",
    "CPUs",
    "TotalWallClockTime",
    "Memory",
    "Network",
    "Disk",
    "Status",
    "ExecutableID",
    "QueueID",
    "PartitionID",
    "StructuralChanges",
    "StructuralChangeParams",
    "DiskIORatio"
]

CWJD_FIELDS = [
    "JobID",
    "SubmitTime",
    "WaitTime",
    "RunTime",
    "CPUs",
    "TotalWallClockTime",
    "Memory",
    "Network",
    "Disk",
    "Status",
    "UserID",
    "GroupID",
    "ExecutableID",
    "QueueID",
    "PartitionID",
    "JobProperties",
    "StructuralChanges",
    "StructuralChangeParams",
    "DiskIORatio",
    "MR_total_launched",
    "MR_total_from_job",
    "MR_total_failed",
    "MR_total_killed",
    "MR_total_splits",
    "MR_total_hdfs_read",
    "MR_total_hdfs_written",
    "MR_total_local_read",
    "MR_total_local_written",
    "MR_total_spilled_records",
    "MR_map_launched",
    "MR_map_total",
    "MR_map_finished",
    "MR_map_failed",
    "MR_map_killed",
    "MR_map_hdfs_read",
    "MR_map_hdfs_written",
    "MR_map_local_read",
    "MR_map_local_written",
    "MR_map_input",
    "MR_map_output",
    "MR_map_input_records",
    "MR_map_output_records",
    "MR_data_local",
    "MR_data_rack",
    "MR_combine_input_records",
    "MR_combine_output_records",
    "MR_reduce_input_records",
    "MR_reduce_input_groups",
    "MR_reduce_output_records",
    "MR_reduce_finished",
    "MR_total_map_time",
    "MR_total_reduce_time",
]

CWTD_FIELDS = [
    "JobID",
    "TaskID",
    "SubmitTime",
    "WaitTime",
    "RunTime",
    "CPUs",
    "TotalWallClockTime",
    "Memory",
    "Network",
    "Disk",
    "Status",
    "ExecutableID",
    "QueueID",
    "PartitionID",
    "StructuralChanges",
    "StructuralChangeParams",
    "DiskIORatio",
    "MR_total_hdfs_read",
    "MR_total_hdfs_written",
    "MR_total_local_read",
    "MR_total_local_written",
    "MR_total_spilled_records",
    "MR_map_input",
    "MR_map_output",
    "MR_map_input_records",
    "MR_map_output_records",
    "MR_data_local",
    "MR_data_rack",
    "MR_combine_input_records",
    "MR_combine_output_records",
    "MR_reduce_input_records",
    "MR_reduce_input_groups",
    "MR_reduce_output_records",
    "MR_task_attempt_host",
    "MR_task_attempt_shuffle_finished",
    "MR_task_attempt_sort_finished",
    "MR_task_attempt_counters",
    "MR_task_attempt_id",
    "MR_task_type",
    "MR_spilled_records",
]

FIELDS = {
    'CWJ': CWJ_FIELDS,
    'CWT': CWT_FIELDS,
    'CWJD': CWJD_FIELDS,
    'CWTD': CWTD_FIELDS
}

def cwj_by_name(record, field):
    if isinstance(record, dict):        
        return record[field]
    else:    
        return _by_name(record, CWJ_FIELDS, field)

def cwjd_by_name(record, field):
    if isinstance(record, dict):        
        return record[field]
    else:    
        return _by_name(record, CWJD_FIELDS, field)

def cwt_by_name(record, field):
    if isinstance(record, dict):        
        return record[field]
    else:    
        return _by_name(record, CWT_FIELDS, field)

def cwtd_by_name(record, field):
    if isinstance(record, dict):        
        return record[field]
    else:    
        return _by_name(record, CWTD_FIELDS, field)
        
def _by_name(record, fields, field):
    idx = fields.index(field)
    try:
        return record[idx]
    except IndexError:
        return None
