#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Show Trace Records
"""

__author__ = "Thomas A. de Ruiter <thomas@de-ruiter.cx>"
__version__ = "11.3.3"

from utils.tr_reader import TRReader
import getopt
import sys

def read_input(filename):
    reader = TRReader(filename)
    reader.return_as_dict = True
    reader.header = True

    for record in reader:
        maxlen = 0
        for field in record:
            if field == "__line__":
                continue
            if len(field) > maxlen:
                maxlen = len(field)

        print "[record on row %d]" % record["__line__"]
        for field in record:
            if field == "__line__":
                continue
            print "%s%s: %s" % (" "*(4 + maxlen - len(field)), field, record[field])

        print
        if raw_input("[q]uit or continue? ") == "q":
            break

def usage():
    print """usage: %s [-h|--help] [-p|--progress]
           [-t|--time] [-f|--field=] <inputfile> <outputfile>

Count the number of occurrences of distinct values for a field in the input.

Options:
    -h, --help:     display this text
    -p, --progress: display progress bar during file operations
""" % " ".join(__name__.split("."))
def run(argv):
    try:
        opts, argv = getopt.getopt(argv, "hp",
        ["help", "doc"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    for o, unused_a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o in ("--doc"):
            help(sys.modules[__name__])
            sys.exit()

    if len(argv) < 1:
        usage()
        sys.exit(2)

    for filename in argv:
        read_input(filename)

def main():
    run(sys.argv[1:])


if __name__ == "__main__":
    main()
