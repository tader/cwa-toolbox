#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011
#     Alexandru Iosup <A.Iosup@tudelft.nl>,
#     Rean Griffith <rean@eecs.berkeley.edu>,
#     Thomas A. de Ruiter <thomas@de-ruiter.cx>,
#     Ion Stoica <istoica@eecs.berkeley.edy>,
#     Matei Zaharia <matei@eecs.berkely.edu>.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#   3. The name of the author may not be used to endorse or promote
#      products derived from this software without specific prior
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


"""Cloud Workloads Archive Toolbox

OPTIONS:
    -h, --help     print command help
         --doc     print command modules pydoc
    -v             increase verbosity level
"""

import pkgutil
import sys
import os
import logging
from datetime import datetime
from ConfigParser import ConfigParser


TOP_PACKAGES = ["inst", "cwa"]
ROOT = "__ROOT__"

dependency_graph = []
commands = []

def setpath():
    """Add a Cloud Workloads Archive instance to the Python path.

    Finds the root of a Cloud Workloads Archive instance, if any, and adds its
    Python source folder to the Python path.
    """
    instance = find_cwa_instance_root()

    if not instance is None:
        src_dir = os.path.join(instance, "cwa", "src")
        if os.path.isdir(src_dir):
            logging.debug("Add '%s' to path." % src_dir)
            sys.path.append(src_dir)

def find_cwa_instance_root(path=None):
    """Find the root of a Cloud Workloads Archive instance.

    The root of a Cloud Workloads Archive instance contains a "cwa" folder, this
    function keeps searching upwards in the filesystem tree for a folder
    containing a "cwa" folder.

    ARGUMENTS:
        path -- path to start the search, if this value is None, then the
        current working directory is used

    RETURNS:
        Path to the Cloud Workloads Archive instance root or None if no instance is
        found.
    """
    if path is None:
        path = os.getcwd()

    if os.path.isdir(os.path.join(path, "cwa")):
        return path
    else:
        (parent, current) = os.path.split(path)
        if len(current) > 0 and os.path.isdir(parent):
            return find_cwa_instance_root(parent)
        else:
            return None

def getopt(argv, possible_options):
    """Identify options in the arguments.

    This function can be used as a simple replacement for the normal getopt
    module. This function scans the argv argument for options in
    possible_options.

    ARGUMENTS:
        argv -- a list of arguments to be scanned for options
        possible_options -- a list of options to scan for

    RETURNS:
        [ matched args ], [ all other args ]
    """
    resulting_argv = []
    options = []
    for arg in argv:
        if len(arg) > 2:
            if arg[0] == "-" and arg[1] != "-":
                args = ["-%s" % a for a in arg[1:]]
            else:
                args = [arg]
        else:
            args = [arg]

        for a in args:
            if a in possible_options:
                options.append(a)
            else:
                resulting_argv.append(a)

    return options, resulting_argv

def get_module(name, or_parent=False):
    """Load the first matching Python module.

    Try to load the module indicated by the name argument. If no matches are
    found, the last part of the module name is popped of and it is tried again,
    and again...

    ARGUMENTS:
        name -- name of the module to be loaded

    RETURNS:
        module_name, module_reference -- if a matching module has been found
        None, None -- otherwise
    """
    for top in TOP_PACKAGES:
        argv = filter(len, [top] + name.split(".")[1:])
        found = False
        m = sys.modules[__name__]

        while len(argv) > 1:
            try:
                module = ".".join(argv)
                m = __import__(module, globals(), locals())
                found = True
                break
            except ImportError as e:
                #logging.exception(e)
                if or_parent:
                    argv.pop()
                else:
                    break

        if found:
            logging.debug("loaded module '%s'" % module)
            # try to get a handle to the module
            for part in argv[1:]:
                if hasattr(m, part):
                    m = getattr(m, part)
                else:
                    return None, None
            return module, m
    return None, None

def main():
    """Main function for the Cloud Workloads Archive Toolbox.

    This function tries to call the run function on one of the many Cloud Workloads
    Archive Toolbox modules.
    """
    logging_level = 0
    just_show_help = False
    just_show_doc = False
    time = False


    # not using getopt, so we don't choke on unknown options
    opts, real_argv = getopt(sys.argv, ["-v", "-h", "--help", "--doc", "-t"])
    for o in opts:
        if o in ("-h", "--help"):
            just_show_help = True
        elif o in ("-d", "--doc"):
            just_show_doc = True
        elif o in ("-v"):
            logging_level += 1
        elif o in ("-t"):
            time = True

    if logging_level == 1:
        logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s: %(message)s')
    elif logging_level >= 2:
        logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(levelname)s: %(message)s')
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')

    setpath()
    module_name, module_reference = get_module(".".join(real_argv), True)

    if not module_name is None:
        logging.debug("loaded module '%s'" % module_name)

        if just_show_doc:
            help(module_reference)
            sys.exit(0)

        if just_show_help:
            if hasattr(module_reference, "usage"):
                usage_function = getattr(module_reference, "usage")
                usage_function()
                sys.exit(0)
            else:
                usage(["cwa"] + module_name.split(".")[1:], module_reference)
                sys.exit(1)

        # try to call the run function on the module
        if hasattr(module_reference, "run"):
            run_function = getattr(module_reference, "run")
            arguments = real_argv[len(module_name.split(".")):]
            logging.debug("calling %s.run(%s)" % (module_name, arguments))
            start = datetime.now()
            run_function(arguments)
            finish = datetime.now()
            logging.info("running %s.run(%s) took %s" % (module_name, arguments, finish - start))
            if time:
                print "walltime %s" % (finish - start)
            sys.exit(0)
        else:
            usage(["cwa"] + module_name.split(".")[1:], module_reference)
            sys.exit(1)

    # if nothing matches, run this ...
    usage(["cwa"], sys.modules[__name__])
    sys.exit(0)

def mymain(command):
    add_dependency(ROOT, command)

    for command in commands[1:]:
        for dependency in check_dependencies(command):
            add_dependency(command, dependency)

def check_dependencies(command):
    return []

def add_dependency(command, dependency):
    if command not in commands:
        commands.append(command)

    if dependency not in commands:
        commands.append(command)

    command_index = commands.index(command)
    dependency_index = commands.index(dependency)

    if len(dependency_graph) < max(command_index, dependency_index) + 1:
        required = max(command_index, dependency_index) + 1 - len(dependency_graph)
        dependency_graph += [set([])] * required

    if not dependency_index in dependency_graph[command_index]:
        dependency_graph[command_index].add(dependency_index)

def get_config(path = None):
    if path is None: path = find_cwa_instance_root()
    default_config_path = os.path.join(os.path.dirname(__file__), "default_config")
    config_path = os.path.join(path, "cwa", "config")
    config = ConfigParser({
        'path': path,
    })

    try:
        logging.debug("reading config: '%s'" % config_path)
        config.read(default_config_path)
    except:
        pass

    try:
        logging.debug("reading config: '%s'" % config_path)
        config.read(config_path)
    except:
        pass

    return config

def usage(module=["cwa"], m=None):
    """Print usage.

    This prints the usage for the cwa main module or a referred package. It
    shows the docstring of the module/package, and a command list consisting of
    available subpackages/-modules.

    ARGUMENTS:
        module -- module name as a list of parts
        m -- reference to the module object, this is used to get the module
        docstring
    """
    print "usage: %s [OPTIONS]... <command>" % " ".join(module)
    print

    doc = getattr(m, "__doc__")
    if not doc is None:
        print doc

    print "COMMANDS:"
    for top in TOP_PACKAGES:
        module[0] = top
        search_paths = filter(lambda x: os.path.isdir(os.path.join(x,
        os.sep.join(module))), sys.path)
        search_paths = map(lambda x: os.path.join(x, os.sep.join(module)), search_paths)

        for (unused_loader, name, unused_ispkg) in pkgutil.iter_modules(search_paths):
            print "    %s" % name
    print


if __name__ == "__main__":
    main()
