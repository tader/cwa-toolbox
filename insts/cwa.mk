BASENAME   ?= traces
CTA_OPTS   ?=

TRACE_PATH ?= traces
CWJ        ?= $(TRACE_PATH)/$(BASENAME).cwj
CWJD       ?= $(TRACE_PATH)/$(BASENAME).cwjd
CWT        ?= $(TRACE_PATH)/$(BASENAME).cwt
CWTD       ?= $(TRACE_PATH)/$(BASENAME).cwtd

DATA_PATH  ?= data
DATA_OVER_TIME_PATH ?= $(DATA_PATH)/over-time

# Used to create rules by templates
VARIABLES=Disk RunTime
BREAKDOWNS=ExecutableID UserID Status VOID

ALL ?= \
	$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_executableid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_executableid_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_executableid_3600-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_executableid_600-top_5.dat \
	$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_userid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_userid_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_userid_3600-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_userid_600-top_5.dat \
	$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_status_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_status_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_status_3600-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_jobs_per_status_600-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-io_per_executableid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-io_ratio_per_executableid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_disk_per_executableid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_disk_per_userid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_disk_per_status_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400-top_5.dat \
    $(DATA_OVER_TIME_PATH)/$(BASENAME)-runtime_per_executableid_86400.dat

lc = $(subst A,a,$(subst B,b,$(subst C,c,$(subst D,d,$(subst E,e,$(subst F,f,$(subst G,g,$(subst H,h,$(subst I,i,$(subst J,j,$(subst K,k,$(subst L,l,$(subst M,m,$(subst N,n,$(subst O,o,$(subst P,p,$(subst Q,q,$(subst R,r,$(subst S,s,$(subst T,t,$(subst U,u,$(subst V,v,$(subst W,w,$(subst X,x,$(subst Y,y,$(subst Z,z,$1))))))))))))))))))))))))))


all: $(ALL)

clean:
	rm -f $(ALL)

#traces/$(BASENAME).cwj: ../rawData/$(BASENAME).csv rules.txt
#	cta import $(CTA_OPTS)


# TEMPLATE: cumulative_jobs_per_x_template
define cumulative_jobs_per_x_template
VARIABLE     = jobs
BREAKDOWN    = $(1)
BREAKDOWN_LC = $(call lc,$(1))

$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_600.dat \
$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_3600.dat \
$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_86400.dat: $$(CWJ)
	cta analyze jobs_per_executableid \
	    --breakdown=$(BREAKDOWN) \
		$$(CTA_OPTS) $$(CWJ) \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC).dat

$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_600-top_5.dat: $$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_600.dat
	cta analyze top \
		$$(CTA_OPTS) --keep=Time -n 6 \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_600.dat \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_600-top_5.dat

$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_3600-top_5.dat: $$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_3600.dat
	cta analyze top \
		$$(CTA_OPTS) --keep=Time -n 6 \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_3600.dat \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_3600-top_5.dat

$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_86400-top_5.dat: $$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_86400.dat
	cta analyze top \
		$$(CTA_OPTS) --keep=Time -n 6 \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_86400.dat \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE)_per_$(BREAKDOWN_LC)_86400-top_5.dat
endef

# TEMPLATE: cumulative_x_per_y_template
define cumulative_x_per_y_template
VARIABLE     = $(1)
VARIABLE_LC  = $(call lc,$(1))
BREAKDOWN    = $(2)
BREAKDOWN_LC = $(call lc,$(2))

$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE_LC)_per_$(BREAKDOWN_LC)_600.dat \
$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE_LC)_per_$(BREAKDOWN_LC)_3600.dat \
$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE_LC)_per_$(BREAKDOWN_LC)_86400.dat: $$(CWJ)
	cta analyze io_per_executableid \
		--variable=$(VARIABLE) \
		--breakdown=$(BREAKDOWN) \
		$$(CTA_OPTS) $$(CWJ) \
		$$(DATA_OVER_TIME_PATH)/$$(BASENAME)-cumulative_$(VARIABLE_LC)_per_$(BREAKDOWN_LC).dat
endef


$(foreach breakdown,$(BREAKDOWNS),$(eval $(call cumulative_jobs_per_x_template,$(breakdown))))
$(foreach variable,$(VARIABLES),$(foreach breakdown,$(BREAKDOWNS), $(eval $(call cumulative_x_per_y_template,$(variable),$(breakdown)))))

$(DATA_OVER_TIME_PATH)/$(BASENAME)-io_per_executableid_86400.dat: $(CWJ)
	cta analyze cio_per_executableid \
		--top "0, 1229, 3, 904, 925" \
		$(CTA_OPTS) $(CWJ) \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-io_per_executableid.dat

$(DATA_OVER_TIME_PATH)/$(BASENAME)-io_ratio_per_executableid_86400.dat: $(CWJ)
	cta analyze cio_per_executableid \
		--top "0, 1229, 3, 904, 925" \
		--variable=DiskIORatio \
		$(CTA_OPTS) $(CWJ) \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-io_ratio_per_executableid.dat

$(DATA_OVER_TIME_PATH)/$(BASENAME)-runtime_per_executableid_86400.dat: $(CWJ)
	cta analyze cio_per_executableid \
		--top "0, 1229, 3, 904, 925" \
		--variable=RunTime \
		$(CTA_OPTS) $(CWJ) \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-runtime_per_executableid.dat

$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400-top_5.dat: $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_86400-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_3600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_3600-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_executableid_600-top_5.dat

$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400-top_5.dat: $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_86400-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_3600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_3600-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_userid_600-top_5.dat

$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400-top_5.dat: $(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_86400-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_3600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_3600-top_5.dat
	cta analyze top \
		$(CTA_OPTS) --keep=Time -n 6 \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_600.dat \
		$(DATA_OVER_TIME_PATH)/$(BASENAME)-cumulative_runtime_per_status_600-top_5.dat

